// Name: Shaw Young        Date: 9-18-18
import java.util.Scanner;
import java.io.*;
import java.text.DecimalFormat;

public class Cemetery
{
   public static void main (String [] args)
   {
      File file = new File("cemetery.txt");
      int numEntries = countEntries(file);
      Person[] cemetery = readIntoArray(file, numEntries); 
      int min = locateMinAgePerson(cemetery);
      int max = locateMaxAgePerson(cemetery); 
      //for testing only
      //for (int i = 0; i < cemetery.length; i++) 
         //System.out.println(cemetery[i]);
      System.out.println("In the St. Mary Magdelene Old Fish Cemetery --> ");
      System.out.println("Name of youngest person: " + cemetery[min].getName());
      System.out.println("Age of youngest person: " + cemetery[min].getAge());    
      System.out.println("Name of oldest person: " + cemetery[max].getName());
      System.out.println("Age of oldest person: " + cemetery[max].getAge());     
   }
   
   /* Counts and returns the number of entries in File f.
      Uses a try-catch block.   
      @param f -- the file object
   */
   public static int countEntries(File f) {
      
      Scanner infile = null;
      try
      {
         infile = new Scanner(f);  
      }
      catch(IOException e)
      {
         System.out.println("oops");
         System.exit(0);   
      }
      
      int counter = 0;
      String line;      
      while(infile.hasNextLine())   {
        counter++;
      }
      
      infile.close();
      
      return counter;
     }

   /* Reads the data.
      Fills the array with Person objects.
      Uses a try-catch block.   
      @param f -- the file object 
      @param num -- the number of lines in the File f  
   */
   public static Person[] readIntoArray (File f, int num)
   {
      
      Scanner infile = null;
      try
      {
         infile = new Scanner(f);  
      }
      catch(IOException e)
      {
         System.out.println("oops");
         System.exit(0);   
      }
      
            
      Person[] personArray = new Person[num];
      
      for( int x  = 0; x < num; x++ )  {
         personArray[x] = makeObjects(infile.nextLine());   
      }
      
      return personArray;
      
   }
   
   
   
   /* A helper method that instantiates one Person object.
      @param entry -- one line of the input file.
   */
   private static Person makeObjects(String entry)
   {
      String[] strArray = entry.split(" ");
      
      String name = "";
      String age = "";
      String address = "";
      
      int index = 0;
         
      while(strArray[index].substring(0,1).matches(".*[ABCDEFGHIJKLMNOPQRUSTUVWXYZ].*") == false)   {
         name = name + strArray[index] + " ";
         index++;
      }
      
      //strArray[index]   
      index+=4;                     
      age = strArray[index];
         
      while(index != strArray.length)   {
         address = address + strArray[index] + " ";
         index++;
      }
      
      return new Person(name, age, address);
   }  
   
   /* Finds and returns the location (the index) of the Person
      who is the youngest.
      @param arr -- an array of Person objects.
   */
   public static int locateMinAgePerson(Person[] arr)
   {
      int youngestIndex = 0;
      for(int x = 0; x < arr.length; x++) {
         if(arr[x].calculateAge(arr[x].getAge()) < arr[youngestIndex].calculateAge(arr[youngestIndex].getAge()))  {
            youngestIndex = x;
         }
      }
      return youngestIndex;
   }   
   
   /* Finds and returns the location (the index) of the Person
      who is the oldest.
      @param arr -- an array of Person objects.
   */
   public static int locateMaxAgePerson(Person[] arr)
   {
      int oldestIndex = 0;
      for(int x = 0; x < arr.length; x++) {
         if(arr[x].calculateAge(arr[x].getAge()) > arr[oldestIndex].calculateAge(arr[oldestIndex].getAge()))  {
            oldestIndex = x;
         }
      }
      return oldestIndex;   
   }        
} 

class Person
{
   /* private fields  */
   private DecimalFormat df = new DecimalFormat("0.0000");
   private String theName;
   private String burialDateAge;    
   private String residentialAddress;
   
   Person(String name, String age, String home) {
      theName = name;
      residentialAddress = home;
      burialDateAge = age;
   }
   /* any necessary accessor methods */
   public String getName()   {
      return theName;
   }
   
   public String getAddress()  {
      return residentialAddress;
   }
   
   public String getAge()   {
      return burialDateAge;
   }
   
   public double calculateAge(String a)
   {      
      double age;
      if(a.contains("w")) {
         a = a.replaceAll("w", "");
         age = Double.parseDouble(a);
         return age*0.0191780822;
      }
      
      if(a.contains("d")) {
         a = a.replaceAll("d", "");
         age = Double.parseDouble(a);
         return age*0.00273972603;
      }
                 
      return Double.parseDouble(a);
      
   }
}