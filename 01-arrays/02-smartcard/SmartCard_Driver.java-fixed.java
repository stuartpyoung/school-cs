// Name: Rio Young
// Date: 9/3/18
 
import java.text.DecimalFormat;
public class SmartCard_Driverfixed
{
    public static void main(String[] args) 
    {
        Station downtown = new Station("Downtown", 1);
        Station center = new Station("Center City", 1);
        Station uptown = new Station("Uptown", 2);
        Station suburbia = new Station("Suburb", 4);
 
//Exit twice 
        SmartCard jimmy = new SmartCard(20.00); 
        jimmy.board(center);                    //Boarded at Center City.  SmartCard has $20.00
        jimmy.disembark(suburbia);              //From Center City to Suburb costs $2.75.  SmartCard has $17.25
        jimmy.disembark(uptown);          //Error:  did not board?!
        System.out.println();

//Not enough money to exit 
        SmartCard susie = new SmartCard(1.00); 
        susie.board(uptown);                 //Boarded at Uptown.  SmartCard has $1.00
        susie.disembark(suburbia);           //Insuffient funds to exit. Please add more money.
        System.out.println();
 
//Not enough money to board        
        SmartCard kim = new SmartCard(.25);    
        kim.board(uptown);                    //Insuffient funds to board. Please add more money.
        System.out.println();
 
//Travel within same zone        
        SmartCard b = new SmartCard(10.00);   
        b.board(center);                      //Boarded at Center City.  SmartCard has $10.00
        b.disembark(downtown);               //From Center City to Downtown costs $0.50.  SmartCard has $9.50
        System.out.println();
 
//Travel from Zone 4 to Zone 1        
        SmartCard mc = new SmartCard(10.00);  
        mc.board(suburbia);                      //Boarded at Suburbia.  SmartCard has $10.00
        mc.disembark(downtown);              //From Suburbia to Downtown costs $2.75.  SmartCard has $7.25
        System.out.println();
 
        //Make more test cases.  What else needs to be tested?
 
//         Board Disembark at same station 
        SmartCard bob = new SmartCard(10.00);  
        bob.board(suburbia);                     //Boarded at Suburbia.  SmartCard has $10.00
        bob.disembark(suburbia);             //From Suburbia to Suburbia costs $0.00.  SmartCard has $10.00
        System.out.println();
 
//         SmartCard contains exact fee and travel within same zone 
        SmartCard jill = new SmartCard(0.50);  
        jill.board(center);                      //Boarded at Center City.  SmartCard has $0.50
        jill.disembark(downtown);               //From Center City to Downtown costs $0.50.  SmartCard has $0.00
        System.out.println();
 
//         Board twice
        SmartCard max = new SmartCard(10.00);  
        max.board(suburbia);                     //Boarded at Suburbia.  SmartCard has $10.00
        max.board(downtown);              //Error: Already boarded
        System.out.println();   
        
//         Travel within same zone
        SmartCard maggie = new SmartCard(10.00);  
        maggie.board(downtown);                     //Boarded at Suburbia.  SmartCard has $10.00
        maggie.disembark(center);              //From Downtown to Center City costs $0.50.  SmartCard has $9.50
        System.out.println();   
        
//         Travel from zone 4 to 1
        SmartCard glenn = new SmartCard(10.00);  
        glenn.board(suburbia);                     //Boarded at Suburbia.  SmartCard has $10.00
        glenn.disembark(downtown);              //From Suburb to Downtown costs $2.75.  SmartCard has $7.25
        System.out.println();   



    }
}  
 
//Note SmartCard is not denoted as public.  Why?
class SmartCard 
{
    public final static DecimalFormat df = new DecimalFormat("$0.00");
    public final static double MIN_FARE = 0.5;
 
    Station previousStation;
    double balance;
    boolean embarked = false;
 
    SmartCard ( double x )   
    {
      balance = x;
    }
 
    void addMoney ( double money )
    {
      balance = balance + money;
    }
 
    void subtractFee ( double fee )
    {
      balance = balance - fee;
    }
 
    void getBalance()
    {
      
      System.out.println(balance);
    }
 
    boolean isBoarded () {
      return embarked;
    }
 
    void board ( Station station )  
    {
      if ( isBoarded() == false )   
      {
         if ( balance < MIN_FARE )
         {
            System.out.println("Insufficient funds to board. Please add more money.");
         }
         else
         {
            previousStation = station;
            String currentStationName = station.getName();  
            int currentZone = station.getZone();
            System.out.println("Boarded at " + currentStationName + ".  SmartCard has " + df.format(balance));
            embarked = true;
         }
       }
       else
       {
         System.out.println("Error: already boarded!");
       }
    }
 
    void disembark ( Station station )  
    {  
      if ( isBoarded() == true )   
      {   
         String currentStationName = station.getName();  
         int currentZone = station.getZone();
         String previousStationName = previousStation.getName();  
         int previousZone = previousStation.getZone();
 
         double fee;
         if ( currentStationName == previousStationName )
         {
            fee = 0;
         } 
         else 
         { 
            fee = MIN_FARE + ( 0.75 ) * ( Math.abs ( currentZone - previousZone ) );  
         }
         if ( balance - fee < 0 )
         {
            System.out.println("Insuffient funds to exit. Please add more money.");
         }
         else
         // From Center City to Suburb costs $2.75.  SmartCard has $17.25
         {
            subtractFee( fee );
            System.out.println("From " + previousStationName + " to " + currentStationName + " costs " + df.format( fee ) + ".  SmartCard has " + df.format( balance ));
            embarked = false;
         }
 
       }
       else
       {
         System.out.println("Error: Did not board.");
       }
 
    }
} 
 
//Note Station is not a public class.  Why?
class Station
{
    String name;
    int zone;
 
    Station(String n, int z)
    {
         name = n;
         zone = z;
    }     
 
    String getName()
    {
      return name;
    } 
 
    int getZone() 
    {
      return zone;
    } 
 
    void setName ( String newName )
    {
      name = newName;
    } 
 
    void setZone ( int newZone ) 
    {
      zone = newZone;
    } 
}
 
 
 /*******************  Sample Run ************
 
 Boarded at Center City.  SmartCard has $20.00
 From Center City to Suburb costs $2.75.  SmartCard has $17.25
 Error: did not board?!
 
 Boarded at Uptown.  SmartCard has $1.00
 Insufficient funds to exit. Please add more money.
 
 Insufficient funds to board. Please add more money.
 
 Boarded at Center City.  SmartCard has $10.00
 From Center City to Downtown costs $0.50.  SmartCard has $9.50
 
 Boarded at Suburb.  SmartCard has $10.00
 From Suburb to Downtown costs $2.75.  SmartCard has $7.25
 
 ************************************************/