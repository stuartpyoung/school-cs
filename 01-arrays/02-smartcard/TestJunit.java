import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestJunit {
  
   @Test
   public static void testFares() {

        Station downtown = new Station("Downtown", 1);
        Station center = new Station("Center City", 1);
        Station uptown = new Station("Uptown", 2);
        Station suburbia = new Station("Suburb", 4);
     
        SmartCard jimmy = new SmartCard(20.00); 
        jimmy.board(center);                    //Boarded at Center City.  SmartCard has $20.00
        jimmy.disembark(suburbia);              //From Center City to Suburb costs $2.75.  SmartCard has $17.25
        jimmy.disembark(uptown);				//Error:  did not board?!
        System.out.println();
   			
        SmartCard susie = new SmartCard(1.00); 
        susie.board(uptown);            		//Boarded at Uptown.  SmartCard has $1.00
        susie.disembark(suburbia);				//Insuffient funds to exit. Please add more money.
        System.out.println();
    
        SmartCard kim = new SmartCard(.25);    
        kim.board(uptown);            		    //Insuffient funds to board. Please add more money.
        System.out.println();
    
        SmartCard b = new SmartCard(10.00);   
        b.board(center);            		    //Boarded at Center City.  SmartCard has $10.00
        b.disembark(downtown);					//From Center City to Downtown costs $0.50.  SmartCard has $9.50
        System.out.println();
          
        SmartCard mc = new SmartCard(10.00);  
        mc.board(suburbia);            		    //Boarded at Suburbia.  SmartCard has $10.00
        mc.disembark(downtown);					//From Suburbia to Downtown costs $2.75.  SmartCard has $7.25    
        System.out.println();
      
        SmartCard ee = new SmartCard(10.00);  
        mc.board(suburbia);            		    //Boarded at Suburbia.  SmartCard has $10.00
        mc.board(downtown);					   //Error: did not disembark?!
        System.out.println();
        


     String [] arguments = {"something"};  
     SmartCard smartcard = new SmartCard();

   }

    public static void main(String[] args)
    {
      testFares();
    }

}