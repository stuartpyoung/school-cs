//Name:Rio Young      
//Date: 9/6/18  
import java.util.*;
import java.io.*;
public class PigLatin
{
   public static void main(String[] args) 
   {
      part_1_using_pig();
     // part_2_using_piglatenizeFile();
   }
   
   public static void part_1_using_pig()
   {
      Scanner sc = new Scanner(System.in);
      while(true)
      {
         System.out.print("\nWhat word? ");
         String input = sc.next();
         if (input.equals("-1")) 
            System.exit(0);
         String p = pig( input );
         System.out.println( p );
      }		
   }
   public static String pig(String string)
   {
      char firstLetterInitial;
      char firstLetterFinal = string.charAt(0);
      char firstVowel
      boolean hasVowel;
      boolean yIsConsonant = true;
      static String vowel = "aiueo";
      static String consonant = "qwrtpsdfghjklzxcvbnm";    
   }
         
   public void firstLetterVowel(String string)      
   {
   
   }
   
   public void ifQU(String string)
   {
   
   }


}
      
      return "";
   }

   public static void part_2_using_piglatenizeFile() 
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("input filename including .txt: ");
      String fileNameIn = sc.next();
      System.out.print("output filename including .txt: ");
      String fileNameOut = sc.next();
      piglatenizeFile( fileNameIn, fileNameOut );
      System.out.println("Piglatin done!");
   }

/****************************** 
*  precondition:  both Strings include .txt
*  postcondition:  output a piglatinized .txt file 
******************************/
   public static void piglatenizeFile(String fileNameIn, String fileNameOut) 
   {
      Scanner infile = null;
      try
      {
         infile = new Scanner(new File(fileNameIn));  
      }
      catch(IOException e)
      {
         System.out.println("oops");
         System.exit(0);   
      }
   
      PrintWriter outfile = null;
      try
      {
         outfile = new PrintWriter(new FileWriter(fileNameOut));
      }
      catch(IOException e)
      {
         System.out.println("File not created");
         System.exit(0);
      }
   	
      /*  Enter your code here.  Try to preserve lines and paragraphs. ***/
   
   
   
   
   
      outfile.close();
      infile.close();
   }
}
