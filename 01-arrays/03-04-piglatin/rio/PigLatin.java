//Name:Rio Young      
//Date: 9/6/18  
import java.util.*;
import java.io.*;
import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;

public class PigLatin
{
   public static void main(String[] args) 
   {
    //  part_1_using_pig();
      part_2_using_piglatenizeFile();
   }
   
   public static void part_1_using_pig()
   {
      Scanner sc = new Scanner(System.in);
      while(true)
      {
         System.out.print("\nWhat word? ");
         String input = sc.next();
         if ( input.equals("-1") ) {
            System.exit(0);
         }
         String p = pig( input );
         System.out.println( p );
      }		
   }
   
   public static String pig(String string)
   {
      String firstLetters = "";
      String remainder = "";
      String vowel = "aiueo";
      String consonant = "qwrtpsdfghjklzxcvbnm";    
  
      // If punctuation (period, comma, question mark, exclamation mark, colon, semi-colon, quotation mark) is used in English then it should remain in Pig Latin.  Apostrophes and dashes are not punctuation and should be ignored and remain where they are.
      String prefix = "";
      String suffix = "";
      if ( string.substring(0,1).equals("\"") )
      {
         prefix = "\"";
         string = string.substring(1);
         int length = string.length() - 1;
         if ( string.substring( length ).equals("\"") )
         {
            suffix = "\"";
            string = string.substring( 0, string.length() - 1 );
         }
      }

      String punctuation = ".,?!:;";
      while ( punctuation.indexOf( string.substring( string.length() - 1 ) ) != -1 )
      {
         int length = string.length() - 1;
         suffix = string.substring( length ) + suffix;
         string = string.substring( 0, length );
      }

      // INITIALIZE ARRAY
      String[] stringArray =  string.split("");
      
      // First rule: Leading consonant (not "q") followed by vowel
      boolean vowelSeen = false;
      boolean isCapitalized = false;
      for ( int i = 0; i < stringArray.length; i++ )
      {
         String currentLetter = stringArray[i];
         boolean isVowel = vowel.indexOf( currentLetter.toLowerCase() ) != -1;

         if ( i == 0 && currentLetter.toUpperCase().equals( currentLetter ) )
         {
            isCapitalized = true;
         }

         // Treat "y" as a consonant if it's the first letter of a word; otherwise, treat "y" as a vowel if it appears earlier than any other vowel. 
         if ( i != 0 && currentLetter.equals("y") )
         {
            isVowel = true;
         }
      
         if ( isVowel )
         {
            // If the first letter is a vowel then just append "way". 
            if ( i == 0 && isVowel )
            {
               firstLetters = "w";
               remainder = string;
            }
            else if ( i != 0 && ! vowelSeen )
            {
              // If the first vowel is a "u" and the letter before it is a "q" then the "u" also goes to the end of the word.
               if ( currentLetter.equals("u") && stringArray[ i - 1 ].equals("q") )
               {
                  firstLetters = string.substring( 0, i + 1 );
                  remainder = string.substring( i + 1 );
               }
               else
               // move the leading consonants to the end of the word and append "ay". 
               {
                  firstLetters = string.substring( 0, i );
                  remainder = string.substring( i);
                  if ( isCapitalized )
                  {
                     firstLetters = firstLetters.substring(0,1).toLowerCase() + firstLetters.substring(1);
                     remainder = remainder.substring(0,1).toUpperCase() + remainder.substring(1);
                  }
               }               
            }
            vowelSeen = true;
         }
      }
      
      //   If the word has no vowel, print "**** NO VOWEL ****". 
      if ( ! vowelSeen ) {
         return "**** NO VOWEL ****";
      }
      
      return prefix + remainder + firstLetters + "ay" + suffix;
   }
         

   public static void part_2_using_piglatenizeFile() 
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("input filename including .txt: ");
      String inputFilename = sc.next();
      System.out.print("output filename including .txt: ");
      String outputFilename = sc.next();
      piglatenizeFile( inputFilename, outputFilename );
      System.out.println("Piglatin done!");
   }

/****************************** 
*  precondition:  both Strings include .txt
*  postcondition:  output a piglatinized .txt file 
******************************/
   public static void piglatenizeFile(String inputFilename, String outputFilename ) 
   {
      Scanner infile = null;
      try
      {
         infile = new Scanner(new File(inputFilename));  
      }
      catch(IOException e)
      {
         System.out.println("oops");
         System.exit(0);   
      }
   
      PrintWriter outfile = null;
      try
      {
         outfile = new PrintWriter(new FileWriter(outputFilename));
      }
      catch(IOException e)
      {
         System.out.println("File not created");
         System.exit(0);
      }
   	
      while ( infile.hasNext() )
      {
        String string = infile.nextLine();
        //System.out.println( "string: " + string);
        String[] tokens = string.split( " " );
        //System.out.println( "tokens: " + tokens);
        //System.out.println( "tokens.length: " + tokens.length);
        
        if( ! string.equals(null) && ! string.matches("^\\s*$")  ) {
            for ( int i = 0; i < tokens.length; i++ )
            {
                tokens[i] = pig( tokens[i] );
                //System.out.println ( "tokens[" + i + "]:" + tokens[i] ); 
            }
        
           outfile.print( String.join( " ", tokens ) + "\n" );
        }
      }
   
      outfile.close();

   
   
   
   
      outfile.close();
      infile.close();
   }
}
