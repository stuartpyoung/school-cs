import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;


public class scannerpractice
{
   public static void main(String[] args) {
      IOpractice();
      
   }

   public static void IOpractice() throws FileNotFoundException
   {
     Scanner scanner = new Scanner( System.in );
     System.out.println( "What filename?" );
     String filename = scanner.next();
     Scanner infile = new Scanner ( new File ( filename ) );
     String fileout = "results.txt";
     PrintWriter outfile = new PrintWriter ( new File ( fileout ) );
     while ( infile.hasNext() )
     {
        String string = infile.nextLine();
        outfile.print( string + ", " );
     }
   
     outfile.close();

   }
}