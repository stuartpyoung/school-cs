//Name: Shaw Young      
//Date: 9/11/18  
import java.util.*;
import java.io.*;
public class PigLatin
{
   public static void main(String[] args) 
   {
     // part_1_using_pig();
     part_2_using_piglatenizeFile();
   }
   
   public static void part_1_using_pig()
   {
      Scanner sc = new Scanner(System.in);
      while(true)
      {
         System.out.print("\nWhat word? ");
         String input = sc.next();
         if ( input.equals("-1") ) {
            System.exit(0);
         }
         String p = pig( input );
         System.out.println( p );
      }		
   }
   
   public static String[] handleQuotes(String inputString) {
      String initialPunctuationQuotes = "";
      String finalPunctuationQuotes = "";
      if (inputString.substring(0,1).equals("\"") ) {
         initialPunctuationQuotes = "\"";
         inputString = inputString.substring(1);
         int length = inputString.length()-1;
         if ( inputString.substring( length ).equals("\"") )
         {
            finalPunctuationQuotes = "\"";
            inputString = inputString.substring( 0, inputString.length() - 1 );
         }
      }
      
   return new String[] {
         initialPunctuationQuotes,
         finalPunctuationQuotes,
         inputString
   };
}
   
   public static boolean hasVowels(String inputString)   { 
      String vowels = "aeiouyAEIOUY";
      for ( int i = 0; i < inputString.length(); i++ ) {
         for ( int x = 0; x < vowels.length(); x++ ) {
            if(inputString.charAt(i) == vowels.charAt(x))
               return true;
         }
      }
      return false;
   }
   
   public static String[] handlePunctuation(String inputString, String finalPunctuation ) {
      String punctuation = "?!:;.,";
      for ( int x  = inputString.length() - 1; x > 0; x-- )  {
         if ( punctuation.indexOf( inputString.substring( x ) ) != -1 ) {
            int length = inputString.length() - 1; 
            finalPunctuation = inputString.substring( x ) + finalPunctuation;
            inputString = inputString.substring( 0, length );
         }
      }

      return new String[] {
            finalPunctuation,
            inputString
      };
}

   public static String pig(String inputString)
   {
      String beginningString = "";
      String endString = "";
      String vowel = "aiueo";
      String consonant = "qwrtpsdfghjklzxcvbnm";    
  
      if ( hasVowels( inputString ) == false ) {
         return "**** NO VOWEL ****";
      }

      String[] result = handleQuotes( inputString );
      String initialPunctuation = result[0]; 
      String finalPunctuation = result[1];
      inputString = result[2];
      
      result = handlePunctuation( inputString, finalPunctuation );
      finalPunctuation = result[0];
      inputString = result[1];

      String[] arrayOfString =  inputString.split("");
      
      boolean hasVowel = false;
      boolean isCapitalized = false;
      for ( int i = 0; i < arrayOfString.length; i++ ) {
         String thisLetter = arrayOfString[i];
         boolean characterIsVowel = vowel.indexOf( thisLetter.toLowerCase() ) != -1;
         
         if ( i == 0 && thisLetter.toUpperCase().equals( thisLetter ) )  {
            isCapitalized = true;
         }

         if ( i != 0 && thisLetter.equals("y") ) {
            characterIsVowel = true;
         }
      
         if ( characterIsVowel ) {
            if ( i == 0 && characterIsVowel ) {
               beginningString = "w";
               endString = inputString;
            }
            else if ( i != 0 && ! hasVowel ) {
               if ( thisLetter.equals("u") && arrayOfString[ i - 1 ].equals("q") ) {
                  beginningString = inputString.substring( 0, i + 1 );
                  endString = inputString.substring( i + 1 );
               }
               else {
                  beginningString = inputString.substring( 0, i );
                  endString = inputString.substring( i);
                  if ( isCapitalized ) {
                     beginningString = beginningString.substring(0,1).toLowerCase() + beginningString.substring(1);
                     endString = endString.substring(0,1).toUpperCase() + endString.substring(1);
                  }
               }               
            }
            hasVowel = true;
         }
      }
      
      return initialPunctuation + endString + beginningString + "ay" + finalPunctuation;
   }
         

   public static void part_2_using_piglatenizeFile() 
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("input filename including .txt: ");
      String fileNameIn = sc.next();
      System.out.print("output filename including .txt: ");
      String fileNameOut = sc.next();
      piglatenizeFile( fileNameIn, fileNameOut );
      System.out.println("Piglatin done!");
   }

/****************************** 
*  precondition:  both Strings include .txt
*  postcondition:  output a piglatinized .txt file 
******************************/
   public static void piglatenizeFile(String fileNameIn, String fileNameOut) 
   {
      Scanner infile = null;
      try
      {
         infile = new Scanner(new File(fileNameIn));  
      }
      catch(IOException e)
      {
         System.out.println("oops");
         System.exit(0);   
      }
   
      PrintWriter outfile = null;
      try
      {
         outfile = new PrintWriter(new FileWriter(fileNameOut));
      }
      catch(IOException e)
      {
         System.out.println("File not created");
         System.exit(0);
      }
   	
      /*  Enter your code here.  Try to preserve lines and paragraphs. ***/
   
   
   
       while(infile.hasNext())  
         outfile.print(pig(infile.next()));
   
   
      outfile.close();
      infile.close();
   }
}
