import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;


public class TestJunit {
  
   @Test
   public static void testPig() {

      List<Map<String , String>> testList  = new ArrayList<Map<String,String>>();

     Map<String,String> testHash1 = new HashMap<String, String>();
      testHash1.put("name", "simple1");
      testHash1.put("input", "pig");
      testHash1.put("expected", "igpay");   
      testList.add(0, testHash1);
      
      Map<String,String> testHash2 = new HashMap<String, String>();
      testHash2.put("name", "simple2");
      testHash2.put("input", "latin");
      testHash2.put("expected", "atinlay");   
      testList.add(1, testHash2);
     
      Map<String,String> testHash3 = new HashMap<String, String>();
      testHash3.put("name", "simple3");
      testHash3.put("input", "this");
      testHash3.put("expected", "isthay");   
      testList.add(2, testHash3);
     
      Map<String,String> testHash4 = new HashMap<String, String>();
      testHash4.put("name", "simple4");
      testHash4.put("input", "strange");
      testHash4.put("expected", "angestray");   
      testList.add(3, testHash4);
     
      Map<String,String> testHash5 = new HashMap<String, String>();
      testHash5.put("name", "simple4");
      testHash5.put("input", "bcdfgh");
      testHash5.put("expected", "**** NO VOWEL ****");   
      testList.add(4, testHash5);
           
      Map<String,String> testHash6 = new HashMap<String, String>();
      testHash6.put("name", "qurious");
      testHash6.put("input", "squeeze");
      testHash6.put("expected", "eezesquay");   
      testList.add(5, testHash6);
     
      Map<String,String> testHash7 = new HashMap<String, String>();
      testHash7.put("name", "qurious2");
      testHash7.put("input", "question");
      testHash7.put("expected", "estionquay");   
      testList.add(6, testHash7);
     
      Map<String,String> testHash8 = new HashMap<String, String>();
      testHash8.put("name", "leadingvowel1");
      testHash8.put("input", "apple");
      testHash8.put("expected", "appleway");   
      testList.add(7, testHash8);
     
      Map<String,String> testHash9 = new HashMap<String, String>();
      testHash9.put("name", "leadingvowel2");
      testHash9.put("input", "eye");
      testHash9.put("expected", "eyeway");   
      testList.add(8, testHash9);
     
      Map<String,String> testHash10 = new HashMap<String, String>();
      testHash10.put("name", "capitals1");
      testHash10.put("input", "Thomas");
      testHash10.put("expected", "Omasthay");   
      testList.add(9, testHash10);
     
      Map<String,String> testHash11 = new HashMap<String, String>();
      testHash11.put("name", "capitals2");
      testHash11.put("input", "Jefferson");
      testHash11.put("expected", "Effersonjay");   
      testList.add(10, testHash11);
      
      Map<String,String> testHash12 = new HashMap<String, String>();
      testHash12.put("name", "capitals3");
      testHash12.put("input", "McDonald");
      testHash12.put("expected", "OnaldmcDay");   
      testList.add(11, testHash12);
      
      Map<String,String> testHash13 = new HashMap<String, String>();
      testHash13.put("name", "capitals4");
      testHash13.put("input", "McArthur");
      testHash13.put("expected", "Arthurmcay");   
      testList.add(12, testHash13);
      
      Map<String,String> testHash14 = new HashMap<String, String>();
      testHash14.put("name", "capitals5");
      testHash14.put("input", "BUBBLES");
      testHash14.put("expected", "UBBLESbay");   
      testList.add(13, testHash14);
      
      Map<String,String> testHash15 = new HashMap<String, String>();
      testHash15.put("name", "capitals6");
      testHash15.put("input", "Apple");
      testHash15.put("expected", "Appleway");   
      testList.add(14, testHash15);
     
      Map<String,String> testHash16 = new HashMap<String, String>();
      testHash16.put("name", "ytest1");
      testHash16.put("input", "yes");
      testHash16.put("expected", "esyay");   
      testList.add(15, testHash16);
     
      Map<String,String> testHash17 = new HashMap<String, String>();
      testHash17.put("name", "ytest2");
      testHash17.put("input", "try");
      testHash17.put("expected", "ytray");   
      testList.add(16, testHash17);
     
      Map<String,String> testHash18 = new HashMap<String, String>();
      testHash18.put("name", "ytest3");
      testHash18.put("input", "rhyme");
      testHash18.put("expected", "ymerhay");   
      testList.add(17, testHash18);

      Map<String,String> testHash19 = new HashMap<String, String>();
      testHash19.put("name", "punctuation1");
      testHash19.put("input", "\"hello\"");
      testHash19.put("expected", "\"ellohay\"");   
      testList.add(18, testHash19);

      Map<String,String> testHash20 = new HashMap<String, String>();
      testHash20.put("name", "punctuation1");
      testHash20.put("input", "\"Hello!!!!\"");
      testHash20.put("expected", "\"Ellohay!!!!\"");   
      testList.add(19, testHash20);
   

      PigLatin pigLatin = new PigLatin();

      for ( Map<String, String> map : testList ) {

         String name = map.get("name");
         String input = map.get("input");
         String expected = map.get("expected");
         
         // DO TEST
         String actual = pigLatin.pig( input );
         System.out.println("actual: " + actual);
         System.out.println("expected: " + expected);

         assertEquals( expected, actual );
      }    

      System.out.println("ALL TESTS COMPLETED SUCCESSFULLY.");

   }

    public static void main(String[] args)
    {
      testPig();
      //testSplitString();
      //testScrub();
    }

}