import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;


public class TestJunit {
  
   @Test
   public static void testConvertDate() {

      Map<String,String> testHash = new HashMap<String, String>();
      List<Map<String , String>> testList  = new ArrayList<Map<String,String>>();
      testHash.put("name", "simple");
      testHash.put("input", "09/12/2008");
      testHash.put("expected", "12-09-08");   
      testList.add(0, testHash);
      
      StringMethods stringMethods = new StringMethods();

      for ( Map<String, String> map : testList ) {
         System.out.println("map: " + map);

         String name = map.get("name");
         String input = map.get("input");
         String expected = map.get("expected");
         
         // DO TEST
         String actual = stringMethods.convertDate( input );
         assertEquals( actual, expected );
      }    
   }

    public static void main(String[] args)
    {
      testConvertDate();
      //testSplitString();
      //testScrub();
    }

}