//Name: Rio Young
//Date: 9/4/18

public class StringMethods
{
   public static void main( String[ ] args ) {};
   
   public static String convertDate ( String string )
   {
      System.out.println( "string:" + string );   

      String[] stringArray = string.split( "/" ); 
      System.out.println("stringArray: ");

      String month = stringArray[0];
      String day = stringArray[1];
      String year = stringArray[2].substring(2);
      
      String newDateStr = day + "-" + month + "-" + year;
      System.out.println("newDateStr: " + newDateStr);
      
     return newDateStr;
   }
         
   public static void printArray ( String[] array ) 
   {
      for ( int i = 0; i < array.length; i++ )
      {
         System.out.println("array[" + i + "]: " + array[i]);
      }
   }
   
      /*  String Methods #2  
   String line = "The rain in Spain falls    mainly on the plain.";
               
      
   
    
   
      
      /*   String Methods #3  
   String messyStr = "Who. do yo$u th,ink you    are?!";
      System.out.println( scrub(messyStr) );       //Who do you think you    are
   }
   
   /*  class method used by String Methods #3    
   static String punct = ".,?$!:;\""; 
   public static String scrub(String s)
   */
   
}
