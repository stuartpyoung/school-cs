// Name: Rio Young
//Date: 9/19/18
import java.util.Scanner;
import java.io.File;
import java.text.DecimalFormat;
import java.util.*;
import java.io.*;

public class Cemetery
{
   public static void main (String [] args)
   {
      File file = new File("cemetery.txt");
      int numberEntries = countEntries(file);
      Person[] cemetery = readIntoArray(file, numberEntries );
      int minimum = locateMinAgePerson(cemetery);
      int maximum = locateMaxAgePerson(cemetery); 
      System.out.println("In the St. Mary Magdelene Old Fish Cemetery --> ");
      System.out.println("Name of youngest person: " + cemetery[minimum].getName());
      System.out.println("Age of youngest person: " + cemetery[minimum].getAge());    
      System.out.println("Name of oldest person: " + cemetery[maximum].getName());
      System.out.println("Age of oldest person: " + cemetery[maximum].getAge());
            
      return;
   }
   
   /* Counts and returns the number of entries in File f.
      Uses a try-catch block.   
      @param f -- the file object
   */
   public static int countEntries( File inputFile )
   {
      int counter = 0;
      
      Scanner infile = openFile( inputFile );
      
      while ( infile.hasNext() )
      {
        String line = infile.nextLine();
        counter++;
      }
      
      infile.close();
      
      return counter;
   }
   
   /* Reads the data.
      Fills the array with Person objects.
      Uses a try-catch block.   
      @param f -- the file object 
      @param num -- the number of lines in the File f  
   */   
   public static Person[] readIntoArray ( File inputFile, int numberEntries )
   {
      String name;
      String date;
      String age;
      Person[] personArray = new Person[ numberEntries ];
      
      Scanner infile = openFile( inputFile );
      
      int counter = 0;
      while ( infile.hasNext() )
      {   
         String line = infile.nextLine();
         if ( line.matches( "^\\s+$" ) ) {
            continue;            
         }
         else {
            Person person = makeObjects( line );
            personArray[ counter ] =  person;            
            counter++;
         }
      }
      
      infile.close();
      
      return personArray;
   }   
   
   public static Scanner openFile ( File inputFile )
   {
      Scanner infile = null;
      try
      {
         infile = new Scanner( inputFile );  
      }
      catch( IOException e )
      {
         System.out.println("oops");
         System.exit(0);   
      }
      
      return infile;
   }
   
   /* A helper method that instantiates one Person object.
      @param entry -- one line of the input file.
   */
   private static Person makeObjects(String line)
   {
      String name = cleanEnd( line.substring( 0, 25 ) );
      String date = cleanEnd( line.substring( 25, 36 ) );
      String age = cleanEnd( line.substring( 37, 42 ) );
      
      return new Person ( name, date, age );      
   }  

   public static String cleanEnd ( String inputString )
   {
      return inputString.replaceAll( "\\s+$", "" );
   }
   
   /* Finds and returns the location (the index) of the Person
      who is the youngest.
      @param arr -- an array of Person objects.
   */
   public static int locateMinAgePerson( Person[] personArray )
   {
      int index = -1;
      double minimum = 99999999;
      for ( int i = 0; i < personArray.length; i++ )
      {
         if ( personArray[i].getAge() < minimum )
         {
            minimum = personArray[i].getAge();
            index = i;
         }
      }
      
      return index;
   }   
   
   /* Finds and returns the location (the index) of the Person
      who is the oldest.
      @param arr -- an array of Person objects.
   */
   public static int locateMaxAgePerson( Person[] personArray )
   {
      int index = -1;
      double maximum = 0;
      for ( int i = 0; i < personArray.length; i++ )
      {
         if ( personArray[i].getAge() > maximum )
         {
            maximum = personArray[i].getAge();
            index = i;
         }
      }
      
      return index;
   }
         
} 

class Person
{
   /* private fields  */
   private DecimalFormat df = new DecimalFormat("0.0000");
   private String name;
   private String date;
   private double age;
   private static double weeksInYear = 52.1429;
   private static double daysInYear = 365.0003;
      
   /* a three-arg constructor  */
   Person ( String inputName, String inputDate, String inputAge )
   {
      name = inputName;
      date = inputDate;
      age = calculateAge( inputAge );
   }

   /* any necessary accessor methods */
   public String getName ()
   {
      return name;
   }
   public String getDate ()
   {
      return date;
   }
   public double getAge ()
   {
      return age;
   }
   
   public double calculateAge( String inputAge )
   {
      double age = 0;
      
      if ( inputAge.indexOf( "w" ) != -1 )
      {
         inputAge = inputAge.replaceAll( "w", "" );
         age = Double.parseDouble( inputAge ) / weeksInYear;
      }
      else if ( inputAge.indexOf( "d" ) != -1 )
      {
         inputAge = inputAge.replaceAll( "d", "" );
         age = Double.parseDouble( inputAge ) / daysInYear;
      }
      else {
         age = Double.parseDouble( inputAge );
      }
      age = Double.parseDouble( df.format( age ) );
      
      return age;
   }
   
}