import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestJunit {
  
   @Test
   public static void testCalculateModes() {

     String [] arguments = {"something"};  
     Modes modes = new Modes();

      int[] tally = {0,0,10,5,10,0,7,1,0,6,0,10,3,0,0,1};
      System.out.println("tally:");
      modes.display(tally);

      int[] actual = modes.calculateModes(tally);
      System.out.println("actual:");
      modes.display(actual);

      int[] expected = { 2, 4, 11 };
      System.out.println("expected:");
      modes.display(expected);

      for ( int i = 0; i < actual.length; i++ ) {     
         assertEquals( actual[i], expected[i] );
      }
   }

    public static void main(String[] args)
    {
      testCalculateModes();
    }

}