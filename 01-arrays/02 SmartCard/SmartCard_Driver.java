// Name:
// Date:
 
import java.text.DecimalFormat;
public class SmartCard_Driver
{
    public static void main(String[] args) 
    {
        Station downtown = new Station("Downtown", 1);
        Station center = new Station("Center City", 1);
        Station uptown = new Station("Uptown", 2);
        Station suburbia = new Station("Suburb", 4);
     
        SmartCard jimmy = new SmartCard(20.00); 
        jimmy.board(center);                    //Boarded at Center City.  SmartCard has $20.00
        jimmy.disembark(suburbia);              //From Center City to Suburb costs $2.75.  SmartCard has $17.25
        jimmy.disembark(uptown);				//Error:  did not board?!
        System.out.println();
   			
        SmartCard susie = new SmartCard(1.00); 
        susie.board(uptown);            		//Boarded at Uptown.  SmartCard has $1.00
        susie.disembark(suburbia);				//Insuffient funds to exit. Please add more money.
        System.out.println();
    
        SmartCard kim = new SmartCard(.25);    
        kim.board(uptown);            		    //Insuffient funds to board. Please add more money.
        System.out.println();
    
        SmartCard b = new SmartCard(10.00);   
        b.board(center);            		    //Boarded at Center City.  SmartCard has $10.00
        b.disembark(downtown);					//From Center City to Downtown costs $0.50.  SmartCard has $9.50
        System.out.println();
          
        SmartCard mc = new SmartCard(10.00);  
        mc.board(suburbia);            		    //Boarded at Suburbia.  SmartCard has $10.00
        mc.disembark(downtown);					//From Suburbia to Downtown costs $2.75.  SmartCard has $7.25    
        System.out.println();
      
        SmartCard ee = new SmartCard(10.00);  
        mc.board(suburbia);            		    //Boarded at Suburbia.  SmartCard has $10.00
        mc.board(downtown);					   //Error: did not disembark?!
        System.out.println();
        
        SmartCard zz = new SmartCard(1.00); 
        zz.board(uptown);            		//Boarded at Uptown.  SmartCard has $1.00
        zz.disembark(suburbia);				//Insuffient funds to exit. Please add more money.
        zz.addMoney(10.00);
        zz.disembark(suburbia);				//From Uptown to Suburbia costs $2.00.  SmartCard has $9.00 
        System.out.println();
        
        //Make more test cases.  What else needs to be tested?
    }
} 	

//Note SmartCard is not denoted as public.  Why?
class SmartCard 
{
    public final static DecimalFormat df = new DecimalFormat("$0.00");
    public final static double MIN_FARE = 0.5;
    
    double myBalance;
    String currentStation;
    int currentZone;
    double fee;
    boolean canEmbark;
    boolean canDisembark;
    
    SmartCard(double balance) {
      myBalance = balance;
      canEmbark = true;
      canDisembark = false;
    }
    
    double getBalance() {
      return myBalance;
    }
    
    void addMoney(double d) {
      myBalance = myBalance + d;
    }
    
    boolean isBoarded()   {
      if(canDisembark = true) 
         return true;
      else
         return false; 
    }
    void cost(Station s) {
      System.out.println(MIN_FARE + (0.75)*(Math.abs(currentZone - s.getZone())));
    }
    
    void board(Station station)   {
      if(canEmbark == true)   {
         currentStation = station.getName();  
         currentZone = station.getZone();
         fee = MIN_FARE + (0.75)*(Math.abs(currentZone - station.getZone()));
         if(myBalance - fee < 0)
            System.out.println("Insuffient funds to board. Please add more money.");
         else  {
            System.out.println("Boarded at " + currentStation + ".  SmartCard has " + df.format(myBalance));
            canEmbark = false;
            canDisembark = true;
            }
      }
      else 
        System.out.println("Error: did not disembark?!"); 
    }
    
    void disembark(Station station)   {
      if(canDisembark == true)  {
         fee = MIN_FARE + (0.75)*(Math.abs(currentZone - station.getZone()));
         if(myBalance - fee < 0) {
            System.out.println("Insuffient funds to exit. Please add more money.");
         }
         else  {
            myBalance = myBalance-fee;
            System.out.println("From " + currentStation + " to " + station.getName() + " costs " + df.format(fee) + " Smartcard has " + df.format(myBalance));
            currentStation = station.getName();
            currentZone = station.getZone();        
            canDisembark = false;
            canEmbark = true;
         }
         
      }
      else
         System.out.println("Error: did not board?!");
    }
    
}
   
//Note Station is not a public class.  Why?
class Station
{
      int myZone;
      String myName;
      
      Station(String name, int n)   {
         myName = name;
         myZone = n;
      }
      
      String getName()   {
         return myName;
      }
      
      int getZone()   {
         return myZone;
      }
      
      void setZone(int x)   {
         myZone = x;
      }
            
      void getName(String m)   {
         myName = m;
      }
      
      /*void toString() {
         System.out.println("Name:" + myName + "\t Zone:" + myZone);
      } */
}

 /*******************  Sample Run ************

 Boarded at Center City.  SmartCard has $20.00
 From Center City to Suburb costs $2.75.  SmartCard has $17.25
 Error: did not board?!
 
 Boarded at Uptown.  SmartCard has $1.00
 Insufficient funds to exit. Please add more money.
 
 Insufficient funds to board. Please add more money.
 
 Boarded at Center City.  SmartCard has $10.00
 From Center City to Downtown costs $0.50.  SmartCard has $9.50
 
 Boarded at Suburb.  SmartCard has $10.00
 From Suburb to Downtown costs $2.75.  SmartCard has $7.25
 
 ************************************************/