// Name: Rio Young
// Date: 3/7/19

import java.io.*;
import java.util.*;

public class Dictionary
{
   static TreeMap<String, TreeSet<String>> englishToSpanish;
   static TreeMap<String, TreeSet<String>> spanishToEnglish;
   static PrintWriter outputFile; 
         
   public static void main(String[] args) 
   {
      Scanner inputFile = null;
      try
      {
         //System.out.println("OPENING INPUT FILE");
         inputFile = new Scanner(new File("spanglish.txt"));

         //System.out.println("OPENING OUTPUT FILE");
         //System.setOut(new PrintStream(new FileOutputStream("dictionaryOutput.txt")));
         outputFile = new PrintWriter(new FileWriter( "dictionaryOutput.txt" ));
         //outputFile.println( "test" );
         
      }
      catch(Exception e)
      {
         System.out.println( "main    error: " + e );         
      }
      

   
         //System.out.println( "main    inputFile: " + inputFile );
         englishToSpanish = makeDictionary( inputFile );
         System.out.println("ENGLISH TO SPANISH");
         outputFile.println("ENGLISH TO SPANISH");
         display( englishToSpanish );
      
         System.out.println();
         
         spanishToEnglish = reverse(englishToSpanish);
         System.out.println("SPANISH TO ENGLISH");
         outputFile.println( "SPANISH TO ENGLISH" );
         display( spanishToEnglish );
         
         //outputFile.println(index.toString());
         inputFile.close(); 						
         outputFile.close();
         //System.out.println("Done.");
   
         
      }
   
   //@SuppressWarnings("unchecked")
   public static TreeMap<String, TreeSet<String>> makeDictionary( Scanner inputFile )
   {
      englishToSpanish = new TreeMap<String, TreeSet<String>>();
      spanishToEnglish = new TreeMap<String, TreeSet<String>>();
      
      while( inputFile.hasNextLine() )
      {
         String english = inputFile.nextLine();
         String spanish = inputFile.nextLine();
         //System.out.println( "makeDictionary    english: " + english + ", spanish: " + spanish );
         add( englishToSpanish, english, spanish );
         add( spanishToEnglish, spanish, english );
         
         //System.out.println( "makeDictionary    englishToSpanish: " + englishToSpanish );
      }

      //System.out.println( "makeDictionary    englishToSpanish: " + englishToSpanish );
      return englishToSpanish;
   }
   
   public static void add( TreeMap<String, TreeSet<String>> dictionary, String word, String translation )
   {
      if ( dictionary.containsKey( word.toString() ) )
      {
         dictionary.get( word ).add( translation );
      }
      else
      {
         TreeSet treeSet = new TreeSet<String>();
         treeSet.add( translation.toString() );
         dictionary.put( (String) word, treeSet );
      }
   }
   
   public static void display( TreeMap<String, TreeSet<String>> treeMap )
   {
      String output = "";
      for ( String key : treeMap.keySet() )
      {
         output += "  " + key + " " + treeMap.get( key ) + "\n"; 
      }
      System.out.println( output );
      outputFile.println( output );
   }
   
   public static TreeMap<String, TreeSet<String>> reverse( TreeMap<String, TreeSet<String>> treeMap )
   {
      spanishToEnglish = new TreeMap<String, TreeSet<String>>();
      
      for ( String key : treeMap.keySet() )
      {
         String english = key;
         Iterator iterator = treeMap.get( key ).iterator();
         while ( iterator.hasNext() )
         {
            String spanish = ( String ) iterator.next();
            add( spanishToEnglish, spanish, english );
         }
      }

      //System.out.println( "makeDictionary    spanishToEnglish: " + spanishToEnglish );
      return spanishToEnglish;
   }
}


   /********************
	INPUT:
   	holiday
		fiesta
		holiday
		vacaciones
		party
		fiesta
		celebration
		fiesta
     <etc.>
  *********************************** 
	OUTPUT:
		ENGLISH TO SPANISH
			banana [banana]
			celebration [fiesta]
			computer [computadora, ordenador]
			double [doblar, doble, duplicar]
			father [padre]
			feast [fiesta]
			good [bueno]
			hand [mano]
			hello [hola]
			holiday [fiesta, vacaciones]
			party [fiesta]
			plaza [plaza]
			priest [padre]
			program [programa, programar]
			sleep [dormir]
			son [hijo]
			sun [sol]
			vacation [vacaciones]

		SPANISH TO ENGLISH
			banana [banana]
			bueno [good]
			computadora [computer]
			doblar [double]
			doble [double]
			dormir [sleep]
			duplicar [double]
			fiesta [celebration, feast, holiday, party]
			hijo [son]
			hola [hello]
			mano [hand]
			ordenador [computer]
			padre [father, priest]
			plaza [plaza]
			programa [program]
			programar [program]
			sol [sun]
			vacaciones [holiday, vacation]

**********************/