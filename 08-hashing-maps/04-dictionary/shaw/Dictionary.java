// Name: Shaw Young     Date: 4/8/19

import java.io.*;
import java.util.*;

public class Dictionary
{
   static PrintWriter outputFile; 
         
   public static void main(String[] args) {
      Scanner inputFile = null;
      try {
         inputFile = new Scanner(new File("spanglish.txt"));
         outputFile = new PrintWriter(new FileWriter( "dictionaryOutput.txt" ));
      }
      catch ( Exception e ) {
         System.out.println( "main    error: " + e );         
      }
      
      TreeMap<String, TreeSet<String>> english2Spanish = makeDictionary( inputFile );
      System.out.println("ENGLISH TO SPANISH");
      outputFile.println("ENGLISH TO SPANISH");
      display( english2Spanish );
   
      TreeMap<String, TreeSet<String>> spanish2English = reverse(english2Spanish);
      System.out.println("SPANISH TO ENGLISH");
      outputFile.println( "SPANISH TO ENGLISH" );
      display( spanish2English );
      
      inputFile.close(); 						
      outputFile.close();
   
      return;         
   }
   
   public static TreeMap<String, TreeSet<String>> makeDictionary( Scanner inputFile ) {
      TreeMap<String, TreeSet<String>> english2Spanish = new TreeMap<String, TreeSet<String>>();
      
      while( inputFile.hasNextLine() ) {
         String english = inputFile.nextLine();
         String spanish = inputFile.nextLine();
         add( english2Spanish, english, spanish );
      }

      return english2Spanish;
   }
   
   @SuppressWarnings("unchecked")
   public static void add( TreeMap<String, TreeSet<String>> dictionary, String word, String translation ) {
      if ( dictionary.containsKey( word.toString() ) ) {
         dictionary.get( word ).add( translation );
      }
      else
      {
         TreeSet treeSet = new TreeSet<String>();
         treeSet.add( translation.toString() );
         dictionary.put( (String) word, treeSet );
      }
   }
   
   public static void display( TreeMap<String, TreeSet<String>> treeMap ) {
      String output = "";
      for ( String key : treeMap.keySet() ) {
         output += "  " + key + " " + treeMap.get( key ) + "\n"; 
      }
      System.out.println( output );
      outputFile.println( output );
   }
   
   public static TreeMap<String, TreeSet<String>> reverse( TreeMap<String, TreeSet<String>> treeMap ) {
      TreeMap<String, TreeSet<String>> spanish2English = new TreeMap<String, TreeSet<String>>();
      
      for ( String key : treeMap.keySet() ) {
         String english = key;
         Iterator iterator = treeMap.get( key ).iterator();
         while ( iterator.hasNext() ) {
            String spanish = ( String ) iterator.next();
            add( spanish2English, spanish, english );
         }
      }

      return spanish2English;
   }
}


   /********************
	INPUT:
   	holiday
		fiesta
		holiday
		vacaciones
		party
		fiesta
		celebration
		fiesta
     <etc.>
  *********************************** 
	OUTPUT:
		ENGLISH TO SPANISH
			banana [banana]
			celebration [fiesta]
			computer [computadora, ordenador]
			double [doblar, doble, duplicar]
			father [padre]
			feast [fiesta]
			good [bueno]
			hand [mano]
			hello [hola]
			holiday [fiesta, vacaciones]
			party [fiesta]
			plaza [plaza]
			priest [padre]
			program [programa, programar]
			sleep [dormir]
			son [hijo]
			sun [sol]
			vacation [vacaciones]

		SPANISH TO ENGLISH
			banana [banana]
			bueno [good]
			computadora [computer]
			doblar [double]
			doble [double]
			dormir [sleep]
			duplicar [double]
			fiesta [celebration, feast, holiday, party]
			hijo [son]
			hola [hello]
			mano [hand]
			ordenador [computer]
			padre [father, priest]
			plaza [plaza]
			programa [program]
			programar [program]
			sol [sun]
			vacaciones [holiday, vacation]

**********************/