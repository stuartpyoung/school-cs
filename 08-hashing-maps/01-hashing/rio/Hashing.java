 // Name: Rio Young
 // Date: 3/5/19

/* 
   Assignment:  This hashing program results in collisions.
   You are to implement three different collision schemes: linear 
   probing, rehashing, and chaining.  Then implement a search 
   algorithm that is appropriate for each collision scheme.
 */
import java.util.*;
import javax.swing.*;
public class Hashing
{
   public static void main(String[] args)
   {
      int arrayLength = Integer.parseInt(JOptionPane.showInputDialog(
                         "Hashing!\n"+
                         "Enter the size of the array:  "));//20
      //int arrayLength = 6;
      //int numItems = 4; 

      //int arrayLength = 5;
      //int numItems = 10; 

      int numItems = Integer.parseInt(JOptionPane.showInputDialog(
                         "Add n items:  "));               //15
     
      int scheme = Integer.parseInt(JOptionPane.showInputDialog(
           "The Load Factor is " + (double)numItems/arrayLength +
           "\nWhich collision scheme?\n"+
           "1. Linear Probing\n" +
           "2. Rehashing\n"+
           "3. Chaining"));
      //int scheme = 3;

      Hashtable table = null;
      switch( scheme )
      {
         case 1:   
            table = new HashtableLinearProbe(arrayLength);
            break;
         case 2: 
            table = new HashtableRehash(arrayLength);
            break;
         case 3:  
            table = new HashtableChaining(arrayLength);
            break;
         default:  System.exit(0);    
      }
      for ( int i = 0; i < numItems; i++ )
      {
         table.add("Item" + i);
      }
      
      //System.exit( 1 );
      
      int itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1)));
      //int itemNumber = 2;


      //System.out.println( "DRIVER    itemNumber: " + itemNumber );
      while( itemNumber != -1 )
      {
         String key = "Item" + itemNumber;
         int index = table.indexOf(key);
         //System.out.println( "DRIVER    index: " + index );
         //itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       //"Search for:  Item0" + " to "+ "Item"+(numItems-1)));
         // found it
         if( index >= 0 )
         {
            System.out.println(key + " found  at index " + index);
            
            //itemNumber = -1;
         }
         else
         {
            System.out.println(key + " not found!");
            itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1))); 
            //System.exit( 1 );
         }
         itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1)));                           
      } 

      System.exit(0);
   }
}

/*********************************************/
interface Hashtable
{
   void add(Object obj);
   tree int indexOf(Object obj);
}
/***************************************************/ 

class HashtableLinearProbe implements Hashtable
{
   private Object[] array;
  
   public HashtableLinearProbe(int size)//constructor
   {
      array = new Object[ size ];
   }
   
   public void add( Object obj )
   {
      int code = obj.hashCode();
      int index = Math.abs( code % array.length );
      if ( array[ index ] == null )  //empty
      {
         //insert it
         array[ index ] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
      else //collision
      {
         System.out.println(obj + "\t" + code + "\tCollision at "+ index);
         index = linearProbe(index);
         array[index] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
   }  
   
   public int linearProbe( int index )
   {
      for ( int i = index + 1; i < array.length; i++ )
      {
         if ( array[ i ] == null )
         {
            return i;
         }
      }
      
      for ( int i = 0; i < index; i++ )
      {
         if ( array[ i ] == null )
         {
            return i;
         }
      }
      
      Object[] tempArray = new Object[ array.length ];
      for ( int i = 0; i < array.length; i++ )
      {
         tempArray[ i ] = array[ i ];
      }
      
      //System.out.println( "DOUBLE--------" );
      array = new Object[ array.length * 2 ];
      for ( int i = 0; i < tempArray.length; i++ )
      {
         add( tempArray[ i ] );
      }
      
      //index = Math.abs( code % array.length );
      
      return linearProbe( index );
   }
   
   public int indexOf( Object obj )     
   {
      int index = Math.abs(obj.hashCode() % array.length);
      //System.out.println( "indexOf    index: " + index );
      while( array[index] != null)
      {
         if ( array[ index ] == obj )  //found it
         {
            return index;
         }
         else //search for it in a linear probe manner
         {
            //System.out.println("Looking at index " + index);
            for ( int i = index; i < array.length; i++ )
            {
               //System.out.println("array[ " + i + " ]: " + array[ i ] );
               if ( array[ i ].equals( obj ) )
               {
                  return i;
               }
            }
            //System.out.println("indexOf    index: " + index );
            for ( int i = 0; i < index; i++ )
            {
               //System.out.println("array[ " + i + " ]: " + array[ i ] );
               if ( array[ i ].equals( obj ) )
               {
                  return i;
               }
            }
            
            //not found
      
            return -1;
            
         }
      }
      //not found
      
      return -1;
   }
}

/*****************************************************/
class HashtableRehash implements Hashtable
{
   private int LIMITFACTOR = 3;
   private Object[] array;
   private int constant;  
   private int rehashLimit;
   private int rehashCount = 0;
   
   public HashtableRehash(int size) //constructor
   {
      array = new Object[ size ];
      constant = getConstant( size );
      rehashLimit = getRehashLimit();
      //System.out.println( "HashtableRehash.CONSTRUCTOR    constant: " + constant );
      //System.out.println( "HashtableRehash.CONSTRUCTOR    rehashLimit: " + rehashLimit );
   }
   
   public int getRehashLimit ()
   {
      return Math.abs( array.length / LIMITFACTOR ) + 1;
   }
   
   public int getConstant( int size )
   {
      //System.out.println( "HashtableRehash.getConstant    size: " + size );
      List<Integer> factors = new ArrayList<Integer>(); 
      for ( int i = 2; i < size; i++ )
      {
         boolean isRelativePrime = true;
         if ( size % i == 0 )
         {
            factors.add( i );
         }
         
         for ( int k = 0; k < factors.size(); k++ )
         {
            if ( i % factors.get( k ) == 0 )
            {
               isRelativePrime = false;
            }   
         }
         
         if ( isRelativePrime )
         {
            return i;
         }         
      }
      
      return -1;
   }
   
   public void add(Object obj)
   {
      int code = obj.hashCode();
      int index = Math.abs(code % array.length);
      if ( array[ index ] == null )  //empty
      {
         //insert it
         System.out.println(obj + "\t" + code + "\t" + index);
         array[ index ] = obj;
      }
      else //collision
      {
         System.out.println(obj + "\t" + code + "\tCollision at "+ index);
         index = rehash(index);
         array[index] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
   }  
   
   public int rehash( int index )
   {
      //System.out.println( "HashtableRehash.rehash    -------- index: " + index + " --------" );

      int address = ( index + constant ) % array.length;      
      if ( array[ address ] == null )
      {
         return address;
      }
      
      // DOUBLE ARRAY IF rehash LIMIT EXCEEDED
      rehashCount++;
      //System.out.println( "HashtableRehash.rehash    -------- rehashCount: " + rehashCount + " --------" );
      
      if ( rehashCount > rehashLimit )
      {
         doubleArray();
         //System.out.println( "HashtableRehash.rehash    rehashLimit: " + rehashLimit );
         rehashCount = 0;
         //System.out.println( "HashtableRehash.rehash    -------- rehashCount: " + rehashCount + " --------" );
      }
      
      return rehash( address );
   }
   
   public void doubleArray ()
   {
      //System.out.println( "HashtableRehash.doubleArray--------" );
      Object[] tempArray = new Object[ array.length ];
      for ( int i = 0; i < array.length; i++ )
      {
         tempArray[ i ] = array[ i ];
      }
      
      array = new Object[ array.length * 2 ];
      for ( int i = 0; i < tempArray.length; i++ )
      {
         if ( tempArray[ i ] != null )
         {
            add( tempArray[ i ] );
         }
      }      

      rehashLimit = getRehashLimit();
      constant = getConstant( array.length );
   }
   
   public int indexOf(Object obj)
   {
      //System.out.println( "HashtableRehash.indexOf    -------- obj: " + obj + " --------" );

      int index = Math.abs(obj.hashCode() % array.length);
      //System.out.println( "HashtableRehash.indexOf    index: " + index  );
      while (array[index] != null)
      {
         //System.out.println( "HashtableRehash.indexOf    index: " + index );
         //System.out.println( "HashtableRehash.indexOf    array[ " + index + "]: " + array[ index ]  );
         if ( array[ index ].equals( obj ) )  //found it
         {
            //System.out.println( "HashtableRehash.indexOf    FOUND" );
            return index;
         }
         else //search for it in a rehashing manner
         {
            index = rehash( index );
            //System.out.println("Looking at index " + index);
         }
      }
      
      //not found
      return -1;
   }
}

/********************************************************/
class HashtableChaining implements Hashtable
{
   
   private LinkedList[] array;
   
   public HashtableChaining( int size )
   {
      //instantiate the array
      //instantiate the LinkedLists
      array = new LinkedList[ size ];
      for ( int i = 0; i < size; i++ )
      {
         array[ i ] = new LinkedList();
                            
      }
   }
   public void add(Object obj)
   {
      int code = obj.hashCode();
      int index = Math.abs(code % array.length);
      array[index].addFirst(obj);
      System.out.println(obj + "\t" + code + " " + " at " +index + ": "+ array[index]);
   }  
   
   public int indexOf(Object obj)
   {
      //System.out.println( "HashtableChaining.indexOf    obj: " + obj + " ----------" );
      
      int index = Math.abs(obj.hashCode() % array.length);
      //System.out.println( "HashtableChaining.indexOf    index: " + index );
      if ( !array[index].isEmpty() )
      {
         if ( array[ index ].getFirst().equals( obj ) )  //found it
         {
            //System.out.println( "HashtableChaining.indexOf    FOUND" );
            return index;            
         }
         else //search for it in a chaining manner
         {
            //System.out.println( "HashtableChaining.indexOf    DOING CHAINING" );
            
            LinkedList tempList = new LinkedList();
            Object tempObject = new Object();
            int foundIndex = -1;
            //System.out.println( "HashtableChaining.indexOf    array[ " + index  + " ].getFirst(): " + array[ index ].getFirst() );
            while ( ! array[ index ].getFirst().equals( obj ) )
            {
               tempList.addFirst( array[ index ].removeFirst() );         
               //System.out.println( "HashtableChaining.indexOf    array[ " + index  + " ].getFirst(): " + array[ index ].getFirst() );

               if ( array[ index ].getFirst().equals( obj ) )
               {
                  foundIndex = index;
               }
            }
            //System.out.println( "HashtableChaining.indexOf    foundIndex: " + foundIndex );
            
            // REVERSE BACK ONTO LinkedList
            while ( ! tempList.isEmpty() )
            {
               array[ index ].addFirst( tempList.removeLast() );
            }
        
            if ( foundIndex != -1 )
            {
               return foundIndex;
            }
         }
      }
      
      //not found
      return -1;
   }
}