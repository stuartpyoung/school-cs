 // Name: Shaw Young     Date: 4/8/19

/* 
   Assignment:  This hashing program results in collisions.
   You are to implement three different collision schemes: linear 
   probing, rehashing, and chaining.  Then implement a search 
   algorithm that is appropriate for each collision scheme.
 */
import java.util.*;
import javax.swing.*;
public class Hashing
{
   public static void main(String[] args) {
      int arrayLength = Integer.parseInt(JOptionPane.showInputDialog(
                         "Hashing!\n"+
                         "Enter the size of the array:  ")); // 20

      int numItems = Integer.parseInt(JOptionPane.showInputDialog(
                         "Add n items:  "));                 // 15
     
      int scheme = Integer.parseInt(JOptionPane.showInputDialog(
           "The Load Factor is " + (double)numItems/arrayLength +
           "\nWhich collision scheme?\n"+
           "1. Linear Probing\n" +
           "2. Rehashing\n"+
           "3. Chaining"));

      Hashtable table = null;
      switch( scheme ) {
         case 1:   
            table = new HashtableLinearProbe(arrayLength);
            break;
         case 2: 
            table = new HashtableRehash(arrayLength);
            break;
         case 3:  
            table = new HashtableChaining(arrayLength);
            break;
         default:  System.exit(0);    
      }
      for ( int i = 0; i < numItems; i++ ) {
         table.add("Item" + i);
      }
      
      int itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1)));

      while( itemNumber != -1 ) {
         String key = "Item" + itemNumber;
         int index = table.indexOf(key);

         // found it
         if( index >= 0 ) {
            System.out.println(key + " found  at index " + index);
         }
         else {
            System.out.println(key + " not found!");
            itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1))); 
            //System.exit( 1 );
         }
         itemNumber = Integer.parseInt(JOptionPane.showInputDialog(
                       "Search for:  Item0" + " to "+ "Item"+(numItems-1)));                           
      } 

      System.exit(0);
   }
}

/*********************************************/
interface Hashtable
{
   void add(Object obj);
   int indexOf(Object obj);
}
/***************************************************/ 

class HashtableLinearProbe implements Hashtable
{
   private Object[] objectList;
  
   public HashtableLinearProbe(int size)//constructor
   {
      objectList = new Object[ size ];
   }
   
   public void add( Object obj ) {
      int code = obj.hashCode();
      int index = Math.abs( code % objectList.length );
      if ( objectList[ index ] == null )  //empty
      {
         //insert it
         objectList[ index ] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
      else //collision
      {
         System.out.println(obj + "\t" + code + "\tCollision at "+ index);
         index = linearProbe(index);
         objectList[index] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
   }  
   
   public int linearProbe( int index ) {

      // RETURN i IF FOUND FROM INDEX TO END OF LIST
      for ( int i = index + 1; i < objectList.length; i++ ) {
         if ( objectList[ i ] == null ) {
            return i;
         }
      }
      
      // RETURN i IF FOUND FROM START OF LIST TO INDEX
      for ( int k = 0; k < index; k++ ) {
         if ( objectList[ k ] == null ) {
            return k;
         }
      }
      
      // OTHERWISE, DOUBLE LIST SIZE AND RETRY linearProbe
      Object[] tempObjectList = new Object[ objectList.length ];
      for ( int i = 0; i < objectList.length; i++ ) {
         tempObjectList[ i ] = objectList[ i ];
      }
      
      objectList = new Object[ objectList.length * 2 ];
      for ( int i = 0; i < tempObjectList.length; i++ ) {
         add( tempObjectList[ i ] );
      }
      
      return linearProbe( index );
   }
   
   public int indexOf( Object object ) {

      int index = Math.abs( object.hashCode() % objectList.length );

      while( objectList[index] != null) {

         // found
         if ( objectList[ index ] == object ) { 
            return index;
         }
         // search for it in a linear probe manner
         else {               
            System.out.println("Looking at index " + index);
            for ( int i = index; i < objectList.length; i++ ) {
               if ( object.equals( objectList[ i ] ) ) {
                  return i;
               }
            }
            for ( int i = 0; i < index; i++ ) {
               if ( object.equals( objectList[ i ] ) ) {
                  return i;
               }
            }            
            //not found

            return -1;            
         }
      }
      //not found
      
      return -1;
   }
}

/*****************************************************/
class HashtableRehash implements Hashtable
{
   private Object[] objectList;
   private int rehashCount = 0;
   private int maxRehashes;
   private int constant;  
   private int MAXFACTORS = 3;
   
   // constructor
   public HashtableRehash(int size) {
      maxRehashes = Math.abs( objectList.length / MAXFACTORS ) + 1;
      constant = calculateConstant( size );
      objectList = new Object[ size ];
   }
   
   public void add(Object obj) {
      int code = obj.hashCode();
      int index = Math.abs(code % objectList.length);

      // INSERT IF empty
      if ( objectList[ index ] == null ) { 
         //insert it
         System.out.println(obj + "\t" + code + "\t" + index);
         objectList[ index ] = obj;
      }
      // OTHERWISE, IT'S A COLLISION
      else {
         System.out.println(obj + "\t" + code + "\tCollision at "+ index);
         index = rehash(index);
         objectList[index] = obj;
         System.out.println(obj + "\t" + code + "\t" + index);
      }
   }  
   
   public int rehash( int index ) {
      int offset = index + constant;
      int hashLocation = offset % objectList.length;      
      if ( objectList[ hashLocation ] == null ) {
         return hashLocation;
      }
      
      rehashCount++;
      if ( rehashCount > maxRehashes ) {
         doubleObjectList();
         rehashCount = 0;
      }
      
      return rehash( hashLocation );
   }
   
   public void doubleObjectList () {
      // COPY objectList
      Object[] tempObjectList = copyList( objectList );

      // DOUBLE AND CLEAR objectList      
      objectList = new Object[ objectList.length * 2 ];

      for ( int i = 0; i < tempObjectList.length; i++ ) {
         if ( tempObjectList[ i ] != null ) {
            add( tempObjectList[ i ] );
         }
      }      

      maxRehashes = Math.abs( objectList.length / MAXFACTORS ) + 1;
      constant = calculateConstant( objectList.length );
   }
   
   public int calculateConstant( int size ) {
      List<Integer> factors = new ArrayList<Integer>(); 

      for ( int i = 2; i < size; i++ ) {
         boolean relativePrimeFound = true;
         if ( size % i == 0 ) {
            factors.add( i );
         }
         
         for ( int k = 0; k < factors.size(); k++ ) {
            if ( i % factors.get( k ) == 0 ) {
               relativePrimeFound = false;
            }   
         }
         
         if ( relativePrimeFound ) {
            return i;
         }         
      }
      
      return -1;
   }
      
   public int indexOf( Object object ) {
      int index = Math.abs( object.hashCode() % objectList.length );
      while ( objectList[index] != null ) {
         // found it
         if ( object.equals( objectList[ index ] )  ) {
            return index;
         }
         // search for it in a rehashing manner
         else {
            index = rehash( index );
         }
      }
      
      //not found
      return -1;
   }

   public Object[] copyList ( Object[] objectList ) {
      Object[] objectListCopy = new Object[ objectList.length ];
      for ( int i = 0; i < objectList.length; i++ ) {
         objectListCopy[ i ] = objectList[ i ];
      }

      return objectListCopy;      
   }
}

/********************************************************/
class HashtableChaining implements Hashtable
{
   
   private LinkedList[] objectList;
   
   public HashtableChaining( int size ) {
      //instantiate the objectList
      objectList = new LinkedList[ size ];

      //instantiate the LinkedLists
      for ( int i = 0; i < size; i++ ) {
         objectList[ i ] = new LinkedList();
      }
   }

   public void add( Object object ) {
      int code = object.hashCode();
      int index = Math.abs(code % objectList.length);
      objectList[index].addFirst( object );
      System.out.println(object + "\t" + code + " " + " at " +index + ": "+ objectList[index]);
   }  
   
   public int indexOf(Object object) {
      int index = Math.abs( object.hashCode() % objectList.length );
      if ( ! objectList[index].isEmpty() ) {

         // found it
         if ( objectList[ index ].getFirst().equals( object ) ) {
            return index;            
         }
         // search for it in a chaining manner
         else {
            return objectList[ index ].indexOf( object );
         }
      }
      
      //not found
      return -1;
   }
}