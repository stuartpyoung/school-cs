// Name: Rio Young
// Date: 3/7/19

import java.util.*;
import java.io.*;

public class SetsOfLetters
{
   public static String lowercase = "abcdefghijklmnopqrstuvwxyz";
   public static String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   public static String symbols = ",.;:/";
   
   public static void main(String[] args) throws FileNotFoundException
   {
      String fileName = "declarationLast.txt";
      fillTheSets(fileName);
   }
   
   public static void fillTheSets( String fn ) throws FileNotFoundException
   {
      Scanner infile = new Scanner(new File(fn));
      /*  enter your code here  */
      //List<String> array = new ArrayList<String>();
      ArrayList<TreeSet> firstLine = parseLine( infile.nextLine() );      
      //System.out.println( "fillTheSets   firstLine: "  + firstLine );
      TreeSet<String> commonLowerCaseSet = firstLine.get( 0 );
      TreeSet<String> commonUpperCaseSet = firstLine.get( 1 );
      TreeSet<String> commonSymbolSet = firstLine.get( 2 );
      System.out.println( "fillTheSets   commonLowerCaseSet: "  + commonLowerCaseSet );
      System.out.println( "fillTheSets   commonUpperCaseSet: "  + commonUpperCaseSet );
      System.out.println( "fillTheSets   commonSymbolSet: "  + commonSymbolSet );
       
      while ( infile.hasNextLine() )
      {
         //array.add( infile.nextLine() );
         String line = infile.nextLine(); 
         ArrayList<TreeSet> currentLine = parseLine( line );
         
         //System.out.println( "fillTheSets   currentLine: "  + currentLine );
         TreeSet<String> currentLowerCaseSet = currentLine.get( 0 );
         TreeSet<String> currentUpperCaseSet = currentLine.get( 1 );
         TreeSet<String> currentSymbolSet = currentLine.get( 2 );
      
         System.out.println( line );
         System.out.println( "Lower Case: " + currentLowerCaseSet );
         System.out.println( "Upper Case: " + currentUpperCaseSet );
         System.out.println( "Other: " +  currentSymbolSet );
         System.out.println( );
         
         
         commonLowerCaseSet = intersection( commonLowerCaseSet, currentLowerCaseSet );
         commonUpperCaseSet = intersection( commonUpperCaseSet, currentUpperCaseSet );
         commonSymbolSet = intersection( commonSymbolSet, currentSymbolSet );         
      
         //System.out.println( "fillTheSets   commonLowerCaseSet: "  + commonLowerCaseSet );
         //System.out.println( "fillTheSets   commonUpperCaseSet: "  + commonUpperCaseSet );
         //System.out.println( "fillTheSets   commonSymbolSet: "  + commonSymbolSet );
         
      }
      
      System.out.println( "Common Lower Case: "  + commonLowerCaseSet );
      System.out.println( "Common Upper Case: "  + commonUpperCaseSet );
      System.out.println( "Common Other: "  + commonSymbolSet );
      
      
      //for ( int i = 0; array.get( i ) != null; i++ )
      //{
      //   ArrayList<Set> thisLine = parseLine ( infile.nextLine() );
      //   Set<String> lowerCaseSet = parseLine[ 0 ];
      //   Set<String> upperCaseSet = parseLine[ 1 ];
      //   Set<String> symbolSet = parseLine[ 2 ];
      //   //Set<String> set = new TreeSet<String>();
      //   //List<String> array = new ArrayList<String>();
      //   //String[] letters = array.get( i ).split( "(?!^)" );
      //   //for ( int k = 0;  )
      //   //System.out.println( Arrays.toString( letters ) );
      //   ////System.out.println( array.get( i ) );
      //   
      //   
      //   // COMPARE COMMON SETS WITH THIS LINE SETS
         
         
      //}
      
   }
   
   public static TreeSet<String> intersection( TreeSet<String> firstSet, TreeSet<String> secondSet )
   {
      Set<String> lowerCaseSet = new TreeSet<String>();
      Iterator<String> firstIterator = firstSet.iterator();
      Iterator<String> secondIterator = secondSet.iterator();
      TreeSet<String> commonSet = new TreeSet<String>();
      while ( firstIterator.hasNext() )
      {
         String firstCharacter = firstIterator.next();
         secondIterator = secondSet.iterator();
         //System.out.println( "intersection   firstCharacter: " + firstCharacter );
         while ( secondIterator.hasNext() )
         {
            String secondCharacter = secondIterator.next();
            //System.out.println( "intersection   secondCharacter: " + secondCharacter );
            if ( firstCharacter.equals( secondCharacter ) )
            {
               commonSet.add( firstCharacter );
               break;
            }
         }
      }

      return commonSet;
   }
   
   public static ArrayList<TreeSet> parseLine ( String line )
   {
      TreeSet<String> lowerCaseSet = new TreeSet<String>();
      TreeSet<String> upperCaseSet = new TreeSet<String>();
      TreeSet<String> symbolSet = new TreeSet<String>();
       //iterate through characters
      for ( int i = 0; i < line.length(); i++ )
      {
         String character = line.substring( i, i + 1 );
         //System.out.println( "parseLine   character: "  + character );
         if ( lowercase.indexOf( character ) != -1 )
         {
            //System.out.println( "parseLine   LOWERCASE: "  + character );
            lowerCaseSet.add( character );
         }
         else if ( uppercase.indexOf( character ) != -1 )
         {
            //System.out.println( "parseLine   UPPERCASE: "  + character );
            upperCaseSet.add( character );
         }
         else if ( ! character.equals( " " ) )
         {
            //System.out.println( "parseLine   SYMBOL: "  + character );
            symbolSet.add( character );            
         }
      }  
      
      ArrayList<TreeSet> arrayList = new ArrayList<TreeSet>();
      arrayList.add( lowerCaseSet );
      arrayList.add( upperCaseSet );
      arrayList.add( symbolSet );
      
      return arrayList;
   }
   
}

/***********************************
  ----jGRASP exec: java SetsOfLetters_teacher
 
 We, therefore, the Representatives of the united States of 
 Lower Case: [a, d, e, f, h, i, n, o, p, r, s, t, u, v]
 Upper Case: [R, S, W]
 Other: [ , ,]
 
 America, in General Congress, Assembled, appealing to the 
 Lower Case: [a, b, c, d, e, g, h, i, l, m, n, o, p, r, s, t]
 Upper Case: [A, C, G]
 Other: [ , ,]
 
 Supreme Judge of the world for the rectitude of our intentions,
 Lower Case: [c, d, e, f, g, h, i, l, m, n, o, p, r, s, t, u, w]
 Upper Case: [J, S]
 Other: [ , ,]
 
 do, in the Name, and by the Authority of the good People of 
 Lower Case: [a, b, d, e, f, g, h, i, l, m, n, o, p, r, t, u, y]
 Upper Case: [A, N, P]
 Other: [ , ,]
 
 these Colonies, solemnly publish and declare, That these United 
 Lower Case: [a, b, c, d, e, h, i, l, m, n, o, p, r, s, t, u, y]
 Upper Case: [C, T, U]
 Other: [ , ,]
 
 Colonies are, and of Right ought to be Free and Independent 
 Lower Case: [a, b, d, e, f, g, h, i, l, n, o, p, r, s, t, u]
 Upper Case: [C, F, I, R]
 Other: [ , ,]
 
 States; that they are Absolved from all Allegiance to the 
 Lower Case: [a, b, c, d, e, f, g, h, i, l, m, n, o, r, s, t, v, y]
 Upper Case: [A, S]
 Other: [ , ;]
 
 British Crown, and that all political connection between them 
 Lower Case: [a, b, c, d, e, h, i, l, m, n, o, p, r, s, t, w]
 Upper Case: [B, C]
 Other: [ , ,]
 
 and the State of Great Britain, is and ought to be totally 
 Lower Case: [a, b, d, e, f, g, h, i, l, n, o, r, s, t, u, y]
 Upper Case: [B, G, S]
 Other: [ , ,]
 
 dissolved; and that as Free and Independent States, they have 
 Lower Case: [a, d, e, h, i, l, n, o, p, r, s, t, v, y]
 Upper Case: [F, I, S]
 Other: [ , ,, ;]
 
 full Power to levy War, conclude Peace, contract Alliances, 
 Lower Case: [a, c, d, e, f, i, l, n, o, r, s, t, u, v, w, y]
 Upper Case: [A, P, W]
 Other: [ , ,]
 
 establish Commerce, and to do all other Acts and Things which 
 Lower Case: [a, b, c, d, e, g, h, i, l, m, n, o, r, s, t, w]
 Upper Case: [A, C, T]
 Other: [ , ,]
 
 Independent States may of right do. And for the support of this 
 Lower Case: [a, d, e, f, g, h, i, m, n, o, p, r, s, t, u, y]
 Upper Case: [A, I, S]
 Other: [ , .]
 
 Declaration, with a firm reliance on the protection of divine 
 Lower Case: [a, c, d, e, f, h, i, l, m, n, o, p, r, t, v, w]
 Upper Case: [D]
 Other: [ , ,]
 
 Providence, we mutually pledge to each other our Lives, our 
 Lower Case: [a, c, d, e, g, h, i, l, m, n, o, p, r, s, t, u, v, w, y]
 Upper Case: [L, P]
 Other: [ , ,]
 
 Fortunes and our sacred Honor.
 Lower Case: [a, c, d, e, n, o, r, s, t, u]
 Upper Case: [F, H]
 Other: [ , .]
 
 Common Lower Case: [d, e, n, o, r, t]
 Common Upper Case: []
 Common Other: [ ]
  ----jGRASP: operation complete.
  ************************************/