// Name: Rio Young
// Date: 3/11/19

import java.io.*;
import java.util.*;  

/* This program takes a text file, creates an index (by line numbers)
 * for all the words in the file and writes the index
 * into the output file.  The program prompts the user for the file names.
 */
public class IndexMakerMap
{
   public static void main(String[] args) throws IOException
   {
      Scanner keyboard = new Scanner(System.in);
      System.out.print("\nEnter input file name: ");
      String infileName = keyboard.nextLine().trim();
      Scanner inputFile = new Scanner(new File(infileName));
      //Scanner inputFile = new Scanner( new File( "fish3.txt" ) );
      
      DocumentIndex index = makeIndex(inputFile);
      
      //System.out.println( index.toString() );
      PrintWriter outputFile = new PrintWriter(new FileWriter("fishIndex.txt"));
      outputFile.println(index.toString());
      inputFile.close(); 						
      outputFile.close();
      System.out.println("Done.");
   }
   
   public static DocumentIndex makeIndex(Scanner inputFile)
   {
      DocumentIndex index = new DocumentIndex(); 	
      int lineNumber = 0;
      while(inputFile.hasNextLine())
      {
         lineNumber++;
         index.addAllWords(inputFile.nextLine(), lineNumber);

            //if ( lineNumber  == 3 )
            //{
            //   System.out.println( "DEBUG EXIT" );
            //   System.exit( 1 );
            //}
      }
      return index;  
   }
}

class DocumentIndex extends TreeMap<String, TreeSet<Integer>>
{
  
  /** Extracts all the words from str, skipping punctuation and whitespace
   *  and for each word calls addWord(word, num).  A good way to skip punctuation
   *  and whitespace is to use String's split method, e.g., split("[., \"!?]") 
   */
   public void addAllWords(String string, int number) 
   {
      //System.out.println( "addAllWords      number: " + number + ", string: " + string);
      string = string.replaceAll( "^[^A-Za-z]+", "" );
      if ( string.equals( "" ) )
      {
         return;
      }
      String[] words = string.split( "[,./;:'\"?<>\\[\\]{}|`~!@#$%^&*()\\s]+" );      
      //print( words );
      for ( int i = 0; i < words.length; i++ )
      {
         //System.out.println( "addAllWords     words[" + i + "]: " + words[ i ] );
         addWord( words[ i ], number );
      }
   }

  /** Makes the word uppercase.  If the word is already in the map, updates the lineNum.
   *  Otherwise, adds word and ArrayList to the map, and updates the lineNum   
   */
   public void addWord(String word, int number)
   {
      word = word.toUpperCase();
      //System.out.println( "addWord     number: " + number + ", word: " + word );
         
      TreeSet<Integer> treeSet = this.get( word );
      //System.out.println( "addWord     treeSet: " + treeSet );
      
      if ( treeSet == null )
      {
         //System.out.println( "addWord      NO ENTRY for word: " + word + ". Adding to DocumentIndex." );
         TreeSet<Integer> tempTreeSet = new TreeSet<Integer>();
         //System.out.println( "addWord      tempTreeSet: " + tempTreeSet );
         tempTreeSet.add( number );
         put( word, tempTreeSet );         
      }
      else {
         //System.out.println( "addWord      ENTRY FOUND for: " + word );
         treeSet.add( number );
         put( word, treeSet );
      }
      TreeSet<Integer> existingTreeSet = get( word );
      //System.out.println( "addWord      existingTreeSet: " + existingTreeSet );
   }
   
   public String toString()
   {
      Set<String> keys = keySet();
      String output = "";
      //System.out.println( "toString   keys: " + keys );
      Iterator<String> iterator = keys.iterator();
      while ( iterator.hasNext() )
      {
         String word = iterator.next();
         //String string = get( word );
         
         System.out.print( word );
         output = output + word + " ";
         String getWord = get( word ) + "";
         getWord = getWord.replace( "[", " " );
         getWord = getWord.replace( "]", "" );
         output = output + getWord + System.getProperty("line.separator");
         System.out.println( getWord );
      }
      //String output = "";
      //while ( iterator.hasNext() )
      //{
      //   System.out.println( this.get( iterator.next() ) );
      //}
      return output;
   }
}

/**********************************************
     ----jGRASP exec: java -ea IndexMakerMap
 
 Enter input file name: fish.txt
 Done.
 
  ----jGRASP: operation complete.
  
************************************************/
/****************** fishIndex.txt  **************
A 12, 14, 15
ARE 16
BLACK 6
BLUE 4, 7
CAR 14
FISH 1, 2, 3, 4, 6, 7, 8, 9, 16
HAS 11, 14
LITTLE 12, 14
LOT 15
NEW 9
OF 16
OLD 8
ONE 1, 11, 14
RED 3
SAY 15
STAR 12
THERE 16
THIS 11, 14
TWO 2
WHAT 15   
   ************************/
