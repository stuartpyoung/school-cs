 // Name: Shaw Young    Date: 4/8/19

import java.util.*;

public class Polynomial_Driver
{
   public static void main(String[] args) {
      Polynomial poly = new Polynomial();    // 2x^3 + -4x + 2
      poly.makeTerm(1, -4);
      poly.makeTerm(3, 2);
      poly.makeTerm(0, 2);
      System.out.println("Map:  " + poly.getMap());
      System.out.println("String:  " + poly.toString());
      double xValue = 2.0; //10
      System.out.println("Evaluated at "+ xValue +": " +poly.evaluateAt( xValue ) );
      
      System.out.println("-----------");
      Polynomial poly2 = new Polynomial();  // 2x^4 + x^2 + -5x + -3
      poly2.makeTerm(1, -5);
      poly2.makeTerm(4, 2);
      poly2.makeTerm(0, -3);
      poly2.makeTerm(2, 1);
      System.out.println("Map:  " + poly2.getMap()); 
      System.out.println("String:  " +poly2.toString());
      double xValue2 = -10.5; //24469.875
      System.out.println("Evaluated at "+ xValue2 +": " +poly2.evaluateAt( xValue2 ) );
      
      
      System.out.println("-----------");
      System.out.println("Sum: " + poly.add(poly2));
      System.out.println("Product:  " + poly.multiply(poly2));
      
      /*  Another case:   (x+1)(x-1) -->  x^2 + -1    */
       System.out.println("===========================");
       Polynomial poly3 = new Polynomial();   // (x+1)
       poly3.makeTerm(1, 1);
       poly3.makeTerm(0, 1);
       System.out.println("Map:  " + poly3.getMap());
       System.out.println("String:  " + poly3.toString());
          
       Polynomial poly4 = new Polynomial();    // (x-1)
       poly4.makeTerm(1, 1);
       poly4.makeTerm(0, -1);
       System.out.println("Map:  " + poly4.getMap());
       System.out.println("String:  " + poly4.toString());
       System.out.println("Product:  " + poly4.multiply(poly3));   // x^2 + -1 
       
       /*  testing the one-arg constructor  */
       System.out.println("==========================="); 
       Polynomial poly5 = new Polynomial("2x^3 + 4x^2 + 6x^1 + -3");
       System.out.println("Map:  " + poly5.getMap());  
       System.out.println(poly5);

   }
}
interface PolynomialInterface
{
   public void makeTerm(Integer exp, Integer coef);
   public Map<Integer, Integer> getMap();
   public double evaluateAt(double x);
   public Polynomial add(Polynomial other);
   public Polynomial multiply(Polynomial other);
   public String toString();
}

class Polynomial implements PolynomialInterface
{
   public final TreeMap<Integer, Integer> polynomialTreeMap;
   
   public Polynomial() {
      polynomialTreeMap = new TreeMap<Integer, Integer>();
   }
   
   public Polynomial( String input ) {
      polynomialTreeMap = new TreeMap<Integer, Integer>();
      String[] terms = input.split( " \\+ " );
      for ( String term : terms ) {
         if ( term.indexOf("^") == -1 ) {
            polynomialTreeMap.put( 0, Integer.parseInt( term ) );
         }
         else {
            String[] components = term.split( "x\\^" ); 
            int coefficient = Integer.parseInt( components[ 0 ] );
            int exponent    = Integer.parseInt( components[ 1 ] ); 
            makeTerm( exponent, coefficient );
         }
      }
   }
   
   public void makeTerm( Integer exponent, Integer coefficient ) {
      polynomialTreeMap.put( exponent, coefficient );
   }
   
   public Map<Integer, Integer> getMap() {
      return polynomialTreeMap;
   }
   
   public double evaluateAt( double x ) {
      double result = 0;
      for ( int exponent : polynomialTreeMap.keySet() ) {
         int coefficient = polynomialTreeMap.get( exponent );
         result += coefficient * Math.pow( x, exponent );
      }

      return result;
   }
   
   public Polynomial add(Polynomial other) {
      Polynomial polynomial = new Polynomial();
      
      // ADD ALL EXPONENTS FOUND IN this.polynomialTreeMap WITH THOSE IN other.polynomialTreeMap
      for ( int exponent : polynomialTreeMap.keySet() ) {
        if ( other.polynomialTreeMap.containsKey( exponent ) ) {
          polynomial.polynomialTreeMap.put( exponent, ( polynomialTreeMap.get( exponent ) + other.polynomialTreeMap.get( exponent ) ) );
        }
        else {
          polynomial.polynomialTreeMap.put( exponent, polynomialTreeMap.get( exponent ) );
        }
      }
      
      // ADD REMAINING EXPONENTS IN other.polynomialTreeMap BUT NOT FOUND IN this.polynomialTreeMap 
      for ( int exponent : other.polynomialTreeMap.keySet() ) {
        if ( ! polynomial.polynomialTreeMap.containsKey( exponent ) ) {
          polynomial.polynomialTreeMap.put( exponent, other.polynomialTreeMap.get( exponent ) );
        }
      }
      
      return polynomial;
   }
   
   public Polynomial multiply(Polynomial other) {
      Polynomial polynomial = new Polynomial();
      
      for ( int exponent : polynomialTreeMap.keySet() ) {
         for ( int otherExponent : other.polynomialTreeMap.keySet() ) {
            if ( polynomial.polynomialTreeMap.containsKey( exponent + otherExponent ) ) {
               polynomial.polynomialTreeMap.put( 
                exponent + otherExponent, 
                polynomial.polynomialTreeMap.get( exponent + otherExponent ) 
                + polynomialTreeMap.get( exponent ) 
                * other.polynomialTreeMap.get( otherExponent ) 
                  
                );
            }
            else {
               polynomial.polynomialTreeMap.put( 
                exponent + otherExponent,  
                other.polynomialTreeMap.get( otherExponent) 
                * polynomialTreeMap.get( exponent ) );
            }
         }
      }
      return polynomial;
   }
   
   public String toString() {
      String output = "";
      for ( int exponent : polynomialTreeMap.keySet() ) {

         int coefficient = polynomialTreeMap.get( exponent );
        
         if ( coefficient == 0 ) {
            continue;
         }
         else if ( coefficient == 1 ) {
            output = " + " + "x^" + exponent + " " + output;
         }
         else if ( exponent == 0 ) {
            output = " + " + coefficient + " " + output;
         }
         else if ( exponent == 1 ) {
            if ( coefficient == 1 ) {
               output = " + " + "x" + " " + output;
            }
            else {
               output = " + " + coefficient + "x" + " " + output;
            }
         }
         else {
            output = " + " + coefficient + "x^" + exponent + " " + output;
         }
         
      }
      output = output.substring( 3, output.length() );
      
      return output;
   }
   
}


/***************************************  
  ----jGRASP exec: java Polynomial_teacher
 Map:  {0=2, 1=-4, 3=2}
 String:  2x^3 + -4x + 2
 Evaluated at 2.0: 10.0
 -----------
 Map:  {0=-3, 1=-5, 2=1, 4=2}
 String:  2x^4 + x^2 + -5x + -3
 Evaluated at -10.5: -2271.25
 
 -----------
 Sum: 2x^4 + 2x^3 + x^2 + -9x + -1
 Product:  4x^7 + -6x^5 + -6x^4 + -10x^3 + 22x^2 + 2x + -6
 
  ----jGRASP: operation complete.
 ********************************************/