 // Name: Rio Young
 // Date: 3/14/19

import java.util.*;

public class Polynomial_Driver
{
   public static void main(String[] args)
   {
      Polynomial poly = new Polynomial();    // 2x^3 + -4x + 2
      poly.makeTerm(1, -4);
      poly.makeTerm(3, 2);
      poly.makeTerm(0, 2);
      System.out.println("Map:  " + poly.getMap());
      System.out.println("String:  " + poly.toString());
      double xValue = 2.0; //10
      System.out.println("Evaluated at "+ xValue +": " +poly.evaluateAt( xValue ) );
      
      System.out.println("-----------");
      Polynomial poly2 = new Polynomial();  // 2x^4 + x^2 + -5x + -3
      poly2.makeTerm(1, -5);
      poly2.makeTerm(4, 2);
      poly2.makeTerm(0, -3);
      poly2.makeTerm(2, 1);
      System.out.println("Map:  " + poly2.getMap()); 
      System.out.println("String:  " +poly2.toString());
      double xValue2 = -10.5; //24469.875
      System.out.println("Evaluated at "+ xValue2 +": " +poly2.evaluateAt( xValue2 ) );
      
      
      System.out.println("-----------");
      System.out.println("Sum: " + poly.add(poly2));
      System.out.println("Product:  " + poly.multiply(poly2));
      
      /*  Another case:   (x+1)(x-1) -->  x^2 + -1    */
       System.out.println("===========================");
       Polynomial poly3 = new Polynomial();   // (x+1)
       poly3.makeTerm(1, 1);
       poly3.makeTerm(0, 1);
       System.out.println("Map:  " + poly3.getMap());
       System.out.println("String:  " + poly3.toString());
          
       Polynomial poly4 = new Polynomial();    // (x-1)
       poly4.makeTerm(1, 1);
       poly4.makeTerm(0, -1);
       System.out.println("Map:  " + poly4.getMap());
       System.out.println("String:  " + poly4.toString());
       System.out.println("Product:  " + poly4.multiply(poly3));   // x^2 + -1 
       
       /*  testing the one-arg constructor  */
       System.out.println("==========================="); 
       Polynomial poly5 = new Polynomial("2x^3 + 4x^2 + 6x^1 + -3");
       System.out.println("Map:  " + poly5.getMap());  
       System.out.println(poly5);

   }
}
interface PolynomialInterface
{
   public void makeTerm(Integer exp, Integer coef);
   public Map<Integer, Integer> getMap();
   public double evaluateAt(double x);
   public Polynomial add(Polynomial other);
   public Polynomial multiply(Polynomial other);
   public String toString();
}

class Polynomial implements PolynomialInterface
{
   public final TreeMap<Integer, Integer> myPoly;
   
   public Polynomial()
   {
      myPoly = new TreeMap<Integer, Integer>();
   }
   
   public Polynomial( String input )
   {
      myPoly = new TreeMap<Integer, Integer>();
      String[] array = input.split( " \\+ " );
      for ( String value : array )
      {
         int index = value.indexOf("^");
         if ( index == -1 )
         {
            myPoly.put( 0, Integer.parseInt( value ) );
         }
         else
         {
            String coefficient = value.substring( 0, index - 1 );
            String exponent = value.substring( index + 1, value.length() );
            makeTerm( Integer.parseInt( exponent ), Integer.parseInt( coefficient ) );
         }
      }
   }
   
   public void makeTerm( Integer exponent, Integer coefficient )
   {
      myPoly.put( exponent, coefficient );
   }
   
   public Map<Integer, Integer> getMap()
   {
      return myPoly;
   }
   
   public double evaluateAt( double x )
   {
      double output = 0;
      for ( int exponent : myPoly.keySet() )
      {
         int coefficient = myPoly.get( exponent );
         //System.out.println( "evaluateAt   ****** exponent: " + exponent + " ******");
         //System.out.println( "evaluateAt    coefficient: " + coefficient );
         //System.out.println( "evaluateAt    coefficient * Math.pow( x, exponent ): " + coefficient * Math.pow( x, exponent ) );
         double value = coefficient * Math.pow( x, exponent );
         //System.out.println( "evaluateAt    value = " + coefficient + " * Math.pow( " + x + ", " + exponent + ") = " + value );
         output += value;
         //System.out.println( "evaluateAt    output: " + output );
      }
      return output;
   }
   
   public Polynomial add(Polynomial other)
   {
      Polynomial polynomial = new Polynomial();
      
      for ( int exponent : myPoly.keySet() )
      {
         if ( other.myPoly.get( exponent ) != null )
         {
            polynomial.myPoly.put( exponent, ( myPoly.get( exponent ) + other.myPoly.get( exponent ) ) );
         }
         else
         {
            polynomial.myPoly.put( exponent, myPoly.get( exponent ) );
         }
      }
      
      for ( int exponent : other.myPoly.keySet() )
      {
         if ( !polynomial.myPoly.containsKey( exponent ) )
         {
            polynomial.myPoly.put( exponent, other.myPoly.get( exponent ) );
         }
      }
      
      
      return polynomial;
   }
   
   public Polynomial multiply(Polynomial other)
   {
      Polynomial polynomial = new Polynomial();
      int tempCoefficient;
      
      for ( int exponent : myPoly.keySet() )
      {
         for ( int otherExponent : other.myPoly.keySet() )
         {
            if ( polynomial.myPoly.containsKey( exponent + otherExponent ) )
            {
               polynomial.myPoly.put( exponent + otherExponent, myPoly.get( exponent ) * other.myPoly.get( otherExponent) + polynomial.myPoly.get( exponent + otherExponent ) );
            }
            else
            {
               polynomial.myPoly.put( exponent + otherExponent, myPoly.get( exponent ) * other.myPoly.get( otherExponent) );
            }
         }
      }
      return polynomial;
   }
   
   public String toString()
   {
      String output = "";
      for ( int exponent : myPoly.keySet() )
      {
         int coefficient = myPoly.get( exponent );
         if ( exponent == 0 )
         {
            output = " + " + coefficient + " " + output;
         }
         else if ( coefficient == 0 )
         {
            continue;
         }
         else if ( coefficient == 1 )
         {
            output = " + " + "x^" + exponent + " " + output;
         }
         else if ( exponent == 1 )
         {
            if ( coefficient == 1 )
            {
               output = " + " + "x" + " " + output;
            }
            else
            {
               output = " + " + coefficient + "x" + " " + output;
            }
         }
         else
         {
            output = " + " + coefficient + "x^" + exponent + " " + output;
         }
         
      }
      output = output.substring( 3, output.length() );
      
      return output;
   }
   
}


/***************************************  
  ----jGRASP exec: java Polynomial_teacher
 Map:  {0=2, 1=-4, 3=2}
 String:  2x^3 + -4x + 2
 Evaluated at 2.0: 10.0
 -----------
 Map:  {0=-3, 1=-5, 2=1, 4=2}
 String:  2x^4 + x^2 + -5x + -3
 Evaluated at -10.5: -2271.25
 
 -----------
 Sum: 2x^4 + 2x^3 + x^2 + -9x + -1
 Product:  4x^7 + -6x^5 + -6x^4 + -10x^3 + 22x^2 + 2x + -6
 
  ----jGRASP: operation complete.
 ********************************************/