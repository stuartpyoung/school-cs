// Name: Rio Young
// Date: 4/9/19
import java.util.*;
import java.io.*;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.Queue;
import java.util.Random;

public class Huffman
{
   public static Scanner keyboard = new Scanner(System.in);
   public static void main(String[] args) throws IOException
   {
      // //Prompt for two strings 
      // System.out.print("Encoding using Huffman codes");
      // System.out.print("\nWhat message? ");
      // String message = keyboard.nextLine();

      // System.out.print("\nEnter middle part of filename:  ");
      // String middlePart = keyboard.next();
   
   // maips
      String message = "Mississippi Map";
      String expected = "01010111110111110000010011001001110011";
      String middlePart = "maips-newest";
   

      huffmanize( message, middlePart );
   }

   @SuppressWarnings("unchecked")
   public static void huffmanize(String message, String middlePart) throws IOException
   {
      // System.out.println( "huffmanize    message: " + message );

      // MAKE A FREQUENCY TABLE OF THE LETTERS
      TreeMap treeMap = makeFrequencyTable ( message );
      // System.out.println( "huffmanize    treeMap: " + treeMap );

      // PUT EACH LETTER-FREQUENCY PAIR INTO A HuffmanTreeNode  
      Set<String> keys = treeMap.keySet();
      PriorityQueue<HuffmanTreeNode> priorityQueue = new PriorityQueue<HuffmanTreeNode>();

      // PUT EACH NODE INTO A PRIORITY QUEUE
      for ( String key : keys )
      {
         HuffmanTreeNode treeNode = new HuffmanTreeNode( treeMap.get( key ), key, null, null );
         priorityQueue.add( treeNode );
      }
      
      // Use the priority queue of nodes to build the Huffman tree by
      // repeating the following 3 steps until the queue has only one entry
      while ( ! priorityQueue.isEmpty() )
      {
         // 1. REMOVE THE TWO NODES WITH THE LOWEST FREQUENCY
         HuffmanTreeNode firstNode = priorityQueue.remove();

         // ADD BACK TO QUEUE         
         if ( priorityQueue.isEmpty() )
         {
            // System.out.println( "huffmanize    ADDING firstNode TO QUEUE");
            priorityQueue.add( firstNode );
            break;
         }
         else {
            // System.out.println( "huffmanize    GETTING secondNode");
            HuffmanTreeNode secondNode = priorityQueue.remove();

            // 2. MAKE THEM CHILDREN OF A THIRD NODE, WITH LETTER "*" AND SUMMED VALUE
            int value = ( int ) firstNode.getFrequency() + ( int ) secondNode.getFrequency();
            // System.out.println( "huffmanize    value: " + value );

            HuffmanTreeNode thirdNode = new HuffmanTreeNode( value, "*", firstNode, secondNode );

            // 3. ADD THE THIRD NODE TO THE PRIORITY QUEUE
            priorityQueue.add( thirdNode );
         }
         // System.out.println( "huffmanize    priorityQueue: " + priorityQueue );
      }

      // THE FINAL QUEUE HAS ONLY ONE ENTRY - THE FINAL TREE
      HuffmanTreeNode finalTree = priorityQueue.remove();
      // System.out.println( "huffmanize    finalTree: " + finalTree );

      // RECURSIVELY BUILD THE SCHEME WHERE GOING LEFT IS 0 AND GOING RIGHT IS 1.
      String currentPath = "";
      TreeMap<String, String> letterCodes = new TreeMap<String, String>();
      letterCodes = getLetterCodes( letterCodes, currentPath, finalTree );
      // System.out.println( "huffmanize    letterCodes: " + letterCodes );

      // PROCESS THE STRING LETTER-BY-LETTER
      String binaryCode = "";
      for ( int i = 0; i < message.length(); i++ )
      {  
         binaryCode += letterCodes.get( message.substring( i, i + 1 ) );
      }
      // System.out.println( "huffmanize    binaryCode: " + binaryCode );

      // PRINT OUT BINARY CODE
      System.out.println( binaryCode );

      // WRITE BINARY CODE TO "message." + middlePart + ".txt" FILE
      writeToFile( binaryCode, "message." + middlePart + ".txt" );
 
      // WRITE CODING SCHEME TO "scheme." + middlePart + ".txt" FILE
      String schemeString = letterCodes.toString();
      schemeString = schemeString.replace( "=", "" );
      schemeString = schemeString.replace( "{", "" );
      schemeString = schemeString.replace( "}", "" );
      String[] schemeArray = schemeString.split( ", " );
      schemeString = String.join( "\n", schemeArray );
      // System.out.println( "huffmanize    FINAL schemeString: " + schemeString );
      writeToFile( schemeString, "scheme." + middlePart + ".txt" );
     
   }

   public static void writeToFile ( String contents, String fileName ) throws IOException
   {
      // System.out.println( "writeToFile    contents: " + contents );
      FileWriter writer = new FileWriter( fileName );
      writer.write( contents );
      writer.flush();
      writer.close();
   }

   public static TreeMap<String, String> getLetterCodes ( TreeMap<String, String>letterCodes, String currentPath, HuffmanTreeNode currentNode ) {
      // System.out.println( "getLetterCodes    letterCodes: " + letterCodes );
      // System.out.println( "getLetterCodes    currentNode: " + currentNode.toString() );
      // System.out.println( "getLetterCodes    currentPath: " + currentPath );
      
      if ( currentNode.getLeft() != null )
      {
         letterCodes = getLetterCodes( letterCodes, currentPath + "0", currentNode.getLeft() );
      }

      if ( currentNode.getRight() != null )
      {
         letterCodes = getLetterCodes( letterCodes, currentPath + "1", currentNode.getRight() );
      }

      // System.out.println( "getLetterCodes    currentNode.getLetter(): " + currentNode.getLetter() );
      // System.out.println( "getLetterCodes    currentPath: " + currentPath );
      if ( currentNode.getLeft() == null 
         && currentNode.getRight() == null ) 
      {
         letterCodes.put( (String) currentNode.getLetter(), currentPath );
      }

      return letterCodes;
   }

   @SuppressWarnings("unchecked")
   public static TreeMap makeFrequencyTable ( String string ) {
      TreeMap treeMap = new TreeMap<String, Integer>();
      String[] array = string.split( "" );

      for ( String letter : array )
      {
         if ( treeMap.containsKey( letter ) )
         {
            treeMap.put( letter, ( (int ) treeMap.get( letter ) + 1 ) );
         }
         else
         {
            treeMap.put( letter, 1 );           
         }
      }

      return treeMap;
   }
}
	/*
	  * This tree node stores two values.  Look at TreeNode's API for some help.
	  * The compareTo method must ensure that the lowest frequency has the highest priority.
	  */
class HuffmanTreeNode implements Comparable<HuffmanTreeNode>
{
   private Object frequency; 
   private Object letter; 
   private HuffmanTreeNode leftNode, rightNode;
   
   public HuffmanTreeNode( Object initFrequency, Object initLetter )
   { 
      frequency = initFrequency; 
      letter = initLetter; 
      leftNode = null; 
      rightNode = null; 
   }
   
   public HuffmanTreeNode( Object initFrequency, Object initLetter, HuffmanTreeNode initLeft, HuffmanTreeNode initRight)
   { 
      frequency = initFrequency; 
      letter = initLetter; 
      leftNode = initLeft; 
      rightNode = initRight; 
   }
   
   @SuppressWarnings("unchecked")
   public int compareTo( HuffmanTreeNode treeNode )
   {
      // System.out.println( "compareTo    treeNode " + treeNode.toString() );
      // System.out.println( "compareTo    frequency " + frequency );
      // System.out.println( "compareTo    treeNode.frequency " + treeNode.frequency );


      if ( ( int ) frequency == ( int ) treeNode.frequency ) 
      {
         // System.out.println( "compareTo    RETURNING 0" );
         return 0;
      }      
      if ( ( int ) frequency < ( int ) treeNode.frequency ) 
      {
         // System.out.println( "compareTo    RETURNING -1" );
         return -1;
      }
      else 
      {
         // System.out.println( "compareTo    RETURNING 1" );
         return 1;
      }
   }

   public Object getFrequency()
   { 
      return frequency; 
   }
   
   public Object getLetter()
   { 
      return letter; 
   }
   
   public HuffmanTreeNode getLeft() 
   { 
      return leftNode; 
   }
   
   public HuffmanTreeNode getRight() 
   { 
      return rightNode; 
   }
   
   public void setFrequency(Object theNewFrequency) 
   { 
      frequency = theNewFrequency; 
   }
   
   public void setLeft( HuffmanTreeNode theNewLeft ) 
   { 
      leftNode = theNewLeft;
   }
   
   public void setRight( HuffmanTreeNode theNewRight )
   { 
      rightNode = theNewRight;
   }
}


