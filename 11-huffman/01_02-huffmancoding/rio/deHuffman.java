// Name: Rio Young
// Date: 4/9/19
import java.util.*;
import java.io.*;
import java.lang.*;

public class deHuffman
{
   //String nonIntegers = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ,.;:";
   public static void main(String[] args) throws IOException
   {
      Scanner keyboard = new Scanner(System.in);
      System.out.print("\nWhat binary message (middle part)? ");
      String middlePart = keyboard.next();
      //String middlePart = "maips";
      // String middlePart = "maips";
      // String middlePart = "fault";
      // String middlePart = "tj";
      // String middlePart = "laughter";
      // String middlePart = "money";
      // String middlePart = "vegetables";

      Scanner sc = new Scanner(new File("message."+middlePart+".txt")); 
      String binaryCode = sc.next();
      Scanner huffLines = new Scanner(new File("scheme."+middlePart+".txt")); 
      	
      TreeNode root = huffmanTree(huffLines);
      String message = dehuff(binaryCode, root);
      System.out.println(message);
      	
      sc.close();
      huffLines.close();
   }
 
   public static String dehuff( String binaryCode, TreeNode root )
   {
      System.out.println( "dehuff   binaryCode:" + binaryCode );
      
      return "";
   }
   

   public static TreeNode huffmanTree( Scanner schemeFile )
   {
      String schemeString = "";
      while ( schemeFile.hasNext() )
      {
         schemeString += schemeFile.nextLine();
      }

      schemeString = schemeString.replace( "\n", "" );
      // System.out.println( "huffmanTree    schemeString: " + schemeString );
      
      ArrayList<String> tokens = tokenise( schemeString );
      // System.out.println( "huffmanTree    tokens: " + tokens.toString() );
      
      TreeNode root = huffman( tokens );
      // System.out.println( "huffmanTree    tree: " );
      // System.out.println( display( root, 0 ) );
      
      return root;
   }
   
   public static String display( TreeNode treeNode, int level)
   {      
      String output = "";
      if ( treeNode == null)
      {
         return "";
      }
      output += display( treeNode.getRight(), level + 1 ); //recurse right
      for ( int k = 0; k < level; k++ )
      {
         output += "\t";
      }
      output += treeNode.getValue() + "\n";
      output += display( treeNode.getLeft(), level + 1); //recurse left

      return output;
   }

   public static TreeNode huffman ( ArrayList<String> tokens ) {
      // System.out.println( "huffman    tokens: " + tokens.toString() );
      TreeNode treeNode = new TreeNode( " ", null, null );
      TreeNode root = treeNode;
      root.setValue( "+" );
      TreeNode currentNode = root;
      for ( String token : tokens )
      {
         // System.out.println( "huffman    token: " + token );
         String letter = token.substring( 0, 1 );
         for ( int i = 1; i < token.length(); i++ )
         {
            currentNode = addNode( currentNode, token.substring( i, i + 1 ) );
            currentNode.setValue( "+" );
         }
         currentNode.setValue( letter );
         
         currentNode = root;
      }
      
      return root;
   }

   
   public static TreeNode addNode( TreeNode currentNode, String binaryNumber )
   {
      if ( binaryNumber.equals( "0" ) )
      {
         if ( currentNode.getLeft() == null )
         {
            currentNode.setLeft( new TreeNode( "", null, null ) ); 
         }
         currentNode = currentNode.getLeft();
      }
      else {
         if ( currentNode.getRight() == null )
         {
            currentNode.setRight( new TreeNode( "", null, null ) ); 
         }
         currentNode = currentNode.getRight();         
      }
      
      return currentNode;
   }
   
   public static String dehuff( String binaryCode, TreeNode root )
   {
      // System.out.println( "dehuff   binaryCode:" + binaryCode );
      // System.out.println( "dehuff   root:" + root );
      
      String output = "";
      TreeNode currentNode = root;

      for ( int i = 0; i < binaryCode.length(); i++ )
      {
         String codon = binaryCode.substring( i, i + 1 );
         if ( codon.equals ( "0" ) )
         {
            currentNode = currentNode.getLeft();
         }   
         else
         {
            currentNode = currentNode.getRight();
         }

         String value = ( String ) currentNode.getValue();
         // System.out.println( "dehuff    value: " + value );

         if ( ! value.equals( "+" ) )
         {
            output += value;
            currentNode = root;
         }
      }   

      return output;
   }
   
   public static ArrayList<String> tokenise( String string ) {
      ArrayList<String> tokens = new ArrayList<String>();
      String temp = string.substring( 0, 1 );
      int index = 1;
      
      while ( index < string.length() ) {
         String nextLetter = string.substring( index, index + 1 );
         if ( nextLetter.matches( "\\d+" ) ) {
            temp += nextLetter;
         }
         else {
            tokens.add( temp );
            temp = nextLetter;
         }

         index++;
      }      
      tokens.add( temp );

      return tokens;      
   }
   
}

 /* TreeNode class for the AP Exams */
class TreeNode
{
   private Object value; 
   private TreeNode left, right;
   
   public TreeNode(Object initValue)
   { 
      value = initValue; 
      left = null; 
      right = null; 
   }
   
   public TreeNode(Object initValue, TreeNode initLeft, TreeNode initRight)
   { 
      value = initValue; 
      left = initLeft; 
      right = initRight; 
   }
   
   public Object getValue()
   { 
      return value; 
   }
   
   public TreeNode getLeft() 
   { 
      return left; 
   }
   
   public TreeNode getRight() 
   { 
      return right; 
   }
   
   public void setValue(Object theNewValue) 
   { 
      value = theNewValue; 
   }
   
   public void setLeft(TreeNode theNewLeft) 
   { 
      left = theNewLeft;
   }
   
   public void setRight(TreeNode theNewRight)
   { 
      right = theNewRight;
   }
}
