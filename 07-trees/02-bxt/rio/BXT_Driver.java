// Name: Rio Young
// Date: 2/7/19

import java.util.*;

/*  Driver for a binary expression tree class.
 *  Input: a postfix string, space delimited tokens. 
 */
public class BXT_Driver
{
   public static void main(String[] args)
   {
      ArrayList<String> postExp = new ArrayList<String>();
      postExp.add("14 -5 /");
      postExp.add("20.0 3.0 -4.0 + *");
      postExp.add("2 3 + 5 / 4 5 - *");
      postExp.add("5.6");
   
      for( String postfix : postExp )
      {
         System.out.println("Postfix Exp: "  + postfix);
         BXT tree = new BXT();
         tree.buildTree( postfix );
         System.out.println("BXT: "); 
         System.out.println( tree.display() );
         System.out.print("Infix order:  ");
         System.out.println( tree.inorderTraverse() );
         System.out.print("Prefix order:  ");
         System.out.println( tree.preorderTraverse() );
         System.out.print("Evaluates to " + tree.evaluateTree());
         System.out.println( "\n------------------------");
      }
   }
}

/*  Represents a binary expression tree.
 *  The BXT builds itself from postorder expressions. It can
 *  evaluate and print itself.  Also prints inorder and postorder strings. 
 */
class BXT
{
   private TreeNode root;
   private String expression = "+*-/";
   
   public BXT()
   {
      root = null;
   }
    
   public BXT( Object object)
   {
      System.out.println( "BXT:constructor    object: " + object);
      root = new TreeNode( object, null, null);
   }
    
   public void buildTree( String string )
   {
     	Stack<TreeNode> stack = new Stack<TreeNode>();
      String[] list = string.split( " " );

      for ( int i = 0; i < list.length; i++ )
      {
         String current = list[ i ];
         //System.out.println( "buildTree    current: " + current );
      
         if ( ! isOperator( current) )
         {
            //System.out.println( "buildTree    NOT isOperator. BUILDING TREENODE");
            stack.push( new TreeNode( current ) );
         }
         else 
         {
            //System.out.println( "buildTree    isOperator. BUILDING TREENODE WITH CURRENT: " + current );
            TreeNode rightNode = stack.pop();
            TreeNode leftNode = stack.pop();
            
            TreeNode tree = new TreeNode( current, leftNode, rightNode );
            //System.out.println( "buildTree    tree: " + tree );

            stack.push( tree );
         }
      }
      
      display();
      //System.out.println( "buildTree    stack: " + stack );
      
      root = (TreeNode) stack.pop();
   }
   
   public boolean isOperator( String string )
   {
      if ( expression.indexOf( string ) == -1 )
      {
         return false;
      }
      
      return true;
   }
   
   public double evaluateTree()
   {
      return evaluateNode(root);
   }
   
   private double evaluateNode( TreeNode treeNode )  // recursive
   {
      //double valueDouble = (double) treeNode.getValue();
      String valueString = (String) treeNode.getValue();
      if ( ! isOperator( valueString ) )
      {
            return Double.valueOf( treeNode.getValue().toString() );
      }

      // DO THE EQUATION
      if ( valueString.equals( "/" ) )
      {
         return ( evaluateNode( treeNode.getLeft() ) / evaluateNode( treeNode.getRight() ) );
      }
      else if ( valueString.equals( "*" ) )
      {
         return ( evaluateNode( treeNode.getLeft() ) * evaluateNode( treeNode.getRight() ) );
      }
      else if ( valueString.equals( "+" ) )
      {
         return ( evaluateNode( treeNode.getLeft() ) + evaluateNode( treeNode.getRight() ) );
      }
      else if ( valueString.equals( "-" ) )
      {
         return ( evaluateNode( treeNode.getLeft() ) - evaluateNode( treeNode.getRight() ) );
      }
      else
      {
         System.out.println( "evaluateNode     operator not supported: " + valueString );
         System.exit( 1 );
         return 0.0;
      }
  
   }
   
   
   public String display()
   {
      return display(root, 0);
   }
   
   private String display( TreeNode treeNode, int level )
   {
      String output = "";
      if ( treeNode == null )
      {
         return "";
      }
      
      output += display( treeNode.getRight(), level + 1 ); //recurse right
      for ( int k = 0; k < level; k++ )
      {
         output += "\t";
      }
      output += treeNode.getValue() + "\n";
      output += display( treeNode.getLeft(), level + 1 ); //recurse left

      return output;
   }
    
   public String inorderTraverse()
   {
      return inorderTraverse(root);
   }
   
   private  String inorderTraverse( TreeNode treeNode )
   {
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += inorderTraverse( treeNode.getLeft());   //recurse left
      toReturn += treeNode.getValue() + " ";              //inorder visit
      toReturn += inorderTraverse( treeNode.getRight() );  //recurse right
      
      return toReturn;
   }
   
   public String preorderTraverse()
   {
      return preorderTraverse(root);
   }
   
   private String preorderTraverse( TreeNode treeNode )
   {
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += treeNode.getValue() + " ";              //preorder visit
      toReturn += preorderTraverse( treeNode.getLeft() );   //recurse left
      toReturn += preorderTraverse( treeNode.getRight() );  //recurse right
      return toReturn;
   }

}

/***************************************

 Postfix Exp: 14 -5 /
 	-5
 /
 	14
 Infix order:  14 / -5 
 Prefix order:  / 14 -5 
 Evaluates to -2.8
 ------------------------
 Postfix Exp: 20.0 3.0 -4.0 + *
 		-4.0
 	+
 		3.0
 *
 	20.0
 Infix order:  20.0 * 3.0 + -4.0 
 Prefix order:  * 20.0 + 3.0 -4.0 
 Evaluates to -20.0
 ------------------------
 Postfix Exp: 2 3 + 5 / 4 5 - *
 		5
 	-
 		4
 *
 		5
 	/
 			3
 		+
 			2
 Infix order:  2 + 3 / 5 * 4 - 5 
 Prefix order:  * / + 2 3 5 - 4 5 
 Evaluates to -1.0
 ------------------------
 Postfix Exp: 5.6
 5.6
 Infix order:  5.6 
 Prefix order:  5.6 
 Evaluates to 5.6
 ------------------------
 
 *******************************************/