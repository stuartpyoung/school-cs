// Name: Shaw Young   Date: 5/7/19

import java.util.*;

public class TreeLab
{
   public static TreeNode root = null;
   public static String s = "XCOMPUTERSCIENCE";
   //public static String s = "XThomasJeffersonHighSchool"; 
   //public static String s = "XAComputerScienceTreeHasItsRootAtTheTop";
   //public static String s = "XA";   //comment out lines 44-46 below
   //public static String s = "XAF";  //comment out lines 44-46 below
   //public static String s = "XAFP";  //comment out lines 44-46 below
   //public static String s = "XDFZM";  //comment out lines 44-46 below 
   
   public static void main(String[] args) {
      root = buildTree( root, s );
      System.out.print( display( root, 0) );
   
      System.out.print("\nPreorder: " + preorderTraverse(root));
      System.out.print("\nInorder: " + inorderTraverse(root));
      System.out.print("\nPostorder: " + postorderTraverse(root));
   
      System.out.println("\n\nNodes = " + countNodes(root));
      System.out.println("Leaves = " + countLeaves(root));
      System.out.println("Only children = " + countOnlys(root));
      System.out.println("Grandparents = " + countGrandParents(root));
   
      System.out.println("\nHeight of tree = " + height(root));
      System.out.println("Longest path = " + longestPath(root));
      System.out.println("Min = " + min(root));
      System.out.println("Max = " + max(root));	
   
      System.out.println("\nBy Level: ");
      System.out.println(displayLevelOrder(root));
   }
 
   public static TreeNode buildTree(TreeNode root, String s) {
      root = new TreeNode("" + s.charAt(1), null, null);
      for(int pos = 2; pos < s.length(); pos++)
         insert(root, "" + s.charAt(pos), pos, 
            (int)(1 + Math.log(pos) / Math.log(2)));
      insert(root, "A", 17, 5); 
      insert(root, "B", 18, 5); 
      insert(root, "C", 37, 6); //B's right child
      return root;
   }

   public static void insert(TreeNode t, String s, int pos, int level) {
      TreeNode p = t;
      for(int k = level - 2; k > 0; k--) {
         if((pos & (1 << k)) == 0)
            p = p.getLeft();
         else
            p = p.getRight();
      }
      if((pos & 1) == 0)
         p.setLeft(new TreeNode(s, null, null));
      else
         p.setRight(new TreeNode(s, null, null));
   }
   
   private static String display(TreeNode t, int level) {
      // turn your head towards left shoulder visualize tree
      String toRet = "";
      if(t == null)
         return "";
      toRet += display(t.getRight(), level + 1); //recurse right
      for(int k = 0; k < level; k++)
         toRet += "  ";
      toRet += t.getValue() + "\n";
      toRet += display(t.getLeft(), level + 1); //recurse left
      return toRet;
   }
   
   public static String preorderTraverse( TreeNode treeNode ) { 
      if ( treeNode == null ) {
         return "";
      }

      String toReturn = "";
      toReturn += treeNode.getValue() + " ";              //preorder visit
      toReturn += preorderTraverse( treeNode.getLeft() );   //recurse left
      toReturn += preorderTraverse( treeNode.getRight() );  //recurse right
      return toReturn;
   }
   
   public static String inorderTraverse( TreeNode treeNode ) {
      String toReturn = "";
      if ( treeNode == null ) {
         return "";
      }
      toReturn += inorderTraverse( treeNode.getLeft());   //recurse left
      toReturn += treeNode.getValue() + " ";              //inorder visit
      toReturn += inorderTraverse( treeNode.getRight() );  //recurse right
      
      return toReturn;
   }
   
   public static String postorderTraverse( TreeNode treeNode ) {
      String toReturn = "";
      if ( treeNode == null ) {
         return "";
      }
      toReturn += postorderTraverse( treeNode.getLeft() );   //recurse left
      toReturn += postorderTraverse( treeNode.getRight() );  //recurse right
      toReturn += treeNode.getValue() + " ";              //inorder visit
      
      return toReturn;
   }
   
   public static int countNodes( TreeNode treeNode ) {
      
      if ( treeNode == null ) {
         return 0;
      }
      
      return 1 + countNodes( treeNode.getRight() ) + countNodes( treeNode.getLeft() );
   }
   
   public static int countLeaves(TreeNode treeNode ) {
      int count = 0;

      if ( treeNode == null ) {
         return 0;
      }

      if ( treeNode.getRight() == null 
         && treeNode.getLeft() == null ) {
         return 1;
      }
      else 
      {
         count = count + countLeaves( treeNode.getRight() ) + countLeaves( treeNode.getLeft() );
      }
      
      return count;
   }
   
   /*  there are clever ways and hard ways to count grandparents  */ 
   public static int countGrandParents( TreeNode treeNode ) {
      int count = 0;
      if ( treeNode == null ) {
         return 0;
      }
      else if ( treeNode.getRight() != null
         || treeNode.getLeft() != null ) {
         if ( treeNode.getRight() != null ) {
            if ( treeNode.getRight().getRight() != null
               || treeNode.getRight().getLeft() != null ) {

               count += 1 + countGrandParents( treeNode.getRight() );
            }
         }

         if ( treeNode.getLeft() != null ) {
            if ( treeNode.getLeft().getRight() != null
               || treeNode.getLeft().getLeft() != null ) {
               
               count += 1 + countGrandParents( treeNode.getLeft() );
            }
         }         
      }
      
      return count;
   }
   
   public static int countOnlys( TreeNode treeNode ) {
      if ( treeNode == null ) {
         return 0;
      }
      
      int count = 0;
      if ( treeNode.getRight() != null ) {
         count += countOnlys( treeNode.getRight() );
         if ( treeNode.getLeft() == null ) {
            count += 1;
         }
      }
      
      if ( treeNode.getLeft() != null ) {
         
         if ( treeNode.getRight() == null ) {
            count += 1;
         }
      }
      
      return count;
   }
   
   /* Returns the max of the heights on both sides of the tree */
   public static int height( TreeNode treeNode ) {
      if ( treeNode == null ) {
         return -1;
      }
      
      int leftHeight = 1 + height( treeNode.getLeft() );
      int rightHeight = 1 + height( treeNode.getRight() );
      
      return Math.max( leftHeight, rightHeight );   
   }
   
   /* Returns the max of sum of heights on both sides of tree */   
   public static int longestPath( TreeNode treeNode ) {
      if ( treeNode == null ) {
         return 0;
      }
      
      int rightPath = longestPath( treeNode.getRight() );
      int leftPath = longestPath( treeNode.getLeft() );
      int fullPath = 1 + height( treeNode.getLeft() ) + height( treeNode.getRight() );
      
      if ( fullPath < ( rightPath + leftPath ) ) {
         return ( rightPath + leftPath );
      }
      
      return fullPath;
   }
   
   /*  Object must be cast to Comparable in order to call .compareTo */
   @SuppressWarnings("unchecked")
   public static Object min( TreeNode treeNode ) {
      Object minimumValue = treeNode.getValue();

      if ( treeNode.getRight() != null ) {
         Object rightMinimum = min( treeNode.getRight() );
         if ( ( (Comparable) minimumValue ).compareTo( (Comparable) rightMinimum ) > 0 ) {
            minimumValue = rightMinimum;   
         }
      }
      
      if ( treeNode.getLeft() != null ) {
         Object leftMinimum = min( treeNode.getLeft() );
         if ( ( (Comparable) minimumValue ).compareTo( (Comparable) leftMinimum ) > 0 ) {
            minimumValue = leftMinimum;   
         }
      }
      
      return minimumValue;
   }
   
   /*  Object must be cast to Comparable in order to call .compareTo  */
   @SuppressWarnings("unchecked")
   public static Object max( TreeNode treeNode ) {

      Object maximumValue = treeNode.getValue();
      if ( treeNode.getRight() != null ) {
         Object rightMaximum = max( treeNode.getRight() );
         if ( ( (Comparable) maximumValue ).compareTo( (Comparable) rightMaximum ) < 0 ) {
            maximumValue = rightMaximum;   
         }
      }
      
      if ( treeNode.getLeft() != null ) {
         Object leftMaximum = max( treeNode.getLeft() );
         if ( ( (Comparable) maximumValue ).compareTo( (Comparable) leftMaximum ) < 0 ) {
            maximumValue = leftMaximum;   
         }
      }
      
      return maximumValue;
   }
      
   /* This method is not recursive.  Use a local queue
    * to store the children of the current TreeNode.
    */
   public static String displayLevelOrder( TreeNode treeNode ) {
      Queue<TreeNode> temporaryQueue = new LinkedList<TreeNode>();
      Queue<TreeNode> queue = new LinkedList<TreeNode>();
      String level = (String) treeNode.getValue();

      if ( treeNode.getLeft() != null ) {
         queue.add( treeNode.getLeft() );
      }
      
      if ( treeNode.getRight() != null ) {
         queue.add( treeNode.getRight() );
      }
      
      while ( ! queue.isEmpty() ) {   
         while ( ! queue.isEmpty() ) {
            TreeNode temporaryNode = queue.remove();
            level += (String) temporaryNode.getValue();
            if ( temporaryNode.getLeft() != null ) {
               temporaryQueue.add( temporaryNode.getLeft() );
            }
            
            if ( temporaryNode.getRight() != null ) {
               temporaryQueue.add( temporaryNode.getRight() );
            }
         }
         
         queue = temporaryQueue;
      }
      
      return level;
   }
}

/***************************************************
    ----jGRASP exec: java TreeLab
 		  	E
 		E
 			C
 	M
 			N
 		T
 			E
 C
 			I
 		U
 			C
 	O
 			S
 					C
 				B
 		P
 				A
 			R
 
 Preorder: C O P R A S B C U C I M T E N E C E 
 Inorder: R A P B C S O C U I C E T N M C E E 
 Postorder: A R C B S P C I U O E N T C E E M C 
 
 Nodes = 18
 Leaves = 8
 Only children = 3
 Grandparents = 5
 
 Height of tree = 5
 Longest path = 8
 Min = A
 Max = U
 
 By Level: 
 COMPUTERSCIENCEABC    
 *******************************************************/

