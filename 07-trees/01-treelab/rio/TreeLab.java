// Name: Rio Young
// Date: 2/5/19

import java.util.*;

public class TreeLab
{
   public static TreeNode root = null;
   public static String s = "XCOMPUTERSCIENCE";
   //public static String s = "XThomasJeffersonHighSchool"; 
   //public static String s = "XAComputerScienceTreeHasItsRootAtTheTop";
   //public static String s = "XA";   //comment out lines 44-46 below
   //public static String s = "XAF";  //comment out lines 44-46 below
   //public static String s = "XAFP";  //comment out lines 44-46 below
   //public static String s = "XDFZM";  //comment out lines 44-46 below 
   
   public static void main(String[] args)
   {
      root = buildTree( root, s );
      System.out.print( display( root, 0) );
   
      System.out.print("\nPreorder: " + preorderTraverse(root));
      System.out.print("\nInorder: " + inorderTraverse(root));
      System.out.print("\nPostorder: " + postorderTraverse(root));
   
      System.out.println("\n\nNodes = " + countNodes(root));
      System.out.println("Leaves = " + countLeaves(root));
      System.out.println("Only children = " + countOnlys(root));
      System.out.println("Grandparents = " + countGrandParents(root));
   
      System.out.println("\nHeight of tree = " + height(root));
      System.out.println("Longest path = " + longestPath(root));
      System.out.println("Min = " + min(root));
      System.out.println("Max = " + max(root));	
   
      System.out.println("\nBy Level: ");
      System.out.println(displayLevelOrder(root));
   }
 
   public static TreeNode buildTree(TreeNode root, String s)
   {
      root = new TreeNode("" + s.charAt(1), null, null);
      for(int pos = 2; pos < s.length(); pos++)
         insert(root, "" + s.charAt(pos), pos, 
            (int)(1 + Math.log(pos) / Math.log(2)));
      insert(root, "A", 17, 5); 
      insert(root, "B", 18, 5); 
      insert(root, "C", 37, 6); //B's right child
      return root;
   }

   public static void insert(TreeNode t, String s, int pos, int level)
   {
      TreeNode p = t;
      for(int k = level - 2; k > 0; k--)
      {
         if((pos & (1 << k)) == 0)
            p = p.getLeft();
         else
            p = p.getRight();
      }
      if((pos & 1) == 0)
         p.setLeft(new TreeNode(s, null, null));
      else
         p.setRight(new TreeNode(s, null, null));
   }
   
   private static String display(TreeNode t, int level)
   {
      // turn your head towards left shoulder visualize tree
      String toRet = "";
      if(t == null)
         return "";
      toRet += display(t.getRight(), level + 1); //recurse right
      for(int k = 0; k < level; k++)
         toRet += "\t";
      toRet += t.getValue() + "\n";
      toRet += display(t.getLeft(), level + 1); //recurse left
      return toRet;
   }
   
   public static String preorderTraverse( TreeNode treeNode )
   { 
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += treeNode.getValue() + " ";              //preorder visit
      toReturn += preorderTraverse( treeNode.getLeft() );   //recurse left
      toReturn += preorderTraverse( treeNode.getRight() );  //recurse right
      return toReturn;
   }
   
   public static String inorderTraverse( TreeNode treeNode )
   {
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += inorderTraverse( treeNode.getLeft());   //recurse left
      toReturn += treeNode.getValue() + " ";              //inorder visit
      toReturn += inorderTraverse( treeNode.getRight() );  //recurse right
      
      return toReturn;
   }
   
   public static String postorderTraverse( TreeNode treeNode )
   {
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += postorderTraverse( treeNode.getLeft() );   //recurse left
      toReturn += postorderTraverse( treeNode.getRight() );  //recurse right
      toReturn += treeNode.getValue() + " ";              //inorder visit
      
      return toReturn;
   }
   
   public static int countNodes( TreeNode treeNode )
   {
      
      if ( treeNode == null )
      {
         return 0;
      }
      int count = 1;
      count += countNodes( treeNode.getLeft() );
      count += countNodes( treeNode.getRight() );
      //System.out.println( "countNodes    treeNode: " + treeNode.getValue() + ", count: " + count );
      
      return count;
   }
   
   public static int countLeaves(TreeNode treeNode )
   {
      //System.out.println( "countLeaves    treeNode: " + treeNode.getValue() );
      int count = 0;
      if ( treeNode == null )
      {
         return 0;
      }
      if ( treeNode.getLeft() == null  && treeNode.getRight() == null )
      {
         return 1;
      }
      else 
      {
         count += countLeaves( treeNode.getLeft() );
         count += countLeaves( treeNode.getRight() );
         //System.out.println( "countLeaves    treeNode: " + treeNode.getValue() + ", count: " + count );
      }
      
      return count;
   }
   
   /*  there are clever ways and hard ways to count grandparents  */ 
   public static int countGrandParents( TreeNode treeNode )
   {
      int count = 0;
      if ( treeNode == null )
      {
         return 0;
      }
      else if ( treeNode.getLeft() != null || treeNode.getRight() != null )
      {
         boolean counted = false;
         if ( treeNode.getLeft() != null )
         {
            if ( treeNode.getLeft().getLeft() != null || treeNode.getLeft().getRight() != null )
            {
               count += 1;
               counted = true;
               count += countGrandParents( treeNode.getLeft() );
            }
         }
         
         if ( treeNode.getRight() != null )
         {
            if ( treeNode.getRight().getLeft() != null || treeNode.getRight().getRight() != null )
            {
               if ( ! counted )
               {                  
                  count += 1;
               }
               count += countGrandParents( treeNode.getRight() );
            }
         }
      }
      
      return count;
   }
   
   public static int countOnlys( TreeNode treeNode )
   {
      //System.out.println( "countOnlys    treeNode: " + treeNode.getValue() );
      int count = 0;
      if ( treeNode == null )
      {
         //System.out.println( "countOnlys    treeNode null");
         return 0;
      }
      
      if ( treeNode.getLeft() != null )
      {
         count += countOnlys( treeNode.getLeft() );
         //System.out.println( "countOnlys    LEFT" );
         
         if ( treeNode.getRight() == null )
         {
            count += 1;
            //System.out.println( "countOnlys    LEFT != null    treeNode: " + treeNode.getValue() + ", count: " + count );
         }
         
      }
      
      if ( treeNode.getRight() != null )
      {
         count += countOnlys( treeNode.getRight() );
         //System.out.println( "countOnlys    RIGHT" );
         if ( treeNode.getLeft() == null )
         {
            count += 1;
            //System.out.println( "countOnlys    RIGHT != null    treeNode: " + treeNode.getValue() + ", count: " + count );
         }
         
         
      }
      
      return count;
   }
   
   /* Returns the max of the heights on both sides of the tree
	 */
   public static int height( TreeNode treeNode )
   {
      //System.out.println( "height" );
      if ( treeNode == null )
      {
         return -1;
      }
      
      int leftHeight = 1 + height( treeNode.getLeft() );
      int rightHeight = 1 + height( treeNode.getRight() );
      
      return Math.max( leftHeight, rightHeight );   
   }
   
   /* Returns the max of sum of heights on both sides of tree
	 */   
   public static int longestPath( TreeNode treeNode )
   {
      //System.out.println( "longestPath    ");
      if ( treeNode == null )
      {
         return 0;
      }
      //System.out.println( "longestPath    treeNode: " + treeNode.getValue() );
      
      int fullPath = 1 + height( treeNode.getLeft() ) + height( treeNode.getRight() );
      //System.out.println( "longestPath    fullPath: " + fullPath );

      int leftPath = longestPath( treeNode.getLeft() );
      int rightPath = longestPath( treeNode.getRight() );
      
      if ( ( leftPath + rightPath ) > fullPath )
      {
         return ( leftPath + rightPath );
      }
      
      return fullPath;
   }
   
   /*  Object must be cast to Comparable in order to call .compareTo  
    */
   @SuppressWarnings("unchecked")
   public static Object min( TreeNode treeNode )
   {
      Object minimum = treeNode.getValue();
      if ( treeNode.getLeft() != null )
      {
         Object leftMinimum = min( treeNode.getLeft() );
         if ( ( (Comparable) minimum ).compareTo( (Comparable) leftMinimum ) > 0 )
         {
            minimum = leftMinimum;   
         }
      }
      
      if ( treeNode.getRight() != null )
      {
         Object rightMinimum = min( treeNode.getRight() );
         if ( ( (Comparable) minimum ).compareTo( (Comparable) rightMinimum ) > 0 )
         {
            minimum = rightMinimum;   
         }
      }
      
      return minimum;
   }
   
   /*  Object must be cast to Comparable in order to call .compareTo  
    */
   @SuppressWarnings("unchecked")
   public static Object max( TreeNode treeNode )
   {
      Object maximum = treeNode.getValue();
      if ( treeNode.getLeft() != null )
      {
         Object leftMaximum = max( treeNode.getLeft() );
         if ( ( (Comparable) maximum ).compareTo( (Comparable) leftMaximum ) < 0 )
         {
            maximum = leftMaximum;   
         }
      }
      
      if ( treeNode.getRight() != null )
      {
         Object rightMaximum = max( treeNode.getRight() );
         if ( ( (Comparable) maximum ).compareTo( (Comparable) rightMaximum ) < 0 )
         {
            maximum = rightMaximum;   
         }
      }
      
      return maximum;
   }
      
   /* This method is not recursive.  Use a local queue
    * to store the children of the current TreeNode.
    */
   public static String displayLevelOrder( TreeNode treeNode )
   {
      String level = (String) treeNode.getValue();
      Queue<TreeNode> queue = new LinkedList<TreeNode>();
      Queue<TreeNode> tempQueue = new LinkedList<TreeNode>();
      if ( treeNode.getLeft() != null )
      {
         queue.add( treeNode.getLeft() );
      }
      
      if ( treeNode.getRight() != null )
      {
         queue.add( treeNode.getRight() );
      }
      
      while ( queue.size() != 0 )
      {   
         //System.out.println( "displayLevelOrder    queue.size(): " + queue.size() );
         while ( queue.size() != 0 )
         {
            TreeNode tempNode = queue.remove();
            level += (String) tempNode.getValue();
            if ( tempNode.getLeft() != null )
            {
               tempQueue.add( tempNode.getLeft() );
            }
            
            if ( tempNode.getRight() != null )
            {
               tempQueue.add( tempNode.getRight() );
            }
         }
         
         queue = tempQueue;
      }
      
      return level;
   }
}

/***************************************************
    ----jGRASP exec: java TreeLab
 		  	E
 		E
 			C
 	M
 			N
 		T
 			E
 C
 			I
 		U
 			C
 	O
 			S
 					C
 				B
 		P
 				A
 			R
 
 Preorder: C O P R A S B C U C I M T E N E C E 
 Inorder: R A P B C S O C U I C E T N M C E E 
 Postorder: A R C B S P C I U O E N T C E E M C 
 
 Nodes = 18
 Leaves = 8
 Only children = 3
 Grandparents = 5
 
 Height of tree = 5
 Longest path = 8
 Min = A
 Max = U
 
 By Level: 
 COMPUTERSCIENCEABC    
 *******************************************************/

