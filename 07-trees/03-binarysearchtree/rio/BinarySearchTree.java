// Name: Rio Young
// Date: 2/12/19

import java.util.*;

/*  Make an ArrayList of input strings.  Build the Binary 
 *  Search Trees (using TreeNodes) from the letters in the 
 *  string.   Display it as a sideways tree (take the code 
 *  from TreeLab).  Prompt the user for a target and search 
 *  the BST for it.  Display the tree's minimum and maximum 
 *  values. Print the letters in order from smallest to largest.
 */

public class BinarySearchTree
{
   public static void main(String[] args)
   {
      Scanner keyboard = new Scanner(System.in);
      ArrayList<String> str = new ArrayList<String>();
      str.add("MAENIRAC");
      str.add("AMERICAN");
      str.add("AACEIMNR");
      str.add("A");
      str.add("6829301");
   
      for( String s : str )
      {
         System.out.println("String: "  + s);
         TreeNode root = null;
         root = buildTree( root, s );
         System.out.println( display(root, 0) );
         System.out.print("Input target: ");
         //String target = "I";
         //System.out.println("String target: " + target );
         String target =  keyboard.next();    //"I"
         boolean itemFound = find(root, target);
         if(itemFound)
            System.out.println("found: " + target);
         else
            System.out.println(target +" not found.");
         
         System.out.println("Min = " + min(root));
         System.out.println("Max = " + max(root));	
      
         System.out.print("In Order: ");
         System.out.println( smallToLarge(root) );
         System.out.println("\n---------------------");
      }
   }
   
   /* @param str string of characters
    */
   public static TreeNode buildTree( TreeNode treeNode, String string )
   {
      treeNode = new TreeNode( string.substring( 0, 1 ) );
      for ( int i = 1; i < string.length(); i++ )
      {
         insert( treeNode, string.substring( i, i + 1 ) );
      }
      
      return treeNode;
   }
   
   /* Recursive algorithm to build a BST:  if the node is 
    * null, insert the new node.  Else, if the item is less,
    * set the left node and recur to the left.  Else, if the 
    * item is greater, set the right node and recur to the 
    * right.   
    * @param s one letter to be inserted
    */
   public static TreeNode insert( TreeNode treeNode, String string)
   {
      Comparable item = ( Comparable ) string;
      
      if ( treeNode == null )
      {
         return new TreeNode( string );
      }
      
      if ( item.compareTo ( ( Comparable ) treeNode.getValue() ) <= 0 )
      {
         treeNode.setLeft( insert( treeNode.getLeft(), string ) );
      }
      else
      {
         treeNode.setRight( insert( treeNode.getRight(), string ) );
      }
      
      return treeNode;
   }
   
   
   
/***************************************
 String: MAENIRAC
 		R
 	N
 M
 			I
 		E
 			C
 	A
 		A
 Input target: I
 found: I
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------
 String: AMERICAN
 		R
 			N
 	M
 			I
 		E
 			C
 A
 	A
 Input target: I
 found: I
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------
 String: AACEIMNR
 						R
 					N
 				M
 			I
 		E
 	C
 A
 	A
 Input target: i
 i not found.
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------   
 ************************************/

   /* Copy the code that is in TreeLab  
    */
   public static String display( TreeNode treeNode, int level)
   {
      String toReturn = "";
      if ( treeNode == null)
      {
         return "";
      }
      toReturn += display( treeNode.getRight(), level + 1 ); //recurse right
      for ( int k = 0; k < level; k++ )
      {
         toReturn += "\t";
      }
      toReturn += treeNode.getValue() + "\n";
      toReturn += display( treeNode.getLeft(), level + 1); //recurse left
      return toReturn;
   }
   
   /* Iterative algorithm:  create a temporary pointer p at 
    * the root. While p is not null, if the p's value equals 
    * the target, return true. If the target is less than the 
    * p's value, go left, otherwise go right. If the target 
    * is not found, return false. Find the target. Recursive 
    * algorithm:  If the tree is empty, return false.  If the 
    * target is less than the current node value, return the 
    * left subtree.  If the target is greater, return the right 
    * subtree.  Otherwise, return true.   
    */    
   public static boolean find( TreeNode treeNode, Comparable target )
   {
      //if ( treeNode == null)
      //{
      //   return false;
      //}
      //
      //if ( target.compareTo( ( Comparable ) treeNode.getValue() ) < 0 )
      //{
      //  return find( treeNode.getLeft(), target );
      //}
      //
      //if ( target.compareTo( ( Comparable ) treeNode.getValue() ) > 0 )
      //{
      //   return find( treeNode.getRight(), target );
      //}
      //
      //return true;
   
      if ( treeNode == null)
      {
         return false;
      }
      
      if ( target.equals( ( Comparable ) treeNode.getValue() ) )
      {
        return true;
      }
      
      if ( target.compareTo( ( Comparable ) treeNode.getValue() ) < 0 )
      {
         return find( treeNode.getLeft(), target );
      }
      else
      {
         return find( treeNode.getRight(), target );
      }
      
      
   }
      
   /*	Starting at the root, return the min value in the BST.   
    *	Use iteration.   Hint:  look at several BSTs. Where are 
    *	the min values always located?  
    */
   public static Object min( TreeNode treeNode )
   {
      if ( treeNode == null )
      {
         return null;
      }
      
      if ( treeNode.getLeft() == null )
      {
         return treeNode.getValue();
      }
     
      return min( treeNode.getLeft() ); 
   }
      
   /* Starting at the root, return the max value in the BST.  
    * Use recursion!
    */
   public static Object max( TreeNode treeNode )
   {
      if ( treeNode == null )
      {
         return null;
      }
      
      if ( treeNode.getRight() == null )
      {
         return treeNode.getValue();
      }
     
      return max( treeNode.getRight() ); 
   }
   
   public static String smallToLarge( TreeNode treeNode )
   {
      String toReturn = "";
      if ( treeNode == null )
      {
         return "";
      }
      toReturn += smallToLarge( treeNode.getLeft());   //recurse left
      toReturn += treeNode.getValue() + " ";              //inorder visit
      toReturn += smallToLarge( treeNode.getRight() );  //recurse right
      
      return toReturn;
   }
}


/***************************************
 String: MAENIRAC
 		R
 	N
 M
 			I
 		E
 			C
 	A
 		A
 Input target: I
 found: I
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------
 String: AMERICAN
 		R
 			N
 	M
 			I
 		E
 			C
 A
 	A
 Input target: I
 found: I
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------
 String: AACEIMNR
 						R
 					N
 				M
 			I
 		E
 	C
 A
 	A
 Input target: i
 i not found.
 Min = A
 Max = R
 In Order: A A C E I M N R 
 ---------------------   
 ************************************/