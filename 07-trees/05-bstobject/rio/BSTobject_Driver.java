// Name: Rio Young
// Date: 2/26/19

import java.util.*;
import java.io.File; 

public class BSTobject_Driver
{
   public static BSTobject<String> tree = null;
   public static BSTobject<Widget> treeOfWidgets = null;
   public static int numberOfWidgets = 10;
   public static void main( String[] args ) 
   {
      // DAY ONE 
      //tree = new BSTobject<String>();
      //tree = build(tree, "T");
      //System.out.print(tree);
      //System.out.println("Size: " + tree.size());
      //System.out.println("Is empty: "+ tree.isEmpty());
   		
         
      //DAY TWO DRIVER: STRINGS
   	// Your assignment the second day is to finish the BSTobject class.  
   	// Specifically, prompt the user for a string, put the characters into 
      // a BST, call toString on this tree, and print the size of the tree.
      
      //Scanner sc = new Scanner(System.in);
      //System.out.print("Input string: ");
      ////String s = "AMERICAN";
      //String input = sc.nextLine();
      //tree = new BSTobject<String>();
      //tree = build( tree, input );
      //System.out.print( tree );
      //System.out.println("Size: " + tree.size());
      //System.out.println("Is empty: "+ tree.isEmpty());
      
   	
   	
      // DAY TWO: WIDGETS		
   	// Next, fill your BST with 10 Widget objects from widgets.txt.  Display 
      // the tree. Then prompt the user to enter cubits and hands.  If the tree 
      // contains that Widget, delete it, of course restoring the BST order. 
      // Display the new tree and its size. If the tree does not contain that 
      // Widget, print "Not found".*/
      File file = new File( "widgets.txt" );
      treeOfWidgets = new BSTobject<Widget>();
      treeOfWidgets = build( treeOfWidgets, file, numberOfWidgets );
      System.out.print( treeOfWidgets );
      Scanner sc = new Scanner(System.in);
      System.out.print("Input string: ");
      int cubits = sc.nextInt();
      int hands = sc.nextInt();
      // int cubits = 13;
      // int hands = 15;

      treeOfWidgets.delete( new Widget( cubits, hands ) );

      System.out.print( treeOfWidgets );
      
   
   	// Day three -- AVL tree  -----------------------
   }
  
   /* Build the tree for Strings, Day 1
    */
   public static BSTobject<String> build( BSTobject<String> tree, String inputString )
   {
      return tree.build( inputString );                  
   }
   
   /* Build the tree for Widgets, Day 2
    */
   public static BSTobject<Widget> build( BSTobject<Widget> tree, File file, int numberOfWidgets )
   {
      return tree.build( file, numberOfWidgets );
   }
}

interface BSTinterface<E extends Comparable<E>>
{
   public E add( E element );             //returns the object
   public boolean contains( E element );
   public boolean isEmpty();
   public E delete( E element );          //returns the object, not a Node<E>
   public int size();
   public String toString();
}

class BSTobject <E extends Comparable<E>> implements BSTinterface<E>
{ 
   // Declare 2 fields 
   public Node root = null;
   public int treeSize = 0;
   // ?

   // Create a default constructor
   public BSTobject () { }
   
   // BUILD
   public BSTobject<E> build( String inputString )
   {
      //System.out.println( "BSTobject.build    inputString: " + inputString );
      //System.out.println( "BSTobject.build    inputString.length(): " + inputString.length() );
      if ( inputString.length() == 0 )
      {
         return this;
      }
      
      root = new Node( "" + inputString.charAt( 0 ), null, null );
      treeSize++;
      if ( inputString.length() == 1 )
      {
         return this;
      }
      
      for ( int position = 1; position < inputString.length(); position++ )
      {  
         insert( root, "" + inputString.charAt( position ) );/*, position,  
            (int)(1 + Math.log(position) / Math.log(2)) );*/
         treeSize++;
      }
      
      return this;
   }
   
   public BSTobject<E> build( File file, int numberOfWidgets )
   {
      Scanner infile = null;
      try{
         infile = new Scanner( file );
      }
      catch ( Exception e )
      {
        System.out.println("File not found.");
      }        
      
      int rootX = infile.nextInt();
      int rootY = infile.nextInt();
      root = new Node( new Widget( rootX, rootY ), null, null );
      treeSize++;
      //System.out.println( "BSTobject.build    root: " + root );
      
      for( int i = 0; i < numberOfWidgets; i++ )   
      {
         int x = infile.nextInt();
         //System.out.println( "X: " + x );
         int y = infile.nextInt();
         //System.out.println( "Y: " + y );
         insert( root, new Widget( x , y ) );
         treeSize++;
      }
      //System.out.println( "BSTobject.build    this: " + this );
      return this;
   }
   
   /* Recursive algorithm to build a BST:  if the node is 
    * null, insert the new node.  Else, if the item is less, 
    * set the left node and recur to the left.  Else, if the 
    * item is greater, set the right node and recur to the right.   
	 */
   public Node insert( Node treeNode, String inputString )
   {  	
      if ( treeNode == null )
      {
         return new Node( inputString );
      }
      if ( inputString.compareTo( treeNode.getValue() + "" ) <= 0 )
      {
         treeNode.setLeft(insert( treeNode.getLeft(), inputString ));
      }
      else
      {
         treeNode.setRight(insert( treeNode.getRight(), inputString ));
      }
   
      return treeNode;
   }
   
   public Node insert( Node treeNode, Widget widget )
   {
      if ( treeNode == null )
      {
         return new Node( widget );
      }
      
      // System.out.println( " Node.insert    treeNode.getValue()getClass().getName(): " + treeNode.getValue().getClass().getName() );
      if ( widget.compareTo( ( Widget ) treeNode.getValue() ) <= 0 )
      {
         treeNode.setLeft(insert( treeNode.getLeft(), widget ));
      }
      else
      {
         treeNode.setRight(insert( treeNode.getRight(), widget ));
      }
   
      return treeNode;
   }

   //instance methods
   public E add( E obj )
   {
      root = add( root, obj );
      treeSize++;
      
      return obj;
   }
   
   //recursive helper method
   private Node<E> add( Node<E> t, E obj )
   {
      return null;
   }
   
   /* Implement the interface here.  Use TreeNode as an example,
    * but root is a field. You need add, contains, isEmpty, 
    * delete, size, and toString.  
    */
   public boolean contains( E element )
   {
      return true;      
   }
   
   public boolean isEmpty()
   {
      if ( treeSize == 0 )
      {
         return true;
      }
      else
      {
         return false;     
      }
   }
   
   // returns the object, not a Node<E>
   public E delete( E target )
   {
      Node parent = root;  //don't lose the root!
      Node current = root;
      
      if ( root.getLeft() == null && root.getRight() == null && root.getValue().equals( target ) ) // Case 1b
      {
         root = null;
         return target;
      }
      
      if ( root.getRight() == null && root.getValue().equals( target ) ) // Case 2c
      {
         Node temp = root.getRight();
         root = root.getLeft();
         return target;
      }
      
      if ( root.getLeft() == null && root.getValue().equals( target ) ) // Case 2d
      {
         root = root.getRight();
         return target;
      }
      
      while( current != null )
      {
         int compare = target.compareTo( (E) current.getValue() );
        // ------->  your code goes here
         
         if ( compare < 0 )
         {
            parent = current;
            current = current.getLeft();
         }
         
         if ( compare > 0 )
         {
            parent = current;
            current = current.getRight();
         }
         
         if ( compare == 0 )
         {
            if ( current.getLeft() == null && current.getRight() == null ) // Case 1a
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( null ); 
               }
               else
               {
                  parent.setRight( null ); 
               }
               
               return target;
            }
            
            if ( current.getLeft() == null ) // Case 2a
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( current.getRight() ); 
               }
               else
               {
                  parent.setRight( current.getRight() ); 
               }
               
               return target;
            }
            
            if ( current.getRight() == null ) // Case 2b
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( current.getLeft() ); 
               }
               else
               {
                  parent.setRight( current.getLeft() ); 
               }
               
               return target;
            }
            
            Node newNodeParent = current.getLeft();
            Node newNode = current.getLeft();
            while ( newNode.getRight() != null ) // Case 3
            {
               newNodeParent = newNode;
               newNode = newNode.getRight();
            }
            
            current.setValue( newNode.getValue() );
            
            if ( newNode.getLeft() != null ) // Case 3b
            {
               if ( current.getLeft() == newNode )
               {
                  current.setLeft( newNode.getLeft() );
               }
               else
               {
                  newNodeParent.setRight( newNode.getLeft() );
               }
            }
            else // Case 3a
            {
               newNodeParent.setRight( null );
               //current.setLeft( newNode.getLeft() );
            }
            
            //newNode.setLeft( current.getLeft() );
            //newNode.setRight( current.getRight() );
            
            return target;
         }
      
      
      
      }
      return target;  //never reached         
         
   }
   
   public int size()
   {
      return treeSize;      
   }
   
   
   public String toString()
   {
      // StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
      // System.out.println( "BSTobject.toString    CALLER: " + stackTraceElements[ 2 ] );

      return display( root, 0 );
   }
   
   private String display( Node treeNode, int level )
   {
      // StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
      // System.out.println( "BSTobject.display    CURRENT LINE: " + stackTraceElements[ 1 ] );
      // System.out.println( "BSTobject.display    CALLER: " + stackTraceElements[ 2 ] );

      // System.out.println( "BSTobject.display    treeNode: " + treeNode );
      // System.out.println( "BSTobject.display    level: " + level );
      
      // turn your head towards left shoulder visualize tree
      String output = "";
      if ( treeNode == null)
      {
         // System.out.println( "BSTobject.display    treeNode == null. Returning '' " );
         return "";
      }
       
      // System.out.println( "BSTobject.display    ****** DOING display( treeNode.getRight() ) " );
      output += display( treeNode.getRight(), level + 1 ); //recurse right
      for (int k = 0; k < level; k++)
      {
         output += "\t";
      }
      output += treeNode.toString() + "\n";
      // System.out.println( "BSTobject.display    ****** DOING display( treeNode.getLeft() ) " );
      output += display( treeNode.getLeft(), level + 1 ); //recurse left

      return output;
   }

   /* Private inner class 
    */  
   private class Node<E>
   {
      // 3 fields
      private Object value; 
      private Node left, right;
      private Widget widget = null;
   
      // 2 constructors, one-arg and three-arg
      public Node(Object initValue)
      { 
         value = initValue; 
         left = null; 
         right = null; 
      }

      public Node(Object initValue, Node initLeft, Node initRight)
      { 
         value = initValue; 
         left = initLeft; 
         right = initRight; 
      }
      
      public Node( Widget inputWidget, Node initLeft, Node initRight)
      { 
         value = inputWidget.getCubits() + " cubits " + inputWidget.getHands() + " hands ";
         widget = inputWidget;
         left = initLeft; 
         right = initRight; 
      }
      
      //methods--Use Node as an example. See Quick Reference Guide.
      public Object getValue()
      { 
         // System.out.println( "Node.getValue    widget: " + widget );
         // StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
         // System.out.println( "Node.getValue    CALLER: " + stackTraceElements[ 2 ] );

         if ( widget != null )
         {
            return widget;
         }
         
         return value; 
      }
      
      public Node getLeft() 
      { 
         return left; 
      }
      
      public Node getRight() 
      { 
         return right; 
      }
      
      public void setValue(Object theNewValue) 
      { 
         value = theNewValue; 
      }
      
      public void setLeft(Node theNewLeft) 
      { 
         left = theNewLeft;
      }
      
      public void setRight(Node theNewRight)
      { 
         right = theNewRight;
      }
      
      public String toString()
      {
         // StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
         // System.out.println( "Node.toString    CALLER: " + stackTraceElements[ 1 ] );
         // System.out.println( "Node.toString    CALLER: " + stackTraceElements[ 2 ] );

         return getValue().toString();
      }
   }
}
