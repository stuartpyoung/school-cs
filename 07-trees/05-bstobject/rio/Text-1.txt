   public E delete( E element )
   {
      Node parent = root;
      Node current = root;
      
      if ( root.getLeft() == null && root.getRight() == null && root.getValue().equals( element ) ) //Case 1b
      {
         root = null;
         return element;
      }
      
      if ( root.getRight() == null && root.getValue().equals( element )
         ||  root.getValue().equals( element ) ) //Case 2c
      {
         Node temp = root.getRight();
         root = root.getLeft();
         root.setRight( temp );

         return element;
      }
      
      if ( root.getLeft() == null && root.getValue().equals( element ) ) //Case 2d
      {
         root = root.getRight();
         return element;
      }
      
      while( current != null )
      {
         // System.out.println( "BSTobject.delete    current.getValue(): " + current.getValue() );
         // System.out.println( "BSTobject.delete    current.getValue().getClass().getName(): " + current.getValue().getClass().getName() );

         int compare = element.compareTo( (E) current.getValue() );
         // System.out.println( "BSTobject.delete    compare: " + compare );
         

         if ( compare < 0 )
         {
            parent = current;
            current = current.getLeft();
         }
         
         if ( compare > 0 )
         {
            parent = current;
            current = current.getRight();
         }
         
         if ( compare == 0 )
         {
            if ( current.getLeft() == null && current.getRight() == null ) //Case 1a
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( null ); 
               }
               else
               {
                  parent.setRight( null ); 
               }
               
               return element;
            }
            
            if ( current.getLeft() == null )
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( current.getRight() ); 
               }
               else
               {
                  parent.setRight( current.getRight() ); 
               }
               
               return element;
            }
            
            if ( current.getRight() == null )
            {
               if ( parent.getLeft() == current )
               {
                  parent.setLeft( current.getLeft() ); 
               }
               else
               {
                  parent.setRight( current.getLeft() ); 
               }
               
               return element;
            }
            
            Node newNodeParent = current.getLeft();
            Node newNode = current.getLeft();
            // System.out.println( "BSTobject.delete     newNode: " + newNode );

            while ( newNode.getRight() != null ) //Case 3
            {
               newNodeParent = newNode;
               newNode = newNode.getRight();
            }
            
            // System.out.println( "BSTobject.delete     parent: " + parent );
            // System.out.println( "BSTobject.delete     HERE 1" );
            if ( parent.getLeft() == current )
            {
               parent.setLeft( newNode ); 
            }
            else
            {
            }
            // System.out.println( "BSTobject.delete     HERE 2" );
            
               parent.setRight( newNode ); 
            if ( newNode.getLeft() != null ) //Case 3b
            {
               newNodeParent.setRight( newNode.getLeft() );
            }
            else //Case 3a
            {
               newNodeParent.setRight( null );
            }
            
            // System.out.println( "BSTobject.delete     HERE 3" );

            newNode.setLeft( current.getLeft() );
            newNode.setRight( current.getRight() );
            
            return element;
         }      
      }
      return element;  //never reached