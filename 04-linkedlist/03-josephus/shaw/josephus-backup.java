// Name: Rio Young
// Date: 11/20/18

import java.util.*;
import java.io.*;

public class Josephus
{
   private static String WINNER = "Josephus";
   //private ListNode pointer = null;
   
   public static void main(String[] args) throws FileNotFoundException
   {
      ///* run numberCircle first with J_numbers.txt  */
      Scanner scanner = new Scanner(System.in);
      //System.out.print("How many names (2-20)? ");
      //int number = Integer.parseInt( scanner.next() );
      int number = 5;
      //System.out.print("How many names to count off each time? "  );
      //int countOff = Integer.parseInt( scanner.next() );
      int countOff = 4;
      //ListNode winningPos = numberCircle( number, countOff, "J_numbers.txt");
      //System.out.println(winningPos.getValue() + " wins!");  
     
      /* run josephusCircle next with J_names.txt  */
      //System.out.println("\n ****  Now start all over. **** \n");
      //System.out.print("Where should "+WINNER+" stand?  ");
      //int winPos = Integer.parseInt( scanner.next() );        
      int winPos = 2;
      ListNode theWinner = josephusCircle( number, countOff, "J_names.txt", winPos);
      //ListNode theWinner = josephusCircle( number, countOff, "J_names-short.txt", winPos );
      System.out.println(theWinner.getValue() + " wins!");  
   }
   
   public static ListNode numberCircle(int n, int countOff, String filename) throws FileNotFoundException
   {
      ListNode p = null;
      p = readNLinesOfFile(n, new File(filename));
      //print( p );
      p = countingOff(p, countOff, n);
      return p;
   }
   
   public static ListNode josephusCircle(int n, int countOff, String filename, int winPos) throws FileNotFoundException
   {
      ListNode p = null;
      p = readNLinesOfFile(n, new File(filename));
      replaceAt(p, WINNER, winPos);
      p = countingOff(p, countOff, n);
      return p;
   }

   /* reads the names, calls insert(), builds the linked list.
	 */
   public static ListNode readNLinesOfFile( int number, File filename ) throws FileNotFoundException
   {
      //System.out.println( "readNLinesOfFile    number: " + number + ", filename: " + filename );
      ListNode pointer = null;
      Scanner scanner = new Scanner( filename );
      for ( int k = 0; k < number; k++ )
      {
         Object item = scanner.next();
         System.out.println( "readNLinesOfFile    number: " + number + ", item: " + item );
         pointer = insert( pointer, item );	
      }
      print ( pointer );
      //System.exit( 1 );
      return pointer;
   }
   
   /* helper method to build the list.  Creates the node, then
    * inserts it in the circular linked list.
	 */
   public static ListNode insert(ListNode pointer, Object nodeName )
   {
      //System.out.println( "insert    pointer: " + pointer + ", nodeName: " + nodeName );
      if ( pointer == null )
      {
         pointer = new ListNode( nodeName, null );
         pointer.setNext( pointer );
      }
      else
      {
         ListNode newNode = new ListNode( nodeName, pointer.getNext() );
         pointer.setNext( newNode );
         //print( pointer );
         //System.out.println( "insert    debug exit" );
         //System.exit( 1 );
      }
      
      return pointer;
   }
   
   /* Runs a Josephus game, counting off and removing each name. Prints after each removal.
      Ends with one remaining name, who is the winner. 
	 */
   public static ListNode countingOff(ListNode pointer, int count, int number )
   {
      //System.out.println( "countingOff    pointer: " + pointer + ", count: " + count + ", number: " + number );
      print( pointer );
      for ( int i = 0; i < number - 1; i++ )
      {
         pointer = remove( pointer, count );
         print( pointer );
      }
      
      return pointer;
   }
   
   /* removes the node after counting off count-1 nodes.
	 */
   public static ListNode remove( ListNode pointer, int count)
   {
      ListNode lastNode = null;
      for ( int i = 0; i < count; i++ )
      {
         lastNode = pointer;
         pointer = pointer.getNext();
      }
      ListNode nextNode = pointer.getNext();
      lastNode.setNext( nextNode );
      
      return nextNode;
   }
   
   /* prints the circular linked list.
	 */
   public static void print( ListNode pointer )
   {
      Object pointerName = pointer.getValue();
      String output = "";
      ListNode nextNode = pointer.getNext();
      //System.out.println( "print    pointerName: " + pointerName );
      while ( nextNode.getValue() != pointerName )
      {
         output = nextNode.getValue() + " " + output;
         //System.out.print( pointer.getValue() + " " );
         nextNode = nextNode.getNext();
      }
      //System.out.println( pointer.getValue() + " " );
      output = pointerName + " " + output;
      System.out.println( output );
   }
	
   /* replaces the value (the string) at the winning node.
	 */
   public static void replaceAt( ListNode pointer, Object nodeName, int position )
   {
      System.out.println( "replaceAt    pointer.getValue(): " + pointer.getValue() + ", position: " + position );
      ListNode tempNode = pointer;
      System.out.println( "pointer.getNext().getValue(): " + pointer.getNext().getValue() );
      for ( int i = 1; i < position; i++ )
      {
         tempNode = tempNode.getNext();
      }
      
      tempNode.setValue( nodeName );
      print( pointer );
      System.exit( 1 );
   }
}

