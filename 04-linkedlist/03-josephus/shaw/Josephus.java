// Name: Shaw Young  Date: 11/19/18

import java.util.*;
import java.io.*;

public class Josephus
{
   private static String WINNER = "Josephus";
   
   public static void main(String[] args) throws FileNotFoundException
   {
      ///* run numberCircle first with J_numbers.txt  */
      Scanner scanner = new Scanner(System.in);
      System.out.print("How many names (2-20)? ");
      int number = Integer.parseInt( scanner.next() );
      System.out.print("How many names to count off each time? "  );
      int countOff = Integer.parseInt( scanner.next() );
      ListNode winningPos = numberCircle( number, countOff, "J_numbers.txt");
      System.out.println(winningPos.getValue() + " wins!");  
     
      /* run josephusCircle next with J_names.txt  */
      System.out.println("\n ****  Now start all over. **** \n");
      System.out.print("Where should "+WINNER+" stand?  ");
      int winningPosition = Integer.parseInt( scanner.next() );        
      ListNode theWinner = josephusCircle( number, countOff, "J_names.txt", winningPosition);
      //ListNode theWinner = josephusCircle( number, countOff, "J_names-short.txt", winningPosition );
      System.out.println(theWinner.getValue() + " wins!");  
   }
   
   public static ListNode numberCircle( int number, int countOff, String filename ) throws FileNotFoundException {
      ListNode pointer = null;
      pointer = readNLinesOfFile( number, new File(filename) );
      pointer = countingOff( pointer, countOff, number );
      return pointer;
   }
   
   public static ListNode josephusCircle( int number, int countOff, String filename, int winningPosition) throws FileNotFoundException {
      ListNode pointer = null;
      pointer = readNLinesOfFile( number, new File(filename) );
      replaceAt( pointer, WINNER, winningPosition );
      pointer = countingOff( pointer, countOff, number );
      return pointer;
   }

   /* reads the names, calls insert(), builds the linked list.
	 */
   public static ListNode readNLinesOfFile( int number, File filename ) throws FileNotFoundException {
      ListNode pointer = null;
      Scanner scanner = new Scanner( filename );
      for ( int i = 0; i < number; i++ ) {
         Object item = scanner.next();
         pointer = insert( pointer, item );	
      }
      pointer = pointer.getNext();

      return pointer;
   }
   
   /* helper method to build the list.  Creates the node, then
    * inserts it in the circular linked list.
	 */
   public static ListNode insert(ListNode pointer, Object nodeName ) {
      if ( pointer != null ) {
         ListNode newNode = new ListNode( nodeName, pointer.getNext() );
         pointer.setNext( newNode );
         pointer = newNode;
      }
      else
      {
         pointer = new ListNode( nodeName, null );
         pointer.setNext( pointer );
      }
      
      return pointer;
   }
   
   /* Runs a Josephus game, counting off and removing each name. Prints after each removal.
      Ends with one remaining name, who is the winner. 
	 */
   public static ListNode countingOff(ListNode pointer, int count, int number ) {
      print( pointer );
      for ( int i = 0; i < number - 1; i++ ) {
         pointer = remove( pointer, count );
         print( pointer );
      }
      
      return pointer;
   }
   
   /* removes the node after counting off count-1 nodes.
	 */
   public static ListNode remove( ListNode pointer, int count)
   {
      ListNode lastNode = null;
      for ( int i = 0; i < count - 1; i++ ) {
         lastNode = pointer;
         pointer = pointer.getNext();
      }
      ListNode nextNode = pointer.getNext();
      lastNode.setNext( nextNode );
      
      return nextNode;
   }
   
   /* prints the circular linked list.
	 */
   public static void print( ListNode pointer )
   {
      Object pointerValue = pointer.getValue();
      while ( pointer.getNext().getValue() != pointerValue ) {
         System.out.print( pointer.getValue() + " " );
         pointer = pointer.getNext();
      }
      System.out.println( pointer.getValue() + " " );
   }
	
   /* replaces the value (the string) at the winning node.
	 */
   public static void replaceAt( ListNode pointer, Object nodeName, int position )
   {
      for ( int i = 0; i < position - 1; i++ ) {
         pointer = pointer.getNext();
      }
      
      pointer.setValue( nodeName );
   }
}

