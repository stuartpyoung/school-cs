/**
 * The class DLNode implements a node for a doubly linked list
 */
public class DLNode {
    private Object element;   // data element stored in this node
    private DLNode next;      // the next node
    private DLNode prev;      // the previous node

    /**
     * DLNode constructor
     */
    public DLNode ( Object e, DLNode p, DLNode n ) {
        element = e; prev = p; next = n;
    }
    public DLNode (  ) {
        element = ""; 
        prev = this; 
        next = this;
    }

    /**
     * sets the element/next/prev field of current node
     */
    public void setElement(Object newElement) { element = newElement; }
    public void setNext(DLNode newNext) { next = newNext; }
    public void setPrev(DLNode newPrev) { prev = newPrev; }

    /**
     * gets the element/next/prev field of current node
     */
    public Object getElement() { return element; }
    public DLNode getNext() { return next; }
    public DLNode getPrev() { return prev; }
}
 