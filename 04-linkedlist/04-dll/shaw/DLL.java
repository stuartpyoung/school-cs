// Name: Shaw Young  Date: 11/27/18
 
//  implements some of the List and LinkedList interfaces: 
//      size(), add(i, o), remove(i);  addFirst(o), addLast(o); 
//  This class also overrides toString().
//  the list is zero-indexed.
//  Uses DLNode.
 
public class DLL        //DoubleLinkedList
{
   private int size = 0;
   private DLNode head = new DLNode(); //dummy node--very useful--simplifies the code
 
   public int size() {
      return size;
   }
 
   /* appends obj to end of list; increases size;
        @return true  */
   public boolean add( Object obj ) {
      addLast(obj);
      return true;   
   }
 
   /* inserts obj at position index.  increments size.   */
   public void add( int index, Object node ) throws IndexOutOfBoundsException  //this the way the real LinkedList is coded
 {
      if( index > size || index < 0 )
         throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

      size += 1;           

      DLNode currentNode = head;
      for ( int i = 0; i < index; i++ ) {
         currentNode = currentNode.getNext();
      }
      DLNode newNode = new DLNode( node, currentNode, currentNode.getNext() );
      currentNode.setNext( newNode );
   }
 
   /* return obj at position index.    */
   public Object get( int index ) { 
      if(index >= size || index < 0)
         throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

      DLNode currentNode = head;
      for ( int i = 0; i < index + 1; i++ ) {
         currentNode = currentNode.getNext();
      }

      return currentNode.getValue();
   }
 
   /* replaces obj at position index. 
        returns the obj that was replaced*/
   public Object set( int index, Object node ) {
      if ( index >= size || index < 0 )
         throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
      /* enter your code below  */
      DLNode currentNode = head;
      for ( int i = 0; i < index + 1; i++ )
      {
         currentNode = currentNode.getNext();
      }
      Object oldName = currentNode.getValue();
      currentNode.setValue( node );
 
      return oldName;
   }
 
   /*  removes the node from position index (zero-indexed).  decrements size.
       @return the object of the node that was removed.    */
   public Object remove( int index ) {
      if( index >= size || index < 0 )
         throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

      DLNode currentNode = head;
      for ( int i = 0; i < index + 1; i++ ) {
         currentNode = currentNode.getNext();
      }
      DLNode previousNode = currentNode.getPrev();
      DLNode nextNode = currentNode.getNext();
      previousNode.setNext( nextNode );
      nextNode.setPrev( previousNode );
      size--;
 
      return currentNode.getValue();
   }
 
   /* inserts obj at front of list, increases size   */
   public void addFirst( Object node ) {
      DLNode newNode = new DLNode( node, head, head.getNext() );
      head.getNext().setPrev( newNode );
      head.setNext( newNode );
      size++;
   }
 
   /* appends obj to end of list, increases size    */
   public void addLast(Object node ) {
      DLNode newNode = new DLNode( node, head.getPrev(), head );
      head.getPrev().setNext( newNode );
      head.setPrev( newNode );
      size++;
   }
 
   /* returns the first element in this list  */
   public Object getFirst() {
      return head.getValue();
   }
 
   /* returns the last element in this list  */
   public Object getLast() {
      return head.getPrev().getValue();
   }
 
   /* returns and removes the first element in this list, or
       returns null if the list is empty  */
   public Object removeFirst() {
      if ( head.getNext() != null ) {
         return remove( 0 );
      }
 
      return null;
   }
 
   /* returns and removes the last element in this list, or
       returns null if the list is empty  */
   public Object removeLast() {
      if ( head.getNext() != null ) {
         return remove( size - 1 );
      }
 
      return null;
   }
 
   /*  returns a String with the values in the list in a 
       friendly format, for example   [Apple, Banana, Cucumber]
       The values are enclosed in [], separated by one comma and one space.
    */
   public String toString() {
      String output =  "[";
      output += head.getNext().getValue();
      Object start = head.getNext().getValue();
      DLNode currentNode = head.getNext().getNext();
      while ( currentNode.getNext().getValue() != start
             && currentNode.getValue() != null ) {
         output += ", " + currentNode.getValue();
         currentNode = currentNode.getNext();
      }
      output += "]";
      return output;
 
   }
}