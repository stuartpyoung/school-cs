// Name: Rio Young
// Date: 11/8/18
import java.util.*;
public class ListLab1
{
   public static void main(String[] args)
   {
      ListNode head = new ListNode("hello", null);
      head = new ListNode("foo", head);
      head = new ListNode("boo", head);
      head = new ListNode("nonsense", head);
      head = new ListNode("computer",
         	new ListNode("science",
         		new ListNode("java",
         			new ListNode("coffee", head)
         		)
         	)
         );
      print(head);
      print(head);
      
/*****************************************
 
 [computer, science, java, coffee, nonsense, boo, foo, hello]
 [computer, science, java, coffee, nonsense, boo, foo, hello]
 First = computer
 Second = science
 Pointer to Last = hello at ListNode@18767ad
 Copy of Last =    hello at ListNode@a7bdcd
 Insert what? abc
 [abc, computer, science, java, coffee, nonsense, boo, foo, hello, abc]
 

  **********************************************/

      
      /**** uncomment the code below for ListLab1 Extension  ****/
      
   	 System.out.println("First = " + first(head));
       System.out.println("Second = " + second(head));
       ListNode p = pointerToLast(head);
       System.out.println("Pointer to Last = " + p.getValue()+ " at " + p);
       ListNode c = copyOfLast(head);
       System.out.println("Copy of Last =    " + c.getValue()+ " at " + c);
   	 	
       Scanner in = new Scanner(System.in);
       System.out.print("Insert what? ");
       String x = in.next();
       head = insertFirst(head, x);
       head = insertLast(head, x);
       print(head);
   }
   public static void print(ListNode head)
   {
      System.out.print("[");
      while(head != null)
      {
         System.out.print(head.getValue());
         head = head.getNext();
         if(head != null)
            System.out.print(", ");
      }
      System.out.println("]");
   }
   
   /* enter your code here */
      public static ListNode copyNode( ListNode node )
      {
         if ( node == null )
         {
            return null;
         }
         
         return new ListNode( node.getValue(), node.getNext() );
      }
      
      public static ListNode copyList( ListNode node )
      {
         if ( node == null )
         {
            return null;
         }
         
         return new ListNode( node.getValue(), copyList( node.getNext() ) );
      }
      
      public static ListNode rest( ListNode h )
      {
         return copyList( h.getNext() );
      }
      
      public static Object first( ListNode head )
      {
         if ( head == null )
         {
            return null;
         }
         
         return head.getValue();
      }
      
      public static Object second( ListNode head )
      {
         //System.out.println( "head.getValue(): " + head.getValue() );
         if ( head == null )
         {
            return null;
         }

         ListNode second = head.getNext();
         
         if ( second == null )
         {
            return null;
         }
         
         return second.getValue();
      }
      
      public static ListNode pointerToLast( ListNode head )
      {
         //System.out.println( "head.getValue(): " + head.getValue() );
         if ( head == null )
         {
            return null;
         }
         
         ListNode last = head;
         ListNode next = last.getNext();
         while ( next != null )
         {
            last = next;
            next = next.getNext();
         }
         
         return last;
      }
      
      public static ListNode copyOfLast( ListNode head )
      {
         return copyNode( pointerToLast( head ) );
      }
      
      public static ListNode insertFirst( ListNode head, Object name )
      {
         return new ListNode( name, head );
      }
      
      public static ListNode insertLast( ListNode head, Object name )
      {
         ListNode lastNode = pointerToLast( head );
         lastNode.setNext( new ListNode( name, null ) );
         
         return head;
      }
      
}
//
//public static Object first( ListNode head )
//      {
//         //System.out.println( "head.getValue(): " + head.getValue() );
//         if ( head == null )
//         {
//            return null;
//         }
//         
//         ListNode first = head;
//         ListNode next = first.getNext();
//         while ( next != null )
//         {
//            first = next;
//            next = next.getNext();
//         }
//         
//         return first.getValue();
//      }
//      
//      public static Object second( ListNode head )
//      {
//         //System.out.println( "head.getValue(): " + head.getValue() );
//         if ( head == null )
//         {
//            return null;
//         }
//         
//         ListNode second = head;
//         ListNode first = second.getNext();
//         
//         if ( first == null )
//         {
//            return null;
//         }
//         
//         while ( first.getNext() != null )
//         {
//            second = first;
//            first = first.getNext();
//         }
//         
//         return second.getValue();
//      }
//
