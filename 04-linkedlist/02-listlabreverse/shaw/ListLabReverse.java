// Name: Shaw Young   Date: 12/18/18

// OUTPUT:
//print the original:                             [computer, science, java, coffee, nonsense, boo, foo, hello]
//recur and print:                                [hello, foo, boo, nonsense, coffee, java, science, computer]
//
//original is unchanged:                          [computer, science, java, coffee, nonsense, boo, foo, hello]
//reverse by building a new list:                 [hello, foo, boo, nonsense, coffee, java, science, computer]
//iterate with 3 pointers:                        [computer, science, java, coffee, nonsense, boo, foo, hello]
//recur with 2 pointers:                          [hello, foo, boo, nonsense, coffee, java, science, computer]
//recur with pointers and append:                 [computer, science, java, coffee, nonsense, boo, foo, hello]
//Mind Bender reverse:                            [hello, foo, boo, nonsense, coffee, java, science, computer]

// EXPECTED:
 //print the original: 				   [computer, science, java, coffee, nonsense, boo, foo, hello]
 //recur and print: 					   [hello, foo, boo, nonsense, coffee, java, science, computer]
 //
 //original is unchanged: 				[computer, science, java, coffee, nonsense, boo, foo, hello]
 //reverse by building a new list: 	[hello, foo, boo, nonsense, coffee, java, science, computer]
 //iterate with 3 pointers:			   [computer, science, java, coffee, nonsense, boo, foo, hello]
 //recur with 2 pointers: 				[hello, foo, boo, nonsense, coffee, java, science, computer]
 //recur with pointers and append: 	[computer, science, java, coffee, nonsense, boo, foo, hello]
 //Mind Bender reverse:					[hello, foo, boo, nonsense, coffee, java, science, computer]

/*****************************************
Demonstrates many ways to reverse a list made of ListNodes.
******************************************/
public class ListLabReverse
{
   public static void main(String[] args)
   {
      ListNode head = new ListNode("hello", null);
      head = new ListNode("foo", head);
      head = new ListNode("boo", head);
      head = new ListNode("nonsense", head);
      head = new ListNode("computer",
         new ListNode("science",
         new ListNode("java",
         new ListNode("coffee", head))));
         
      System.out.print("print the original: \t\t\t\t");
      print(head);
         
      System.out.print("recur and print: \t\t\t\t");
      recurAndPrint(head);
      
      System.out.println();
      System.out.print("original is unchanged: \t\t\t\t");
      print(head);
      
      System.out.print("reverse by building a new list: \t\t");
      head = reverseBuildNew(head);
      print( head );      
      
      System.out.print("iterate with 3 pointers:\t\t\t");
      head = iterateThreePointers(head);
      print(head);
      	
      System.out.print("recur with 2 pointers: \t\t\t\t");
      head = recurTwoPointers(null, head);
      print(head);
              
      System.out.print("recur with pointers and append: \t\t");
      head = recurPointersAppend(head);
      print(head);
      	
      System.out.print("Mind Bender reverse:\t\t\t\t");
      head = mindBender(head);
      print(head);
   }
   
   public static void print(ListNode head)
   {
      System.out.print("[");
      while(head != null) {
         System.out.print(head.getValue());
         head = head.getNext();
         if(head != null)
            System.out.print(", ");
      }
      System.out.println("]");
   }
   
	/*********************************************
	1. This approach doesn't actually reverse the list.  It only prints
	the list in reverse order.  recurAndPrint() prints the square 
	brackets and calls helper().  helper() is recursive.
	********************************************************/
   public static void recurAndPrint( ListNode head ) {
      System.out.print( "[" );
      helper( head );
      System.out.println( "]" );
   }
   
   private static void helper( ListNode head ) {
      String output = "";
      if ( head.getNext() ==  null ) {
         output += head.getValue();
      }
      else {
         helper( head.getNext() );
         if ( head.getNext() != null ) {
            output += ", " ;         
         }
         output += head.getValue();
      }

      System.out.print( output );
   }
   
   /*********************************************
	2. This iterative method (for or while) produces a copy of the 
	reversed list.	 For each node going forward, make a new node and 
	link it to the list.	The list will naturally be in reverse. 
	***********************************************************/
   public static ListNode reverseBuildNew( ListNode head ) {
      ListNode newHead = new ListNode( head.getValue(), null );
      ListNode nextNode = head.getNext();
      if ( nextNode != null ) {
         while ( nextNode != null ) {
            newHead = new ListNode( nextNode.getValue(), newHead );
            nextNode = nextNode.getNext();
         }
      }
      
      return newHead;
   }

   /*******************************************
	3a. This iterative method (while) uses 3 pointers to reverse 
	the list in place.   The two local pointers are called
	prev and next.
   ********************************************************/
   public static ListNode iterateThreePointers( ListNode head ) {
      if ( head == null ) {
         return head;
      }
      
      ListNode currentNode = head;
      ListNode previousNode = null;
      ListNode nextNode = null;
      while ( currentNode != null ) {
          nextNode = currentNode.getNext();
          currentNode.setNext( previousNode );
          previousNode = currentNode;
          currentNode = nextNode;
      }
      head = previousNode;

      return previousNode;
   }
   	
	/**************************************************
	3b. This recursive method uses two pointers as arguments to reverse 
	the list in place. Each level creates and uses a third pointer, called "next".
   ********************************************************/
   public static ListNode recurTwoPointers( ListNode previousNode, ListNode head ) {
      if ( head == null ) {
         return head;
      }
      else {
         ListNode nextNode = head.getNext();
         while ( nextNode != null ) {
            head.setNext( previousNode );
            previousNode = head;
            
            if ( nextNode != null ) {
               head = nextNode;
               nextNode = nextNode.getNext();
            }
         }
         head.setNext( previousNode );
         previousNode = head;   
         
         return previousNode;         
      }
   } 
   
   /**********************************************
	3c. On each recursive level, find pointerToLast() and 
	nextToLast(). Make a new last. On way back, append() 
	one level to the other. 
	********************************************************/
   public static ListNode recurPointersAppend(ListNode head) {
      if ( head == null || head.getNext() == null )
      {
         return head;         
      }
      else {
         ListNode lastNode = pointerToLast( head );
         nextToLast( head ).setNext( null );
         append( lastNode, recurPointersAppend( head ) );
   
         return lastNode;
      }
   }
   
   public static ListNode pointerToLast( ListNode head ) {
      if ( head == null ) {
         return head;
      }
      else {
         ListNode lastNode = head;
         ListNode nextNode = lastNode.getNext();
         while ( nextNode != null ) {
            lastNode = nextNode;
            nextNode = nextNode.getNext();
         }
         
         return lastNode;         
      }      
   }
   
   private static ListNode nextToLast(ListNode head) {
      if ( head == null ) {
         return head;
      }
      else {
         ListNode lastNode = head;
         ListNode nextNode = lastNode.getNext();
         ListNode nextToLastNode = null; 
         while ( nextNode != null ) {
            nextToLastNode = lastNode;
            lastNode = nextNode;
            nextNode = nextNode.getNext();
         }
         
         return nextToLastNode;         
      }      
   }
   
   private static ListNode append( ListNode node, ListNode nextNode ) {
      node.setNext( nextNode );
      return node;
   }

   /**********************************************
   3d. This difficult method reverses the list in place, using one
   local pointer. Start with pointerToLast(). The helper method
   is recursive.
	********************************************************/
   public static ListNode mindBender(ListNode head) {
      ListNode temp = pointerToLast(head);
      mindBenderHelper( head );
      head.setNext(null);
      return temp;
   }
   
   public static void mindBenderHelper(ListNode head) {
      if ( head.getNext() == null ) {
         // DO NOTHING
      }
      else {
         mindBenderHelper( head.getNext() );
         head.getNext().setNext( head );
      }
   }
}

