 //Name: Shaw Young    Date: 4/8/19
 
import java.util.*;

/* implement the API for java.util.PriorityQueue
 * test this class with HeapPriorityQueue_Driver.java.
 * test this class with LunchRoom.java.
 * add(E) and remove()  must work in O(log n) time
 */
public class HeapPriorityQueue<E extends Comparable<E>> 
{
   private ArrayList<E> myHeap;
   
   public HeapPriorityQueue() {
      myHeap = new ArrayList<E>();
      myHeap.add(null);
   }
   
   public E remove() {
      E output = myHeap.remove( 1 );
      int last = myHeap.size() - 1;
      int first = 1;      
      for ( int i = 0; i < Math.abs( myHeap.size() / 2); i++ ) {
         heapDown( first, last );
         first++;
      }

      return output;
   }
   
   private void heapDown(int k, int size) {
      if ( size < 2 * k ) {
         return;
      }

      int minChild = getMinChild( k, size );

      if ( myHeap.get( k ).compareTo( myHeap.get( minChild ) ) > 0 ) {
         swap ( k, minChild );
         heapDown( minChild, size );
      }
      
      // FINAL SWAP IF NEEDED
      if ( myHeap.get( k ).compareTo( myHeap.get( minChild ) ) > 0 ) {
         swap( k, minChild );
      }
   }
   
   public int getMinChild ( int k, int size ) {
      int left = 2 * k;
      int right = 2 * k + 1;

      if ( right > size ) {
         return left;
      }
      else if ( myHeap.get( right ).compareTo( myHeap.get( left ) ) < 0 ) {
         return right;
      }

      return left;
   }

   public boolean add(E obj) {
     myHeap.add( obj );
     heapUp( myHeap.size() - 1 );
     
     return true;
   }
   
   private void heapUp( int k ) {
      int parent = k / 2;
      if ( parent == 0 ) {
         return;
      }
      if ( myHeap.get( k ).compareTo( myHeap.get( parent ) ) < 0 ) {
         swap( parent, k );
         heapUp( parent );
      }
   }
   
   public String toString() {
      return myHeap.toString();
   }  

   public E peek() {
      if ( myHeap.size() == 1 ) {
         return null;
      }

      return myHeap.get( 1 );
   }
 
   public boolean isEmpty() {
     return myHeap.size() == 1 && myHeap.get( 0 ) == null;
   }
   
   private void swap(int a, int b) {
     E temp = myHeap.get( a );
     myHeap.set( a, myHeap.get( b ) );
     myHeap.set( b, temp );
   }
         
}
