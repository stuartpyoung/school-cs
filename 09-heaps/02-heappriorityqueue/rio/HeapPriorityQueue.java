 //Name: Rio Young  
 //Date: 4/1/19
 
import java.util.*;


/* implement the API for java.util.PriorityQueue
 * test this class with HeapPriorityQueue_Driver.java.
 * test this class with LunchRoom.java.
 * add(E) and remove()  must work in O(log n) time
 */
public class HeapPriorityQueue<E extends Comparable<E>> 
{
   private ArrayList<E> myHeap;
   
   public HeapPriorityQueue()
   {
      myHeap = new ArrayList<E>();
      myHeap.add( null );
   }
   
   public E remove()
   {
      E output = myHeap.remove( 1 );
      int last = myHeap.size() - 1;
      int first = 1;
      int limit = Math.abs( myHeap.size() / 2 );
      for ( int i = 0; i < limit; i++ )
      {
         heapDown( first, last );
         first++;
      }
      // System.out.println( "remove    FINAL myHeap: " + myHeap.toString() );

      return output;
   }
   
   private void heapDown(int k, int size)
   {
      // System.out.println( "heapDown    k: " + k );
      // System.out.println( "heapDown    size: " + size );
      // System.out.println( "heapDown    myHeap IN: " + myHeap.toString() );

      int left = 2 * k;
      int right = 2 * k + 1;
      int minChild = -1;
      
      if ( left > size )
      {
         return;
      }
      else
      {
         minChild = left;
      }
      
      if ( ( right < size ) && ( myHeap.get( right ).compareTo( myHeap.get( left ) ) < 0 ) )
      {
         minChild = right;
      }
      // System.out.println( "heapDown    minChild: " + minChild );
      // System.out.println( "heapDown    myHeap.get( minChild ): " + myHeap.get( minChild) );      
      // System.out.println( "heapDown    myHeap OUT: " + myHeap.toString() );

      if ( myHeap.get( k ).compareTo( myHeap.get( minChild ) ) > 0 )
      {
         // System.out.println( "heapDown    DOING SWAP with minChild: " + minChild );
         swap ( k, minChild );
         heapDown( minChild, size );
      }
      
      if ( myHeap.get( k ).compareTo( myHeap.get( minChild ) ) > 0 )   
      {
         swap( k, minChild );
      }
   }
   
   public boolean add(E obj)
   {
     myHeap.add( obj );
     heapUp( myHeap.size() - 1 );
     
     //System.out.println( "add    AFTER ADD myHeap: " + myHeap );
     
     return true;
   }
   
   private void heapUp( int k )
   {
      int parent = Math.abs( k / 2 );
      if ( parent == 0 )
      {
         return;
      }
      if ( myHeap.get( k ).compareTo( myHeap.get( parent ) ) < 0 )
      {
         swap( parent, k );
         heapUp( parent );
      }
   }
   
   public String toString()
   {
      return myHeap.toString();
   }  

   public E peek()
   {
      if ( myHeap.size() == 1 )
      {
         return null;
      }

      return myHeap.get( 1 );
   }
 
   public boolean isEmpty()
   {
      if ( myHeap.size() == 1 )
      {
         return true;
      }
     
     return false;
   }
   
   private void swap(int a, int b)
   {
     E temp = myHeap.get( a );
     myHeap.set( a, myHeap.get( b ) );
     myHeap.set( b, temp );
   }
         
}
