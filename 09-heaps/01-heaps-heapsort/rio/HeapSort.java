// Name: Rio Young
// Date: 3/27/19

import java.text.DecimalFormat;


public class HeapSort
{
   public static int SIZE;  //9 or 100
	
   public static void main(String[] args)
   {
      //Part 1: Given a heap, sort it. Do this part first. 
      SIZE = 9;  
      double heap[] = {-1,99,80,85,17,30,84,2,16,1};
      
      display(heap);
      sort(heap);
      display(heap);
      System.out.println(isSorted(heap));
      
      //Part 2:  Generate 100 random numbers, make a heap, sort it.
       SIZE = 100;
       double[] heap2 = new double[SIZE + 1];
       heap2 = createRandom(heap2);
       display(heap2);
       makeHeap(heap2, SIZE);
       display(heap2); 
       sort(heap2);
       display(heap2);
       System.out.println(isSorted(heap2));
   }
   
	//******* Part 1 ******************************************
   public static void display(double[] array)
   {
      for ( int k = 1; k < array.length; k++ )
      {
         System.out.print( array[ k ] + "    " );
      }
      System.out.println();	
   }
   
   public static void sort( double[] array )
   {
      /* enter your code here */
      int last = array.length - 1;
      int first = 1;
      
      for ( int i = 0; i < array.length - 1; i++ )
      {
         System.out.print( "BEFORE SWAP:    " );
         display( array );
         swap( array, first, last );
         System.out.print( "AFTER SWAP:    " );
         display( array );
         last--;
         heapDown( array, first, last );
      }
      
      if ( array[ 2 ] > array[ 3 ] )   //just an extra swap, if needed.
      {
         swap( array, 2, 3 );
      }
   }
  
   public static void swap( double[] array, int a, int b )
   {
      double temp = array[ a ];
      array[ a ] = array[ b ];
      array[ b ] = temp;
   }
   
   public static void heapDown(double[] array, int k, int size)
   {
      System.out.println( "heapDown    k: " + k );
      System.out.println( "heapDown    size: " + size );
      System.out.println( "heapDown    array IN: " );
      display( array );

      int left = 2 * k;
      int right = 2 * k + 1;
      int maxChild = -1;
      
      if ( left > size )
      {
         return;
      }
      else
      {
         maxChild = left;
      }
      
      if ( ( right < size ) && array[ right ] > array[ left ] )
      {
         maxChild = right;
      }

      System.out.println( "heapDown    maxChild: " + maxChild );
      System.out.println( "heapDown    array[ maxChild ]: " + array[ maxChild ] );      
      System.out.println( "heapDown    array OUT: " );
      display( array );

      if ( array[ k ] < array[ maxChild ] )
      {
         swap ( array, k, maxChild );
         heapDown( array, maxChild, size );
      }
   }
   
   public static boolean isSorted( double[] arr )
   {
      for ( int i = 0; i < arr.length - 1; i++ )
      {
         if ( arr[ i ] > arr[ i + 1 ] )
         {
            return false;
         }
      }
      
      return true;  

   }
   
   //****** Part 2 *******************************************

   //Generate 100 random numbers (between 1 and 100, formatted to 2 decimal places) 
   public static double[] createRandom(double[] array)
   {  
      array[0] = -1;   //because it will become a heap
      for ( int i = 1; i < array.length; i++ )
      {
         DecimalFormat df = new DecimalFormat("####0.00");
         array[ i ] = Double.parseDouble( df.format( Math.random() * 100 ) );
      }
      return array;
   }
   
   //turn the random array into a heap
   public static void makeHeap(double[] array, int size)
   {
      for ( int k = size / 2; k >= 1; k-- )
      {
         heapDown( array, k, size );
      }
   }
}

