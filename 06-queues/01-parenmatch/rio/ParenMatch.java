// Name: Rio Young
// Date: 1/7/19

import java.util.*;

public class ParenMatch
{
   public static final String left  = "([{<";
   public static final String right = ")]}>";
   
   public static void main(String[] args)
   {
      System.out.println("Parentheses Match");
      ArrayList<String> parenExp = new ArrayList<String>();
      /* enter test cases here */
      
      parenExp.add( "5+7" ); //g 
      parenExp.add( "(5+7)" ); //g 
      parenExp.add( ")5+7(" ); //b 
      parenExp.add( "((5+7)*3)" ); //g 
      parenExp.add( "<{5+7}*3>" ); //g 
      parenExp.add( "[(5+7)*]3" ); //g 
      parenExp.add( "(5+7)*3" ); //g
      parenExp.add( "5+(7*3)" ); //g 
      parenExp.add( "[(5+7)*3" ); //b 
      parenExp.add( "[(5+7]*3)" ); //b 
      parenExp.add( "[(5+7)*3])" ); //b 
      parenExp.add( "([(5+7)*3]" ); //b 
      
   
      for( String s : parenExp )
      {
         boolean good = checkParen(s);
         if(good)
            System.out.println(s + "\t good!");
         else
            System.out.println(s + "\t BAD");
      }
   }
   
   public static boolean checkParen(String exp)
   {
      Stack<String> stack = new Stack<String>(); 
      for ( int i = 0; i < exp.length(); i++ )
      {
         String currentSymbol = exp.substring( i, i + 1 );
         //System.out.println( "currentSymbol: " + currentSymbol );
         if ( left.indexOf( currentSymbol ) != -1 )
         {
            stack.push( currentSymbol );
         }
         else if ( right.indexOf( currentSymbol ) != -1 )
         {
            if ( stack.isEmpty() )
            {
               return false;
            }
            else
            {
               if ( left.indexOf( stack.peek() ) == right.indexOf( currentSymbol ) )
               {
                  stack.pop();
               }
            }
         }
      }
      if ( !stack.isEmpty() )
      {
         return false;
      }
      
      return true;
   }
}

/*
 Parentheses Match
 5+7	 good!
 (5+7)	 good!
 )5+7(	 BAD
 ((5+7)*3)	 good!
 <{5+7}*3>	 good!
 [(5+7)*]3	 good!
 (5+7)*3	 good!
 5+(7*3)	 good!
 ((5+7)*3	 BAD
 [(5+7]*3)	 BAD
 [(5+7)*3])	 BAD
 ([(5+7)*3]	 BAD
 */
