// Name: Rio Young
// Date: 1/15/19

import java.io.*;
import java.util.*;

public class SongQueue
{
   private static Scanner keyboard;  //use this global Scanner for this lab only
   private static Queue<String> songQueue;
   
   public static void main(String[] args) throws Exception
   {
      keyboard = new Scanner(System.in);
      songQueue = readPlayList();
      printSongList();
      
      String prompt = "Add song (A), Play song (P), Delete song (D), Quit (Q):  ";
      String str = "";
      do{      
         System.out.print(prompt);
         str = keyboard.nextLine().toUpperCase();
         processRequest( str );
         System.out.println();
      }while(!str.equals("Q"));
      //System.out.println( songQueue.peek() );
      System.out.println( "No more music for you today. Goodbye!" );
   }
   
   public static Queue<String> readPlayList() throws IOException
   {
      Scanner infile = new Scanner (new File("songs.txt"));
      Queue<String> queue = new LinkedList<String>();
      String line = null;
      String name;
      //( line = infile.nextLine() ) != null
      while ( infile.hasNextLine() )
      {
         line = infile.nextLine();
         name = line.substring( 0, line.indexOf( '-' ) - 1 );
         //System.out.println( "readPlayList   name: " + name );
         queue.add( name );
         //System.out.println( "readPlayList   ADDING SONG" );
      }
      return queue;
   }
   
   public static void processRequest(String str)
   {
      //System.out.println( "processRequest    str: *" + str + "*" );
      //printSongList();
      if ( str.equals( "A" ) )
      {
         add();
      }
      else if ( str.equals( "P" ) )
      {
         play();
      }
      else if ( str.equals( "D" ) )
      {
         delete();
      }      
   }

   public static void add()
   {
      //System.out.println( "ADDING" );
      System.out.print( "Song to add? " );
      String name = keyboard.nextLine();
      //System.out.println( "name: " + name );
      songQueue.add( name );
      System.out.print( "Your music queue: " );
      printSongList();
   }
   
   public static void delete()
   {
      //System.out.println( "DELETING" );
      //printSongList();
      
      boolean found = false;
      Queue<String> queue = new LinkedList<String>();
      
      if ( songQueue.isEmpty() )
      {
         System.out.println( "Empty queue!" );
      }
      else
      {
         System.out.print( "Your music queue: " );
         printSongList();
         System.out.print( "Enter song to delete (exact match): " );
         String name = keyboard.nextLine();
         //System.out.println( "name: " + name );
         
         while ( ! songQueue.isEmpty() )
         {
            String song = songQueue.remove();
            //System.out.println( "song: " + song );
            if ( song.equals( name ) )
            {
               found = true;
               //do nothing
            }
            else
            {
               queue.add( song );
            }
         }
         
         if ( ! found )
         {
            System.out.println( "Error: Song not in list." );
         }
         
         songQueue = queue;
         System.out.print( "Your music queue: " );
         printSongList();
      }
   }
   
   public static void play()
   {
      if ( !songQueue.isEmpty() )
      {
         System.out.println( "Now playing: " + songQueue.remove() );
      }
      else
      {
         System.out.println( "Empty queue!" );
      } 
   }
   
   public static void printSongList()
   {
      System.out.println( songQueue.toString() );
   }
   
   public static Queue<String> getQueue()
   {
      return songQueue;
   }
   
}

/*********************************************

 Your music queue: [Really Love, Uptown Funk, Thinking Out Loud, Alright, Traveller, Alright]
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Now playing: Really Love
 Your music queue: [Uptown Funk, Thinking Out Loud, Alright, Traveller, Alright]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Now playing: Uptown Funk
 Your music queue: [Thinking Out Loud, Alright, Traveller, Alright]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  d
 Your music queue: [Thinking Out Loud, Alright, Traveller, Alright]
 Delete which song (exact match)?  Alright
 Your music queue: [Thinking Out Loud, Traveller]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  d
 Your music queue: [Thinking Out Loud, Traveller]
 Delete which song (exact match)?  xxx
 Error:  Song not in list.
 Your music queue: [Thinking Out Loud, Traveller]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  a
 Song to add? Girl Crush
 Your music queue: [Thinking Out Loud, Traveller, Girl Crush]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Now playing: Thinking Out Loud
 Your music queue: [Traveller, Girl Crush]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Now playing: Traveller
 Your music queue: [Girl Crush]
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Now playing: Girl Crush
 Your music queue: []
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  p
 Empty queue!
 Your music queue: []
 
 Add song (A), Play song (P), Delete song (D), Quit (Q):  q
 
 No more music today!

**********************************************/