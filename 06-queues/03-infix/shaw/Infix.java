// Name: Shaw Young Date: 4/5/19

import java.util.*;

public class Infix
{  
   public static void main(String[] args) {
      System.out.println("Infix  \t-->\tPostfix\t\t-->\tEvaluate");
     /*build your list of Infix expressions here  */
      ArrayList<String> infixExp = new ArrayList<String>();

      infixExp.add( "3 + 4 * 5" );                 //	3 4 5 * +			   23
      infixExp.add( "3 * 4 + 5" ); 			         // 3 4 * 5 +			   17
      infixExp.add( "( -5 + 15 ) - 6 / 3" );			// -5 15 + 6 3 / -		8
      infixExp.add( "( 3 + 4 ) * ( 5 + 6 )" );		// 3 4 + 5 6 + *			77
      infixExp.add( "( 3 * ( 4 + 5 ) - 2 ) / 5" ); // 3 4 5 + * 2 - 5 /		5
      infixExp.add( "8 + -1 * 2 - 9 / 3" );			// 8 -1 2 * + 9 3 / -	3
      infixExp.add( "3 * ( 4 * 5 + 6 )" );			// 3 4 5 * 6 + *			78
   
      for( String infix : infixExp ) {
         //System.out.println("INFIX: " + infix );
         String pf = infixToPostfix(infix);
         System.out.println(infix + "\t\t\t" + pf + "\t\t\t" + Postfix.eval(pf));  //Postfix must work!
      }
   }
   
   public static String infixToPostfix( String infix ) {
      List<String> infixParts = new ArrayList<String>(Arrays.asList(infix.split(" ")));

    String output = "";
    Stack<String> stack = new Stack<String>();
    for ( int i = 0; i < infixParts.size(); i++ ) {
         if ( ! Postfix.isOperator( infixParts.get( i ) ) && ! isABracket( infixParts.get( i ) ) ) {
            output += infixParts.get( i );
            output += " ";
         }
         else if ( infixParts.get( i ).equals( ")" ) ) {
            while ( ! stack.peek().equals( "(" ) ) {
               output += stack.pop();
               output += " ";
            }
            stack.pop();
         }
         else if ( infixParts.get( i ).equals( "(" ) ) {
            stack.push( infixParts.get( i ) );
         }
         else {
            if ( stack.isEmpty() || stack.peek().equals( "(" ) ) {
               stack.push( infixParts.get( i ) );
            }
            else if ( isLower( stack.peek().charAt( 0 ), infixParts.get( i ).charAt( 0 ) ) ) {
               stack.push( infixParts.get( i ) );
            }
            else {
               while ( stack.size() != 0
                  && ! isLower( stack.peek().charAt( 0 ), infixParts.get( i ).charAt( 0 ) ) 
                  && ! stack.peek().equals( "(" )
               ) {
                  output += stack.pop();                  
                  output += " ";
               }
               stack.push( infixParts.get( i ) );
            }
         }
      }
    
    while ( stack.size() != 0 ) {
      output += stack.pop();
      output += " ";
    }
    
    return output;
   }
   
	//returns true if character1 has lower or equal precedence than character2
   public static boolean isLower(char character1, char character2) {    
      String firstCharacter = Character.toString( character1 );
      String secondCharacter = Character.toString( character2 );

      if ( firstCharacter.equals( secondCharacter ) ) {
         return true;
      }

      if ( ( firstCharacter.equals( "-" ) || firstCharacter.equals( "+" ) ) && (  secondCharacter.equals( "/" ) || secondCharacter.equals( "*" ) ) )  {
         return true;
      }
      
      if ( ( firstCharacter.equals( "*" ) && secondCharacter.equals( "/" ) ) || (  firstCharacter.equals( "/" ) && secondCharacter.equals( "*" ) ) ) {
         return false;
      }

      if ( ( firstCharacter.equals( "-" ) && secondCharacter.equals( "+" ) ) || (  firstCharacter.equals( "+" ) && secondCharacter.equals( "-" ) ) ) {
         return false;
      }
      
      return false;      
   }
   
   public static boolean isABracket(String currentSymbol ) {
      if ( "()".indexOf( currentSymbol ) != -1 ) {
         return true;
      }

      return false;
   }

}
	
/********************************************

 Infix  	-->	Postfix		-->	Evaluate
 3 + 4 * 5			3 4 5 * +			23
 3 * 4 + 5			3 4 * 5 +			17
 ( -5 + 15 ) - 6 / 3			-5 15 + 6 3 / -			8
 ( 3 + 4 ) * ( 5 + 6 )			3 4 + 5 6 + *			77
 ( 3 * ( 4 + 5 ) - 2 ) / 5			3 4 5 + * 2 - 5 /			5
 8 + -1 * 2 - 9 / 3			8 -1 2 * + 9 3 / -			3
 3 * ( 4 * 5 + 6 )			3 4 5 * 6 + *			78
 
***********************************************/