// Name: Rio Young
// Date: 1/10/19

import java.util.*;

public class Infix
{  
   public static void main(String[] args)
   {
      System.out.println("Infix  \t-->\tPostfix\t\t-->\tEvaluate");
     /*build your list of Infix expressions here  */
      ArrayList<String> infixExp = new ArrayList<String>();
      //
      infixExp.add( "3 + 4 * 5" );                 //	3 4 5 * +			   23
      infixExp.add( "3 * 4 + 5" ); 			         // 3 4 * 5 +			   17
      infixExp.add( "( -5 + 15 ) - 6 / 3" );			// -5 15 + 6 3 / -		8
      infixExp.add( "( 3 + 4 ) * ( 5 + 6 )" );		// 3 4 + 5 6 + *			77
      infixExp.add( "( 3 * ( 4 + 5 ) - 2 ) / 5" ); // 3 4 5 + * 2 - 5 /		5
      infixExp.add( "8 + -1 * 2 - 9 / 3" );			// 8 -1 2 * + 9 3 / -	3
      infixExp.add( "3 * ( 4 * 5 + 6 )" );			// 3 4 5 * 6 + *			78
 

   
      for( String infix : infixExp )
      {
         String pf = infixToPostfix(infix);
         System.out.println(infix + "\t\t\t" + pf + "\t\t\t" + Postfix.eval(pf));  //Postfix must work!
      }
   }
   
   public static String infixToPostfix(String infix)
   {
      List<String> infixParts = new ArrayList<String>(Arrays.asList(infix.split(" ")));
      /* enter your code here  */
    String result = "";
    Stack<String> stack = new Stack<String>();
    for ( int i = 0; i < infixParts.size(); i++ )
      {
         if ( ! Postfix.isOperator( infixParts.get( i ) ) && ! isBrackets( infixParts.get( i ) ) )
         {
            result += infixParts.get( i ) + " ";
            //System.out.println( "result: " + result );
         }
         else if ( infixParts.get( i ).equals( "(" ) )
         {
            stack.push( infixParts.get( i ) );
            //System.out.println( "pushed to stack: " + infixParts.get( i ) );
         }
         else if ( infixParts.get( i ).equals( ")" ) )
         {
            while ( ! stack.peek().equals( "(" ) )
            {
               result += stack.pop() + " ";
               //System.out.println( "result: " + result );
            }
            stack.pop();
         }
         else
         {
            
            //System.out.println( "inside else  infixParts.get( i ): " + infixParts.get( i ) );
            if ( stack.isEmpty() || stack.peek().equals( "(" ) )
            {
               stack.push( infixParts.get( i ) );
               //System.out.println( "pushed to stack: " + infixParts.get( i ) );
            }
            else if ( isLower( stack.peek().charAt( 0 ), infixParts.get( i ).charAt( 0 ) ) )
            {
               stack.push( infixParts.get( i ) );
               //System.out.println( "pushed to stack: " + infixParts.get( i ) );
            }
            else
            {
               result += stack.pop() + " ";
               stack.push( infixParts.get( i ) );
            }
         }
         
      }
    
    while ( stack.size() != 0 )
    {
      result += stack.pop() + " ";
    }
    //System.out.println( "result: " + result );
    
    return result;
   }
   
	//returns true if c1 has lower or equal precedence than c2
   public static boolean isLower(char c1, char c2)
   {
      String token1 = "" + c1;
      String token2 = "" + c2;
      if( token1.equals( token2 ) )
      {
         return true;
      }
      if ( ( token1.equals( "+" ) || token1.equals( "-" ) ) && (  token2.equals( "*" ) || token2.equals( "/" ) ) ) 
      {
         return true;
      }
      
      return false;
      
   }
   
   public static boolean isBrackets(String currentSymbol )
   {
      String operators = "()";
      for ( int i = 0; i < operators.length(); i++ )
      {
         if ( currentSymbol.equals( operators.substring( i, i + 1 ) ) )
         {
            return true;
         }
      }
      return false;
   }

}
	
/********************************************

 Infix  	-->	Postfix		-->	Evaluate
 3 + 4 * 5			3 4 5 * +			23
 3 * 4 + 5			3 4 * 5 +			17
 ( -5 + 15 ) - 6 / 3			-5 15 + 6 3 / -			8
 ( 3 + 4 ) * ( 5 + 6 )			3 4 + 5 6 + *			77
 ( 3 * ( 4 + 5 ) - 2 ) / 5			3 4 5 + * 2 - 5 /			5
 8 + -1 * 2 - 9 / 3			8 -1 2 * + 9 3 / -			3
 3 * ( 4 * 5 + 6 )			3 4 5 * 6 + *			78
 
***********************************************/