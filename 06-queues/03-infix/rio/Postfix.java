// Name: Rio Young
// Date: 1/8/19

import java.util.*;

public class Postfix
{
   public static void main(String[] args)
   {
      System.out.println("Postfix  -->  Evaluate");
      ArrayList<String> postfixExp = new ArrayList<String>();
      /*  build your list of expressions here  */
      postfixExp.add( "3 4 5 * +" );		//23 
      postfixExp.add( "3 4 * 5 +" );		// 17
      postfixExp.add( "10 20 + -6 6 * +" );		// -6
      postfixExp.add( "3 4 + 5 6 + *" );		// 77
      postfixExp.add( "3 4 5 + * 2 - 5 /" );		// 5
      postfixExp.add( "8 1 2 * + 9 3 / -" );		// 7
      postfixExp.add( "2 3 ^" );		// 8
      postfixExp.add( "20 3 %" );		// 2
      postfixExp.add( "21 3 %" );		// 0
      postfixExp.add( "22 3 %" );		// 1
      postfixExp.add( "23 3 %" );		// 2
      postfixExp.add( "5 !" );		// 120
      postfixExp.add( "1 1 1 1 1 + + + + !" );		// 120
      
   
      
      for( String pf : postfixExp )
      {
         System.out.println(pf + "\t\t" + eval(pf));
      }
   }
   
   public static int eval(String pf)
   {
      List<String> postfixParts = new ArrayList<String>(Arrays.asList(pf.split(" ")));
      /*  enter your code here  */
      Stack<Integer> stack = new Stack<Integer>();
      for ( int i = 0; i < postfixParts.size(); i++ )
      {
         if ( !isOperator( postfixParts.get( i ) ) )
         {
            stack.push( Integer.parseInt( postfixParts.get( i ) ) );
         }
         else if ( postfixParts.get( i ) == "(" || postfixParts.get( i ) == ")" )
         {
            break;
         }
         else
         {
            //System.out.println( "stack.peek()1: " + stack.peek() );
            int a =  stack.pop();
            
            int b =  0;
            if ( ! stack.isEmpty() )
            {
               //System.out.println( "stack.peek()2: " + stack.peek() );
               //System.out.println();
               b = stack.pop();
            }
            
            stack.push( eval( a, b, postfixParts.get( i ) ) );
         }
      }
      
      
      return stack.peek();
   }
   
   public static int eval(int a, int b, String operatorSymbol )
   {
      int finalNumber = 0;
      if ( operatorSymbol.equals("+") )
      {
         finalNumber = a + b;
      }
      if ( operatorSymbol.equals("*") )
      {
         finalNumber = a * b;
      }
      if ( operatorSymbol.equals("-") )
      {
         finalNumber = b - a;
      }
      if ( operatorSymbol.equals("/") )
      {
         finalNumber = b / a;
      }
      if ( operatorSymbol.equals("%") )
      {
         finalNumber = b % a;
      }
      if ( operatorSymbol.equals("^") )
      {
         double doublea = a;
         double doubleb = b;
         finalNumber = (int) Math.pow( doubleb, doublea );
         // finalNumber = (int) doubleFinalNumber;
      }
      if ( operatorSymbol.equals("!") )
      {
         finalNumber = factorial( a );
      }

      return finalNumber;
   }
   
   public static int factorial( int a )
   {
      int result = 1;
      for ( int i = a; i > 1; i-- )
      {
         result *= i;
      }
      
      return result;
   }
   
   public static boolean isOperator(String currentSymbol )
   {
      String operators = "*+-%^!/";
      for ( int i = 0; i < operators.length(); i++ )
      {
         if ( currentSymbol.equals( operators.substring( i, i + 1 ) ) )
         {
            return true;
         }
      }
      return false;
   }
}

/**********************************************
Postfix  -->  Evaluate
 3 4 5 * +		23
 3 4 * 5 +		17
 10 20 + -6 6 * +		-6
 3 4 + 5 6 + *		77
 3 4 5 + * 2 - 5 /		5
 8 1 2 * + 9 3 / -		7
 2 3 ^		8
 20 3 %		2
 21 3 %		0
 22 3 %		1
 23 3 %		2
 5 !		120
 1 1 1 1 1 + + + + !		120
 
 
 *************************************/