// Name: Shaw Young Date: 4/5/19

import java.util.*;

public class McRonald
{
   public static final double CHANCEREMOVE = 1/7;
   public static final double CHANCEADD = 0.2;
   public static final int TIME = 1080;            // 18 hrs * 60 min
   public static int numberCustomers = 0;
   public static int customerWaitTime = 0;
   public static int averageWaitTime = 0;
   public static int longestWaitTime = 0;
   public static int queueLength = 0;
   public static int longestQueue = 0;
   public static Queue<Customer> queue;

      
   public static void main(String[] args) {
      queue = new LinkedList<Customer>(); 
      doSimulation();
   }
   
   public static void display(Queue<Customer> queue, int time )  {
      System.out.print( time + ": " );
      System.out.println( queue.toString() );
   }

   public static void doSimulation() {  
      for ( int i = 0; i < TIME; i++ ) {
         addCustomer( i );
         deleteCustomer( i );
         display( queue, i );
      }
      
      drainQueue( TIME );
      
      System.out.println( "Total numberCustomers served = " + numberCustomers );
      System.out.println( "Average wait time = " + ( (double) averageWaitTime / (double) numberCustomers ) );
      System.out.println( "Longest wait time = " + longestWaitTime );
      System.out.println( "Longest queue = " + longestQueue );
   }
   
   public static void addCustomer( int time ) {
      if ( Math.random() <= CHANCEADD ) {
         queue.add( new Customer( time ) );
         numberCustomers++;
         queueLength++;
         if ( queueLength > longestQueue ) {
            longestQueue = queueLength;
         }
      }
   }

   public static void deleteCustomer( int time ) {
      if ( queue.isEmpty() ) {
         return;
      }

      Customer customer = queue.peek();
      int currentWait = customer.getWait( time );
      if ( currentWait > longestWaitTime ) {
         longestWaitTime = currentWait;
      }

      customerWaitTime++;
      if ( customerWaitTime == 6 ) {         
         queue.remove();
         queueLength--;
         averageWaitTime += currentWait;
         customerWaitTime = 0;
      }
      else if ( Math.random() <= CHANCEREMOVE ) {
         queue.remove();
         queueLength--;
         averageWaitTime += currentWait;
         customerWaitTime = 0;
      }

      return;
   }
   
   public static void drainQueue( int time ) {
      while ( queueLength > 0 ) {
         deleteCustomer( time );
         display( queue, time );
         time++;
      }
   }
   
}

class Customer    {
   private int arrival;
      
   public Customer( int time ) {
      arrival = time;
   }
   
   public int getWait( int time ) {
      return time - arrival;
   }
   
   public int getArrival() {
      return arrival;
   }
   
   public String toString () {
      return Integer.toString( arrival );
   }
   
 }


