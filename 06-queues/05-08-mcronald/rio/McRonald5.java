// Name: Rio Young
// Date: 1/23/19

import java.util.*;

public class McRonald5
{
   public static final int TIME = 1080;  //18 hrs * 60 min
   //public static final int TIME = 10;
   public static final double ADDPROBABILITY = 0.2;
   public static final double ADDVIPPROBABILITY = 0.01;
   public static final double REMOVEPROBABILITY = 1.0/7.0;
   public static final int LEFTCOLUMNWIDTH = 20;
   public static Queue<Customer> queue;
   public static Queue<Customer> vipQueue;
   public static int vipWaited = 0;
   public static int vipCustomers = 0;
   public static int customers = 0;
   public static int waited = 0;
   public static int longestWait = 0;
   public static int longestQueue = 0;
   public static int currentWait = 0;
   private timeConverter converter;
      
   public void main(String[] args)
   {
      queue = new LinkedList<Customer>(); 
      vipQueue = new LinkedList<Customer>();
      converter = new timeConverter();
      simulate();
   }
   
   public static void simulate()
   {  
      for ( int i = 0; i < TIME; i++ )
      {
         addCustomer( i );
         removeCustomers( i );
         display( vipQueue, queue, i );
      }
      
      remainingCustomers( TIME );
      
      System.out.println( "Total customers served = " + customers );
      System.out.println( "Average wait time = " + ( (double) waited / (double) customers ) );
      System.out.println( "Longest wait time = " + longestWait );
      System.out.println( "Longest queue = " + longestQueue );
      System.out.println( "Total VIP customers served = " + vipCustomers );
      System.out.println( "Average VIP wait time = " + ( (double) vipWaited / (double) vipCustomers ) );  
   }
   
   public static void remainingCustomers( int time )
   {
      while ( queue.size() > 0 )
      {
         removeCustomers( time );
         display( vipQueue, queue, time );
         time++;
      }
   }
   
   public static void addCustomer( int time )
   {
      //System.out.println( "ADD CUSTOMER" );
      double random = Math.random();
      //System.out.println( "random: " + random );
      if ( Math.random() <= ADDVIPPROBABILITY )
      {
         vipQueue.add( new Customer( time ) );
         vipCustomers++;
     }
      else if ( Math.random() <= ADDPROBABILITY )
      {
         queue.add( new Customer( time ) );
         customers++;
         if ( queue.size() > longestQueue )
         {
            longestQueue = queue.size();
         }
      }
   }

   public static void removeCustomers( int time )
   {
      //System.out.println( "REMOVE CUSTOMERS" );
      
      if ( queue.size() == 0 && vipQueue.size() == 0 )
      {
        return;
      }
      
      if ( vipQueue.size() != 0 )
      {
        Customer customer = vipQueue.peek();
        currentWait++;
        int customerWait = customer.getWait( time );
        double random = Math.random();
        if ( ( currentWait > 1 && Math.random() <= REMOVEPROBABILITY )
            || currentWait > 6 )
        {
           vipQueue.remove();
           //System.out.println( "removeCustomers    DELETING CUSTOMER: " + customer );
           vipWaited += customerWait;
           currentWait = 0;
           return;
        }        
      }
      else
      {
        Customer customer = queue.peek();
        currentWait++;
        int customerWait = customer.getWait( time );
        //System.out.println( "removeCustomers    customerWait: " + customerWait );
        //int wait = customer.getWait( time );
        if ( customerWait > longestWait )
        {
           longestWait = customerWait;
        }
        //System.out.println( "removeCustomers    customer.getArrival(): " + customer.getArrival() + ", wait: " + wait );
        double random = Math.random();
        if ( ( currentWait > 1 && Math.random() <= REMOVEPROBABILITY )
            || currentWait > 6 )
        {
           queue.remove();
           //System.out.println( "removeCustomers    DELETING CUSTOMER: " + customer );
           waited += customerWait;
           currentWait = 0;
           return;
        }
      }
   }
   
   public static void display( Queue<Customer> vipQueue, Queue<Customer> queue, int time ) 
   {
      String leftColumn = vipQueue.toString();
      for ( int i = LEFTCOLUMNWIDTH; i > leftColumn.length(); i-- )
      {
        leftColumn += " ";
      }
      System.out.print( converter.convert( time ) + ": " );
      System.out.print( leftColumn );
      System.out.println( queue.toString() );
      
   }
}

 class Customer   
 {
   private int arrival;
   private timeConverter converter;
      
   public Customer( int time )
   {
      arrival = time;
      converter = new timeConverter();
   }
   
   public int getArrival()
   {
      return arrival;
   }
   
   public int getWait( int time )
   {
      //System.out.println( "Customer.getWait    arrival: " + arrival );
      return time - arrival;
   }
   
   public String toString ()
   {  
      return converter.convert( arrival );
   }
 }
 
 class timeConverter
 {
   public String convert( int time )
   {
     String hours = padString( time / 60 );
     String minutes = padString( time % 60 );
      
      return hours + ":" + minutes;
   }
   
   public String padString( int time )
   {
      String padded = Integer.toString( time );
      if ( padded.length() == 1 )
      {
        padded = "0" + padded;
      }
      
      return padded;
   }
   
   
 }