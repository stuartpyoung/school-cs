 // name: Shaw Young     date: 10-29-18
import java.util.*;
import java.io.*;
public class SelectionSort_Driver
{
   public static void main(String[] args) throws Exception
   {
     //Part 1, for doubles
//       double[] sloppy = new double[10];
//        
//       for ( int x = 9; x > -1; x-- ) {
//          sloppy[sloppy.length - x - 1] = x;
//          System.out.println("sloppy[" + x + "]: " + sloppy[sloppy.length - x - 1 ]);
//       }
//       
//       //System.out.println(Selection.findMax(sloppy, 1));
//       Selection.sort(sloppy);
//       System.out.println("\n");
//       //Selection.swap(sloppy, 0, 1);
//       for ( int q = 0; q < 10; q++ ) {
//          System.out.println("sloppy[" + q + "] " + q + ": " + sloppy[q]);
//       }
//       
//    }
      // 

      double[] array = {3,1,4,1,5,9,2,6};    // small example array from the MergeSort handout

      // int n = (int)(Math.random()*100)+20;
      // double[] array = new double[n];
      // for(int k = 0; k < array.length; k++)
      //    array[k] = Math.random()*100;	
      
      // Selection.sort(array);
      // print(array);
      // if( isAscending(array) )
      //    System.out.println("In order!");
      // else
      //    System.out.println("Out of order  :-( ");
      // System.out.println();
      
      // //Part 2, for Strings
      // int size = 100;
      // Scanner sc = new Scanner(new File("declaration.txt"));
      // Comparable[] arrayStr = new String[size];
      // for(int k = 0; k < arrayStr.length; k++)
      //    arrayStr[k] = sc.next();	
   
      // Selection.sort(arrayStr);
      // print(arrayStr);
      // System.out.println();
      
      // if( isAscending(arrayStr) )
      //    System.out.println("In order!");
      // else
      //    System.out.println("Out of order  :-( ");
   }
   public static void print(double[] a)
   {
      for(double d: a)         //for-each
         System.out.print(d+" ");
      System.out.println();
   }
   public static void print(Object[] papaya)
   {
      for(Object abc : papaya)     //for-each
         System.out.print(abc+" ");
   }
   public static boolean isAscending(double[] a)
   {
      for ( int x = 0; x < (a.length - 1); x++ ) {
         if(a[x] > a[x + 1 ] ) {
            return false;
         }
      }
      return true;
   }
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static boolean isAscending(Comparable[] a)
   {
      for ( int x = 0; x < (a.length - 1); x++ ) {
         if(a[x].compareTo(a[x + 1 ]) > 0 ) {
            return false;
         }
      }
      return true;
   }
}
//*********************************************
//name: Shaw Young date: 10-29-18
class Selection
{
   public static void sort(double[] array)
   {
      int maxIndex; 
      for (int i= 0; i < (array.length - 1); i++) {
         maxIndex = findMax(array, i+1);
         swap(array, maxIndex, i);
      }
       
   }  
//    public static void {
//       for (int i = 0; i < (array.length - 1); i++) {
//    }
   
   private static int findMax(double[] array, int upper)//"upper" controls where the inner loop of the Selection Sort ends
   {
      int maxIndex = 0;
      for ( int x = 0; x < upper; x++ ) {
         if ( array[maxIndex] > array[x] ) {
            maxIndex = x;
         }
      }
      return maxIndex;   
   }
   private static void swap(double[] array, int a, int b)
   {
      double tempA = array[a];
      array[a] = array[b];
      array[b] = tempA;
   }   	
   
	/*******  for Comparables ********************/
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
    public static void sort(Comparable[] array)
   {
      int maxIndex; 
      for (int i = 0; i < (array.length - 1); i++) {
         maxIndex = findMax(array, i+1);
         swap(array, maxIndex, i);
      }
   }
   @SuppressWarnings("unchecked")
    public static int findMax(Comparable[] array, int upper)
   {
      int maxIndex = 0;
      for ( int x = 0; x < upper; x++ ) {
         if ( array[x].compareTo(array[maxIndex]) > 0 ) {
            maxIndex = x;
         }
      }
      return maxIndex;
   }
   public static void swap(Object[] array, int a, int b)
   {
      Object tempA = array[a];
      array[a] = array[b];
      array[b] = tempA;
   }
   
   @SuppressWarnings("unchecked")
   public static void swap(Comparable[] array, int a, int b)
   {
      Comparable tempA = array[a];
      array[a] = array[b];
      array[b] = tempA;
   }
}

