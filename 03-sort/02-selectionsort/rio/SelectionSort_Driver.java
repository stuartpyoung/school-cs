 // name: Rio Young     date: 10/25/18
import java.util.*;
import java.io.*;
public class SelectionSort_Driver
{
   public static void main(String[] args) throws Exception
   {
     //Part 1, for doubles
      int n = (int)(Math.random()*100)+20;
      double[] array = new double[n];
      for(int k = 0; k < array.length; k++)
         array[k] = Math.random()*100;	
      //print( array );
      Selection.sort(array);
      print( array );
      if( isAscending(array) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
      System.out.println();
      
      //Part 2, for Strings
      int size = 100;
      Scanner sc = new Scanner(new File("declaration.txt"));
      Comparable[] arrayStr = new String[size];
      for(int k = 0; k < arrayStr.length; k++)
         arrayStr[k] = sc.next();	
      
      Selection.sort(arrayStr);
      print(arrayStr);
      System.out.println();
      
      if( isAscending(arrayStr) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
   }
   
   public static void print(double[] a)
   {
      for ( double d: a )
      {
         System.out.print( d + " ") ;
      }
      System.out.println();
   }
   
   public static void print(Object[] papaya)
   {
      for(Object abc : papaya)     
      {
         System.out.print(abc+" ");
      }
   }
   
   public static boolean isAscending(double[] array )
   {
      for ( int i = 0; i < array.length - 2; i++ )
      {
         if ( array[ i ] > array[ i + 1 ] )
         {
            return false;
         }
      }
   
      return true;
   }
   
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static boolean isAscending(Comparable[] array )
   {
      for ( int i = 0; i < array.length - 2; i++ )
      {
         if ( array[ i ].compareTo( array[ i + 1 ] ) > 0 )
         {
            return false;
         }
      }
   
      return true;
   }
}

//*********************************************
 // name: Rio Young     date: 10/25/18

class Selection
{
   public static void sort(double[] array)
   {
      for ( int i = 0; i < array.length - 1; i++ )
      {
         int max = findMax( array, array.length - i );
         //System.out.println( "sort    max: " + max );
         if ( ( array.length - i ) != max )
         {
            swap (array, max, array.length - i - 1 );
         }
      }
   }
   
   //"upper" controls where the inner, loop of the Selection Sort ends
   private static int findMax(double[] array, int upper)
   {
      //System.out.println( "sort    upper: " + upper );
      int max = 0;
      double temp = 0;
      for ( int i = 0; i < upper; i++ )
      {
         if ( temp < array[ i ] )
         {
            temp = array[ i ];
            max = i;
         }
      }
      
      return max;
   }
   
   private static void swap( double[] array, int from, int to )
   {
      double temp = array[ from ];
      array[ from ] = array[ to ];
      array[ to ] = temp;
   }   	
   
	/*******  for Comparables ********************/
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
    public static void sort(Comparable[] array)
   {
      for ( int i = 0; i < array.length - 1; i++ )
      {
         int max = findMax( array, array.length - i );
         //System.out.println( "sort    max: " + max );
         if ( ( array.length - i ) != max )
         {
            swap (array, max, array.length - i - 1 );
         }
      }
   }

   @SuppressWarnings("unchecked")
    public static int findMax(Comparable[] array, int upper)
   {
      //System.out.println( "sort    upper: " + upper );
      int max = 0;
      Comparable temp = array[ 0 ];
      for ( int i = 0; i < upper; i++ )
      {
         if ( temp.compareTo( array[ i ] ) < 0 )
         {
            temp = array[ i ];
            max = i;
         }
      }
      
      return max;
   }

   public static void swap(Object[] array, int from, int to )
   {
      Object temp = array[ from ];
      array[ from ] = array[ to ];
      array[ to ] = temp;
   }
}

