 // name: Rio Young     date: 10/25/18
import java.util.*;
import java.io.*;
public class InsertionSort_Driver
{
   public static void main(String[] args) throws Exception
   {
     //Part 1, for doubles
      int n = (int)(Math.random()*100)+20;
      double[] array = new double[n];
      for(int k = 0; k < array.length; k++)
         array[k] = Math.random()*100;	
      
      
      print( array );
      Insertion.sort( array );
      print( array );
      
      if( isAscending(array) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
      System.out.println();
      
      //Part 2, for Strings
      int size = 100;
      Scanner sc = new Scanner(new File("declaration.txt"));
      Comparable[] arrayStr = new String[size];
      for(int k = 0; k < arrayStr.length; k++)
         arrayStr[k] = sc.next();	
      
      Insertion.sort(arrayStr);
      print(arrayStr);
      System.out.println();
      
      if( isAscending( arrayStr ) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
   }
   
   public static void print(double[] a)
   {
      for(double d: a)
      {
         System.out.print( d + " " );
      }
      System.out.println();
   }
   
   public static void print(Object[] papaya)
   {
      for(Object abc : papaya)     //for-each
         System.out.print(abc+" ");
   }
   
   public static boolean isAscending(double[] a )
   {
      for ( int x = 0; x < (a.length - 1); x++ ) {
         if(a[x] > a[x + 1 ] ) {
            return false;
         }
      }
      return true;
   }
   
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static boolean isAscending(Comparable[] a )
   {
      for ( int x = 0; x < (a.length - 1); x++ ) {
         if(a[x].compareTo(a[x + 1 ]) > 0 ) {
            return false;
         }
      }
      return true;
   }
}

//**********************************************************
 // name: Rio Young     date: 10/25/18
class Insertion
{
   public static double[] sort(double[] array)
   {
      for ( int x = 1; x < array.length; x++ )  {
         shift( array, x, array[x] );
      }
      
      return array;
   }
   
   private static int shift(double[] array, int index, double value)
   {
      double tempValue = array[ index ];

      for ( int x = index - 1; x >= 0; x-- ) {
         if ( tempValue <= array[ x ]  ) {
            array[ x + 1 ] = array[ x ];
         }
         else {
            array[ x + 1 ] = tempValue;
            return x + 1;
         }
      }
      array[ 0 ] = tempValue;
      
      return -999;
   }
   
   @SuppressWarnings("unchecked")
   public static Comparable[] sort(Comparable[] array)
   {
      for ( int x = 1; x < array.length; x++ ) {
         shift( array, x, array[x] );
      }
      
      return array;
   }
   
   @SuppressWarnings("unchecked")
   private static int shift(Comparable[] array, int index, Comparable value)
   {
      Comparable tempValue = array[ index ];
      for ( int x = index - 1; x >= 0; x-- ) {
         if ( array[ x ].compareTo( tempValue ) >= 0 ) {
            array[ x + 1 ] = array[ x ];
         }
         else{
            array[ x + 1 ] = tempValue;
            return x + 1;
         }
      }
      array[ 0 ] = tempValue;
      
      return -1;
   }
}
