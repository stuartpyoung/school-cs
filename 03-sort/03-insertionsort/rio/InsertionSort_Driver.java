 // name: Rio Young     date: 10/25/18
import java.util.*;
import java.io.*;
public class InsertionSort_Driver
{
   public static void main(String[] args) throws Exception
   {
     //Part 1, for doubles
      int n = (int)(Math.random()*100)+20;
      double[] array = new double[n];
      for(int k = 0; k < array.length; k++)
         array[k] = Math.random()*100;	
      
      
      print( array );
      Insertion.sort( array );
      print( array );
      
      if( isAscending(array) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
      System.out.println();
      
      //Part 2, for Strings
      int size = 100;
      Scanner sc = new Scanner(new File("declaration.txt"));
      Comparable[] arrayStr = new String[size];
      for(int k = 0; k < arrayStr.length; k++)
         arrayStr[k] = sc.next();	
      
      Insertion.sort(arrayStr);
      print(arrayStr);
      System.out.println();
      
      if( isAscending( arrayStr ) )
         System.out.println("In order!");
      else
         System.out.println("Out of order  :-( ");
   }
   
   public static void print(double[] a)
   {
      for(double d: a)
      {
         System.out.print( d + " " );
      }
      System.out.println();
   }
   
   public static void print(Object[] papaya)
   {
      for(Object abc : papaya)     //for-each
         System.out.print(abc+" ");
   }
   
   public static boolean isAscending(double[] array )
   {
      for ( int i = 0; i < array.length - 2; i++ )
      {
         if ( array[ i ] > array[ i + 1 ] )
         {
            return false;
         }
      }
   
      return true;
   }
   
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static boolean isAscending(Comparable[] array )
   {
      for ( int i = 0; i < array.length - 2; i++ )
      {
         if ( array[ i ].compareTo( array[ i + 1 ] ) > 0 )
         {
            return false;
         }
      }
   
      return true;
   }
}

//**********************************************************
 // name: Rio Young     date: 10/25/18
class Insertion
{
   public static double[] sort(double[] array)
   {
      for ( int i = 1; i < array.length; i++ )
      {
         //System.out.println( "sort    SHIFTING position " + i );
         shift( array, i, array[ i ] );
      }
      
      return array;
   }
   
   private static int shift(double[] array, int index, double value)
   {
      double temp = array[ index ];
      for ( int i = index - 1; i >= 0; i-- )
      {
         if ( array[ i ] < temp )
         {
            array[ i + 1 ] = temp;
            //System.out.println( "shift    SETTING array[ " + ( i + 1 ) + " ] = " + temp );
            return i + 1;
         }
         else
         {
            array[ i + 1 ] = array[ i ];
            //System.out.println( "shift    MOVING array[ " + i + " ] to array[ " + ( i + 1 ) + " ]");
         }
      }
      array[ 0 ] = temp;
      
      return -1;
   }
   
   @SuppressWarnings("unchecked")
   public static Comparable[] sort(Comparable[] array)
   {
      for ( int i = 1; i < array.length; i++ )
      {
         //System.out.println( "sort    SHIFTING position " + i );
         shift( array, i, array[ i ] );
      }
      
      return array;
   }
   
   @SuppressWarnings("unchecked")
   private static int shift(Comparable[] array, int index, Comparable value)
   {
      Comparable temp = array[ index ];
      for ( int i = index - 1; i >= 0; i-- )
      {
         if ( array[ i ].compareTo( temp ) < 0 )
         {
            array[ i + 1 ] = temp;
            //System.out.println( "shift    SETTING array[ " + ( i + 1 ) + " ] = " + temp );
            return i + 1;
         }
         else
         {
            array[ i + 1 ] = array[ i ];
            //System.out.println( "shift    MOVING array[ " + i + " ] to array[ " + ( i + 1 ) + " ]");
         }
      }
      array[ 0 ] = temp;
      
      return -1;
   }
}
