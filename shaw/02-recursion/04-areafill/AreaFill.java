// Name: Shaw Young
// Date: 10-3-18

import java.util.*;
import java.io.*;

public class AreaFill
{
   private static char[][] grid = null;
   private static String filename = null;
   private int counter = 0;
   public static void main(String[] args) 
   {
      Scanner sc = new Scanner(System.in);
      while(true)  // what does this do?
      {
         System.out.print("Fill the Area of (-1 to exit): ");
         filename = sc.next();
         
         if(filename.equals("-1"))
         {
            sc.close();
            System.out.println("Good-bye");
            System.exit(0);   
         }
         grid = read(filename);
         String theGrid = display(grid);
         System.out.println( theGrid );
         System.out.print( "1-Fill or 2-Fill-and-Count: ");
         
         int choice = sc.nextInt();
         
         switch(choice)
         {
            case 1:
               {
                  System.out.print("\nEnter ROW COL to fill from: ");
                  int row = sc.nextInt();
                  int col = sc.nextInt(); 
                  fill(grid, row, col, grid[row][col]);
                  System.out.println( display(grid) );
                  break;
               }
            case 2:
               {
                  System.out.print("\nEnter ROW COL to fill from: ");
                  int row = sc.nextInt();
                  int col = sc.nextInt();
                  int count = fillAndCount(grid, row, col, grid[row][col]);
                  System.out.println( display(grid) );
                  System.out.println("count = " + count);
                  System.out.println();
                  break;
               }
            default:
               System.out.print("\nTry again! ");
         }
      }
   }
   
   /**
    * Reads the contents of the file into a matrix.
    * Uses try-catch.
    * @param filename The string representing the filename.
    * @returns A 2D array of chars.
    */
   public static char[][] read(String filename)
   {
      Scanner infile = null;
      try
      {
         infile = new Scanner(new File(filename));
      }
      catch (Exception e)
      {
         System.out.println("File not found");
         return null;
      }
      
      int rowLength = infile.nextInt();
      int columnLength = infile.nextInt();
	  // SKIP PAST END OF LINE WITH INTEGERS
  	  
      grid = new char[rowLength][columnLength];
      
      for ( int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
    	  string = infile.nextLine();
    	  //System.out.println( "string: " + string );
    	  String[] stringArray = string.split("");
    	  //System.out.print( "stringArray.length: " + stringArray.length );
      	  
    	  //System.out.print( "stringArray: " );
//    	  for ( int x = 0; x < stringArray.length; x++ ) {
//    		  System.out.print( stringArray[x] + " ");
//    	  }
//    	  //System.out.println();
    	  
    	  for ( int columnIndex = 0; columnIndex < columnLength; columnIndex++ ) {
    		  grid[ rowIndex ][ columnIndex ] = stringArray[columnIndex].charAt(0);
    	  }
      }
      
      return grid; 
   }
   
   /**
    * @param g A 2-D array of chars.
    * @returns A string representing the 2D array.
    */
   public static String display(char[][] g)
   {
      String output = "";
      for ( int rowIndex = 0; rowIndex < grid.length; rowIndex++) {
    	  for ( int columnIndex = 0; columnIndex < grid[0].length; columnIndex++ ) {
    		  output += grid[ rowIndex ][ columnIndex ];
    	  }
    	  output += "\n";
      }
      
	  return output;
   }
   
   /**
    * Fills part of the matrix with a different character.
    * @param g A 2D char array.
    * @param r An int representing the row of the cell to be filled.
    * @param c An int representing the column of the cell to be filled.
    * @param ch A char representing the replacement character.
    */
   public static void fill( char[][] inputGrid, int rowCounter, int columnCounter, char character )
   {
	   //System.out.println( "Fill     " + rowCounter + " " + columnCounter );

	   if ( rowCounter >= 0
			   && rowCounter < inputGrid.length
			   && columnCounter >= 0
			   && columnCounter < inputGrid[0].length
			   && inputGrid[ rowCounter ][ columnCounter ] == character 
			   ) {
		   
		   inputGrid[ rowCounter ][ columnCounter ] = '*';
	
		   fill( inputGrid, rowCounter - 1, columnCounter, character );
		   fill( inputGrid, rowCounter + 1, columnCounter, character );
		   fill( inputGrid, rowCounter, columnCounter - 1, character );
		   fill( inputGrid, rowCounter, columnCounter + 1, character );
		   
	   }
	   
	   return;
   }
   
   /**
    * Fills part of the matrix with a different character.
    * Counts as you go.  Does not use a static variable.
    * @param g A 2D char array.
    * @param r An int representing the row of the cell to be filled.
    * @param c An int representing the column of the cell to be filled.
    * @param ch A char representing the replacement character.
    * @return An int representing the number of characters that were replaced.
    */
   public static int fillAndCount( char[][] inputGrid, int rowCounter, int columnCounter, char character )
   {
	 
	   if ( rowCounter < 0
			   || rowCounter >= inputGrid.length
			   || columnCounter < 0
			   || columnCounter >= inputGrid[0].length
			   || inputGrid[ rowCounter ][ columnCounter ] != character 
			   )	{
		  return 0;
	  }
	  else	{
		   
		   inputGrid[ rowCounter ][ columnCounter ] = '*';
		   return 1 + fillAndCount( inputGrid, rowCounter - 1, columnCounter, character ) +
		   fillAndCount( inputGrid, rowCounter + 1, columnCounter, character ) +
		   fillAndCount( inputGrid, rowCounter, columnCounter - 1, character ) +
		   fillAndCount( inputGrid, rowCounter, columnCounter + 1, character );		   
	   }
   }
}