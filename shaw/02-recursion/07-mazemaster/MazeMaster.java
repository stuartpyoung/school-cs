//name: Shaw Young    date: 10-15-18
import java.util.*;
import java.io.*;
public class MazeMaster
{
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("Enter the maze's filename (no .txt): ");
      Maze m = new Maze(sc.next());
      // Maze m = new Maze();     //extension:  generate a random maze
      m.display();      //display maze
      System.out.println("Options: ");
      System.out.println("1: Mark all paths.");
      System.out.println("2: Mark all paths, and display the count of all STEPs.");
      System.out.println("3: Mark only the correct path.");
      System.out.println("4: Mark only the correct path. If no path exists, say so.");
      System.out.println("5: Mark only the correct path and display the count of STEPs.\n\tIf no path exists, say so.");
      System.out.print("Please make a selection: ");
      m.solve(sc.nextInt());
      m.display();      //display solved maze
   
   } 
}


class Maze
{
   //Constants
   private final char WALL = 'W';
   private final char DOT = '.';
   private final char START = 'S';
   private final char EXIT = 'E';
   private final char STEP = '*';
   //fields
   private char[][] maze;
   private int startRow, startCol;
  
  //constructors
   public Maze()  //extension:  generate a random maze
   {
   }
   public Maze(char[][] m)  //copy constructor
   {
      maze = m;
      for(int r = 0; r < maze.length; r++)
      {
         for(int c = 0; c < maze[0].length; c++)
         { 
            if(maze[r][c] == START)      //identify start
            {
               startRow = r;
               startCol = c;
            }
         }
      }
   } 
   public Maze(String filename)    
   {
       
	   Scanner infile = null;
	   try {
		   infile = new Scanner(new File(filename));
	   }
	   catch (Exception e) {
		   System.out.println("File not found");
		   
	   }
	   int row = infile.nextInt();
	   int column = infile.nextInt();
	   maze = new char[row][column];
	   
	   for(int r = 0; r < maze.length; r++) {
		   String[] stringArray = infile.nextLine().split("");   
		   for ( int c = 0; c < maze[0].length; c++ ) {
			   maze[ r ][ c ] = stringArray[c].charAt(0);
		   }
	   }
	   
   }
   public char[][] getMaze()
   {
      return maze;
   }
   public void display()
   {
      if(maze==null) 
         return;
      for(int a = 0; a<maze.length; a++)
      {
         for(int b = 0; b<maze[0].length; b++)
         {
            System.out.print(maze[a][b]);
         }
         System.out.println("");
      }
      System.out.println("");
   }
   
   public void solve(int n)
   {
      switch(n)
      {
         case 1:
            {
               markAllPaths(startRow, startCol);
               break;
            }
         case 2:
            {
               int count = markAllPathsAndCountStars(startRow, startCol);
               System.out.println("Number of steps = " + count);
               break;
            }
         case 3:
            {
               markTheCorrectPath(startRow, startCol);
               break;
            }
         case 4:         //use mazeNoPath.txt 
            {
               if( !markTheCorrectPath(startRow, startCol) )
                  System.out.println("No path exists."); 
               break;
            }
         case 5:
            {
               if( !markCorrectPathAndCountSteps(startRow, startCol, 0) )
                  System.out.println("No path exists."); 
               break;
            }
         default:
            System.out.println("File not found");   
      }
   }
   
    /*  1  almost like AreaFill*/
   public void markAllPaths(int rowCounter, int columnCounter)
   {
	   if ( rowCounter >= 0 && rowCounter < getMaze().length && columnCounter >= 0 && columnCounter < getMaze()[0].length && getMaze()[ rowCounter ][ columnCounter ] == '.' ) {
         getMaze()[ rowCounter ][ columnCounter ] = '*';
		
	      markAllPaths( rowCounter - 1, columnCounter );
	      markAllPaths( rowCounter + 1, columnCounter);
	      markAllPaths( rowCounter, columnCounter - 1);
	      markAllPaths( rowCounter, columnCounter + 1);
      }
   }
   
    /*  2  like AreaFill's counting without a static variable */  
   public int markAllPathsAndCountStars(int rowCounter, int columnCounter)
   {
      if ( rowCounter < 0 || rowCounter >= getMaze().length || columnCounter < 0 || columnCounter >= getMaze()[0].length || getMaze()[ rowCounter ][ columnCounter ] != '.' )	{
		  return 0;
	   }
	   else	{   
		   getMaze()[ rowCounter ][ columnCounter ] = '*';
		   return 1 + markAllPathsAndCountStars( rowCounter - 1, columnCounter ) +
		   markAllPathsAndCountStars( rowCounter + 1, columnCounter ) +
		   markAllPathsAndCountStars( rowCounter, columnCounter - 1 ) +
		   markAllPathsAndCountStars( rowCounter, columnCounter + 1 );		   
	   }
   }

   /*  3   recur until you find E, then mark the True path */
   public boolean markTheCorrectPath(int r, int c)
   {
      return false;
   }
   /*  4   Mark only the correct path. If no path exists, say so.
           Hint:  the method above returns the boolean that you need.  */
      

   /*  5  Mark only the correct path and display the count of STEPs.
          If no path exists, say so. */
   public boolean markCorrectPathAndCountSteps(int r, int c, int count)
   {
      return false;
   }
}
