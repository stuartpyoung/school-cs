// Name: Shaw Young
// Date: 9-26-18
  
import java.util.*;
public class Permutations
{
   public static int count = 0;
    
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("\nHow many digits? ");
      int n = sc.nextInt();
//      leftRight("", n);          
      oddDigits("", n);
//      superprime(n);
//      if(count==0)
//         System.out.println( "there are no " + n + "-digit superprimes." );
//      else
//          System.out.println("Count is "+count);
   }
   
    /**
     * Builds all the permutations of a string of length n containing Ls and Rs
     * @param inputString A string 
     * @param length An postive int representing the length of the string
     */
   public static void leftRight(String inputString, int length)
   {
	   if(length == 0) {
		   System.out.println( inputString );
	   }
	   else {
		   leftRight( inputString + "L", length - 1 );
		   leftRight( inputString + "R", length - 1 );
	   }
   }
   
    /**
     * Builds all the permutations of a string of length n containing odd digits
     * @param s A string 
     * @param n A postive int representing the length of the string
     */
   public static void oddDigits(String inputString, int length )
   {
	   if(length == 0) {
		   System.out.println( inputString );
	   }
	   else {
		   for ( int i = 1; i < 10 ; i+=2 ) {
			   oddDigits( inputString + Integer.toString( i ), length - 1 );
		   }
	   }
   }
   
      
    /**
     * Builds all combinations of a n-digit number whose value is a superprime
     * @param n A positive int representing the desired length of superprimes  
     */
   public static void superprime(int n)
   {
	  recur(2, n); //try leading 2, 3, 5, 7, i.e. all the single-digit primes
      recur(3, n); 
      recur(5, n);
      recur(7, n);
   }

    /**
     * Recursive helper method for superprime
     * @param possibleSuperPrime The possible superprime
     * @param length A positive int representing the desired length of superprimes
     */
   private static void recur(int possibleSuperPrime, int length)
   {
	   if ( length == 1 ) {
		   if ( isPrime( possibleSuperPrime ) == true ) {
			   System.out.println( possibleSuperPrime );
			   count++;
		   }
	   }
	   else {
		   for ( int i = 1; i < 10 ; i+=2 ) {
			   int currentDigit = Integer.parseInt( 
					   Integer.toString( possibleSuperPrime )
					   + Integer.toString( i ) );
			   if ( isPrime( currentDigit ) ) {
				   recur( currentDigit, length - 1 );   
			   }
			   
		   }
	   }
	   
   }
   
    /**
     * Determines if the parameter is a prime number.
     * @param n An int.
     * @return true if prime, false otherwise.
     */
   public static boolean isPrime(int n)
   {
	   if ( n == 2 ) {
		   return true;
	   }
	   
	   else if ( n % 2 == 0 ) {
		   return false;
	   }
		   
	   for ( int i = 3; i * i <= n; i += 2) {
		   if ( n % i == 0 ) {
			   return false;
		   }
	   }
	   
	   return true;
   }
}
