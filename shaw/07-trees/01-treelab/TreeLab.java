// Name: Shaw Young
// Date: 2-5-19

import java.util.*;

public class TreeLab
{
   public static TreeNode root = null;
   public static String s = "XCOMPUTERSCIENCE";
   //public static String s = "XThomasJeffersonHighSchool"; 
   //public static String s = "XAComputerScienceTreeHasItsRootAtTheTop";
   //public static String s = "XA";   //comment out lines 44-46 below
   //public static String s = "XAF";  //comment out lines 44-46 below
   //public static String s = "XAFP";  //comment out lines 44-46 below
   //public static String s = "XDFZM";  //comment out lines 44-46 below 
   
   public static void main(String[] args)
   {
      root = buildTree( root, s );
      System.out.print( display( root, 0) );
   
      System.out.print("\nPreorder: " + preorderTraverse(root));
      System.out.print("\nInorder: " + inorderTraverse(root));
      System.out.print("\nPostorder: " + postorderTraverse(root));
   
      System.out.println("\n\nNodes = " + countNodes(root));
      System.out.println("Leaves = " + countLeaves(root));
      System.out.println("Only children = " + countOnlys(root));
      System.out.println("Grandparents = " + countGrandParents(root));
   
      System.out.println("\nHeight of tree = " + height(root));
      System.out.println("Longest path = " + longestPath(root));
      System.out.println("Min = " + min(root));
      System.out.println("Max = " + max(root));	
   
      System.out.println("\nBy Level: ");
      System.out.println(displayLevelOrder(root));
   }
 
   public static TreeNode buildTree(TreeNode root, String s)
   {
      root = new TreeNode("" + s.charAt(1), null, null);
      for(int pos = 2; pos < s.length(); pos++)
         insert(root, "" + s.charAt(pos), pos, 
            (int)(1 + Math.log(pos) / Math.log(2)));
      insert(root, "A", 17, 5); 
      insert(root, "B", 18, 5); 
      insert(root, "C", 37, 6); //B's right child
      return root;
   }

   public static void insert(TreeNode t, String s, int pos, int level)
   {
      TreeNode p = t;
      for(int k = level - 2; k > 0; k--)
      {
         if((pos & (1 << k)) == 0)
            p = p.getLeft();
         else
            p = p.getRight();
      }
      if((pos & 1) == 0)
         p.setLeft(new TreeNode(s, null, null));
      else
         p.setRight(new TreeNode(s, null, null));
   }
   
   private static String display(TreeNode t, int level)
   {
      // turn your head towards left shoulder visualize tree
      String toRet = "";
      if(t == null)
         return "";
      toRet += display(t.getRight(), level + 1); //recurse right
      for(int k = 0; k < level; k++)
         toRet += "\t";
      toRet += t.getValue() + "\n";
      toRet += display(t.getLeft(), level + 1); //recurse left
      return toRet;
   }
   
   public static String preorderTraverse(TreeNode t)
   { 
      String toReturn = "";
      if(t == null)
         return "";
      toReturn += t.getValue() + " ";              //preorder visit
      toReturn += preorderTraverse(t.getLeft());   //recurse left
      toReturn += preorderTraverse(t.getRight());  //recurse right
      return toReturn;
   }
   
   public static String inorderTraverse(TreeNode t) {
      String toReturn = "";
      if(t == null)
         return "";
      toReturn += inorderTraverse(t.getLeft());
      toReturn += t.getValue() + " ";              
      toReturn += inorderTraverse(t.getRight());  
      return toReturn;   
   }      
   
   public static String postorderTraverse(TreeNode t)
   {
      String toReturn = "";
      if(t == null)
         return "";
      toReturn += inorderTraverse(t.getLeft());         
      toReturn += inorderTraverse(t.getRight());  
      toReturn += t.getValue() + " ";    
      return toReturn; 
   }
   
   public static int countNodes(TreeNode t)
   {
      if( t == null ) {
         return 0;
      }
      return countNodes(t.getLeft()) + 1 + countNodes(t.getRight()); 
      
   }
   
   public static int countLeaves(TreeNode t)
   {
      if ( t == null) {
         return 0;
      }
      if ( t.getLeft() == null && t.getRight() == null ) {
         return 1;
      }
      
      else {
         return countLeaves(t.getLeft()) + countLeaves(t.getRight());
      }
   }
   
   /*  there are clever ways and hard ways to count grandparents  */ 
   public static int countGrandParents(TreeNode t)
   {
      return -1;
   }
   
   public static int countOnlys(TreeNode t)
   {
      if ( t == null || ( t.getRight() == null && t.getLeft() == null ) ) {
         return 0;
      }
      if ( ( t.getLeft() == null && t.getRight() != null ) || ( t.getLeft() != null && t.getRight() == null ) ) {
         return 1 + countOnlys(t.getLeft()) + countOnlys(t.getRight());
      }
      
      else {
         return countOnlys(t.getLeft()) + countOnlys(t.getRight());
      }
   }
   
   /* Returns the max of the heights on both sides of the tree
	 */
   public static int height(TreeNode t)
   {
      
      if ( t == null ) {
         return 0;
      }
      if ( t.getLeft() == null && t.getRight() == null ) {
         return 0;
      }
      else {
         return Math.max( height( t.getLeft() ) , height( t.getRight() ) ) + 1;
      }

   }
   
   /* Returns the max of sum of heights on both sides of tree
	 */ 
    //treat each one as a root, and find the longest path each of those ig  
   public static int longestPath(TreeNode t)
   {
      int output = height(t.getRight()) + height(t.getLeft()) + 2;
      return output;
      //find the heights of the left and right branches and add two
      
   }
   
   //start of min code
   @SuppressWarnings("unchecked")
   public static Object min(TreeNode t)
   {
      Object currentMin = t.getValue();
     return minPt2(t, currentMin);
   }
   
   public static Object minPt2(TreeNode t, Object currentMin) {
      if ( t.getLeft() == null && t.getRight() == null ) {
         return returnMin(t.getValue(), currentMin);
      }
      else {
         if ( t.getLeft() == null && t.getRight() != null ) { //if theres only right 
            return minPt2( t.getRight(), returnMin(t.getRight().getValue(), currentMin) ); 
         }
         if ( t.getLeft() != null && t.getRight() == null ) { //if theres only left 
            return minPt2( t.getLeft(), returnMin(t.getLeft().getValue(), currentMin) );
         }
         else {//bowfa 
            return returnMin(minPt2(t.getLeft(), returnMin(t.getLeft().getValue(), currentMin) ),
             minPt2( t.getRight(), returnMin(t.getRight().getValue(), currentMin) ) );
         }
      }
   }
   
   @SuppressWarnings("unchecked")
   public static Object returnMin(Object a, Object b) { //finds min between two values, returns lower one
     
      
      if ( ((Comparable)a).compareTo( (Comparable)b ) < 0 ) {
         return a;
      } 
      return b;
   }
   //end of min code
   
   //start of max code
   @SuppressWarnings("unchecked")
   public static Object max(TreeNode t)
   {
      Object currentMax = t.getValue();
     return maxPt2(t, currentMax);
   }
   
   @SuppressWarnings("unchecked")
   public static Object maxPt2(TreeNode t, Object currentMax) {
      if ( t.getLeft() == null && t.getRight() == null ) {
         return returnMax(t.getValue(), currentMax);
      }
      else {
         if ( t.getLeft() == null && t.getRight() != null ) { //if theres only right 
            return maxPt2( t.getRight(), returnMax(t.getRight().getValue(), currentMax) ); 
         }
         if ( t.getLeft() != null && t.getRight() == null ) { //if theres only left 
            return maxPt2( t.getLeft(), returnMax(t.getLeft().getValue(), currentMax) );
         }
         else {//bowfa 
            return returnMax(maxPt2(t.getLeft(), returnMax(t.getLeft().getValue(), currentMax) ),
             maxPt2( t.getRight(), returnMax(t.getRight().getValue(), currentMax) ) );
         }
      }
   }
   
   @SuppressWarnings("unchecked")   
   public static Object returnMax(Object a, Object b) { //finds max between two values, returns higher one
     
      
      if ( ((Comparable)a).compareTo( (Comparable)b ) > 0 ) {
         return a;
      } 
      return b;
   }
   //end of max code
      
   /* This method is not recursive.  Use a local queue
    * to store the children of the current TreeNode.
    */
   public static String displayLevelOrder(TreeNode t)
   {
      String output = "";
      Queue<TreeNode> temp;
      if ( t != null ) {
         temp.add(t);
         while ( !temp.isEmpty() ) {
            t = (TreeNode)temp.remove();
            output = t.getValue() + output;
            if ( t.getLeft() != null ) {
               temp.add( t.getLeft() );
            }
            if ( t.getRight() != null ) {
               temp.add( t.getRight() );   
            }
         }
      }
      return output;
   }
}

/***************************************************
    ----jGRASP exec: java TreeLab
 		  	E
 		E
 			C
 	M
 			N
 		T
 			E
 C
 			I
 		U
 			C
 	O
 			S
 					C
 				B
 		P
 				A
 			R
 
 Preorder: C O P R A S B C U C I M T E N E C E 
 Inorder: R A P B C S O C U I C E T N M C E E 
 Postorder: A R C B S P C I U O E N T C E E M C 
 
 Nodes = 18
 Leaves = 8
 Only children = 3
 Grandparents = 5
 
 Height of tree = 5
 Longest path = 8
 Min = A
 Max = U
 
 By Level: 
 COMPUTERSCIENCEABC    
 *******************************************************/
