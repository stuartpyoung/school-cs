//name: Shaw Young    
//date: 10-25-18
import java.io.*;      //the File 
import java.util.*;    //the Scanner 
public class Searches_Driver
{
   private static int amount=1325;
   public static void main(String[] args) throws Exception
   {
      String[] apple = input("declaration.txt");
      Arrays.sort(apple);    
      Scanner sc = new Scanner(System.in);
      while(true)
      {
         System.out.print("Enter a word: ");
         String target = sc.next();   //Liberty  
         if(target.equals("-1") )
            break;
         Searches.reset();
         int found = Searches.linear(apple, target);
         if(found == -1)
            System.out.println(target + " was not found by the linear search.");
         else
            System.out.println("Linear Search found it at location "+found+" in " + Searches.getLinearCount()+" comparisons.");
         int found2 = Searches.binary(apple, target);
         if(found2 == -1)
            System.out.println(target + " was not found by the binary search.");
         else
            System.out.println("Binary Search found it at location "+found2+" in " + Searches.getBinaryCount()+" comparisons.");
      }
   }
   public static String[] input(String filename) throws Exception
   {
      Scanner infile = new Scanner( new File(filename) );
      String[] array = new String[amount];
      for (int k =0; k<amount; k++)    
         array[k] = infile.next();
      infile.close();
      return array;
   }
}
/////////////////////////////////////////////////////////
class Searches
{
   private static int linearCount = 0;
   private static int binaryCount = 0;      
   public static int getLinearCount()
   {
      return linearCount;
   }
   public static int getBinaryCount()
   {
      return binaryCount;
   }
   public static void reset()
   {
      linearCount = 0;
      binaryCount = 0;
   }
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static int linear(Comparable[] array, Comparable target)
   { 
      linearCount++;
      int a = 0; 
      for (int k = 0; k < array.length; k++)    {
         if(array[k].compareTo(target) == 0 ) {
            a = k; 
            break;  
         }
         linearCount++;
      }
      
      return a;
   }
   
//   @SuppressWarnings("unchecked")
//   public static int replace(String[] array, int index1, int index2) {
//	   String u = array[index1];
//	   array[index1] = array[index2];
//	   array[index2] = u;
//   }
   
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   public static int binary(Comparable[] array, Comparable target)
   {
      return binaryhelper(array, target, 0, array.length - 1);
   }
   
   @SuppressWarnings("unchecked")//this removes the warning for Comparable
   private static int binaryhelper(Comparable[] array, Comparable target, int start, int end)
   {
    
    // 
//     int binarySearch(int a[], int item, int low, int high) 
//     
//     if (high <= low) 
//        return (item > a[low])?  (low + 1): low; 
//   
//     int mid = (low + high)/2; 
//   
//     if(item == a[mid]) 
//         return mid+1; 
//   
//     if(item > a[mid]) 
//         return binarySearch(a, item, mid+1, high); 
//     return binarySearch(a, item, low, mid-1); 
//     
    
      int halfPoint = array.length / 2;
      if ( end <= start) {
      
         binaryCount++;  
         if ( array[ halfPoint ].compareTo( target ) == 0 ) {
            return halfPoint;
         }
         
         else if ( array[ halfPoint ].compareTo( target ) > 0 ) {
            return binaryhelper(array, target, start, halfPoint + 1);
         }
         
         
         // else if ( array[ halfPoint ].compareTo( target ) < 0 ) {
            return binaryhelper(array, target, halfPoint + 1, end);  
         
      }
      return -1;
         
   }
      
      	
   
}
