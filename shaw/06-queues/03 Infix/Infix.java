// Name: Shaw Young
// Date: 1-23-19

import java.util.*;

public class Infix
{
   public static void main(String[] args)
   {
      System.out.println("Infix  \t-->\tPostfix\t\t-->\tEvaluate");
     /*build your list of Infix expressions here  */
      ArrayList<String> infixExp = new ArrayList<String>();
      infixExp.add("3 + 4");
      infixExp.add("3 + 4 * 5");
      infixExp.add("3 * 4 + 5");
         
      for( String infix : infixExp )
      {
         String pf = infixToPostfix(infix);
         System.out.println(infix + "\t\t\t" + pf + "\t\t\t" + Postfix.eval(pf));  //Postfix must work!
      }
   }
   
   public static String infixToPostfix(String infix)
   {
      List<String> infixParts = new ArrayList<String>(Arrays.asList(infix.split(" ")));
      Stack<String> tempStorage = new Stack<String>();
      
      String result = "";
      //loop over input
      //if is number -- add to result
      //if is operator -- if stack is not empty pop 
      //the prev operator off the stack and add it 
      //to the result
      //push the current operator to the stack

      if ( infixParts.isEmpty() ) {
         return result;   
      }
      
      for ( String s : infixParts ) {
         if ( !isOperator( s ) ) { //if its a number just add it to the string
            result += s ;
            System.out.println("number ");
         }

         if ( isOperator ( s ) ) {  //otherwise (in the case that its a operator)
            System.out.println("operator");
            if ( tempStorage.isEmpty() )   {
               tempStorage.push(s);
            }
            if ( isLeftParentheses(tempStorage.peek()) ) {
               tempStorage.push(s);
            }
            if ( isLeftParentheses(s) ) {
               tempStorage.push(s);
            }
            
            if ( isRightParentheses(s) ) {
               while( !isLeftParentheses(tempStorage.peek()) ) {
                  result += tempStorage.pop();
               }
            }
            
            tempStorage.pop();           
            
            if ( !tempStorage.isEmpty() ) {
               
               if ( isLower( tempStorage.peek().charAt(0), s.charAt(0)  ) ) {
                  result += tempStorage.pop();   
                  tempStorage.add(s);
               }
               if ( isLower( s.charAt(0), tempStorage.peek().charAt(0) ) ) {
                  result += tempStorage.pop();   
                  tempStorage.add(s);
               }
               
            }
            
         }
         System.out.println("tempStorage so far:" + tempStorage);
         System.out.println("results so far:" + result);
      }
      
      if ( !tempStorage.isEmpty() ) {
         while ( !tempStorage.isEmpty() ) {
            result += tempStorage.pop();
         }
      }
      
     // System.out.println("final result : " + result);
      return result.replace("", " ").trim();
    
    
   }
   //conditions for 2c
   //if tempStorage is empty
   //if string on top of the stack is a left parentheses
   //if the operator at teh top of the stack is equal or high precedence than the current operator   
   public static boolean isLeftParentheses(String potentialParentheses)  {
      if(potentialParentheses == "(" || potentialParentheses == "[") {
         return true;
      } 
      else {
         return false;
      }
   }
   
   public static boolean isRightParentheses(String potentialParentheses)  {
      if(potentialParentheses == ")" || potentialParentheses == "]") {
         return true;
      } 
      else {
         return false;
      }
   }
   
   public static boolean isOperator(String potentialOperator)  {
      String operator = "+-*/";
      
      for(int i = 0; i < operator.length(); i++) {
         if(potentialOperator.equals(operator.substring(i, i+1))) {
            return true;
         }      
      }
      
      return false; 
   }
   
	//returns true if c1 has lower or equal precedence than c2
   public static boolean isLower(char c1, char c2)
   {
      if( ( c1 == '+' || c1 == '-' ) && ( c2 == '*' || c2 == '/' ) ) {
         return true; 
      }
      else if( ( c2 == '+' || c2 == '-' ) && ( c1 == '*' || c1 == '/' ) ) {
         return false; 
      }
      else {
         return false;
      }
      
   }
}
	
/********************************************

 Infix  	-->	Postfix		-->	Evaluate
 3 + 4 * 5			3 4 5 * +			23
 3 * 4 + 5			3 4 * 5 +			17
 ( -5 + 15 ) - 6 / 3			-5 15 + 6 3 / -			8
 ( 3 + 4 ) * ( 5 + 6 )			3 4 + 5 6 + *			77
 ( 3 * ( 4 + 5 ) - 2 ) / 5			3 4 5 + * 2 - 5 /			5
 8 + -1 * 2 - 9 / 3			8 -1 2 * + 9 3 / -			3
 3 * ( 4 * 5 + 6 )			3 4 5 * 6 + *			78
 
***********************************************/