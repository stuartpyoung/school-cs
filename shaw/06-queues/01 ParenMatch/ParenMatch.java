// Name: Shaw Young
// Date: 1/16/18

import java.util.*;

public class ParenMatch
{
   public static final String left  = "([{<";
   public static final String right = ")]}>";
   
   public static void main(String[] args)
   {
      System.out.println("Parentheses Match");
      ArrayList<String> parenExp = new ArrayList<String>();
      /* enter test cases here */
parenExp.add("5+7");
parenExp.add("(5+7)");	 
parenExp.add(")5+7(");	
parenExp.add("((5+7)*3)");	 
parenExp.add("<{5+7}*3>");	 
parenExp.add("[(5+7)*]3");	 
parenExp.add("(5+7)*3");	 
parenExp.add("5+(7*3)");	 
parenExp.add("((5+7)*3");	 
parenExp.add("[(5+7]*3)");	 
parenExp.add("[(5+7)*3])"); 
parenExp.add("([(5+7)*3]");
      for( String s : parenExp )
      {
         boolean good = checkParen(s);
         if(good)
            System.out.println(s + "\t good!");
         else
            System.out.println(s + "\t BAD");
      }
   }

   public static boolean checkParen(String exp)
   {
      System.out.println("");
      if(exp.equals("")) {
         return false;
      }
      
      Stack<String> parentheses = new Stack<String>(); //stack of parentheses
      String[] equation = exp.split(""); //string array of the whole thing
      int equationOriginalLength = equation.length;
      int index = 0;

      for ( String s : equation ) {
         if ( left.contains ( equation [ index ] ) ) {
            parentheses.push( equation [ index ] );
         }

         if ( right.contains ( equation [ index ] ) && parentheses.isEmpty()) {
            return false;   
         }
                            
         if ( right.contains ( equation [ index ] ) ) {
            break;
         }
         
         index++;
      }


      if ( parentheses.isEmpty() ) {
         return true;
      }
      int numberOfRights = 0;
      
      for ( int x = index; x < equationOriginalLength; x++ ) {
         if ( right.contains ( equation [ x ] ) )  {
            numberOfRights++;               
         }
      }
      
      if ( numberOfRights != parentheses.size() ) {
         return false;
      } 
         
      for ( int x = index; x < equationOriginalLength; x++ ) {
         if ( right.contains ( equation [ x ] ) )  {
            
            if ( right.indexOf ( equation [ x ] ) == left.indexOf ( parentheses.peek() ) ) {//check the index of both - the same = good peek
               parentheses.pop();  
            }
        //     if ( parentheses.isEmpty() ) {
//                return true;
//             }

         }
         
      }
      
      if ( parentheses.isEmpty() ) {
         return true;
      }
      else { 
         return false;
      }
      // System.out.println("Size: " + parentheses.size());
//       for ( int x = 0; x < parentheses.size(); x++ ) {
//          System.out.println(parentheses.pop());  
//       }
//       
 
      
   }
}

//       int index0 = 0;
//       for ( String i : equation ) {
//          if( !left.contains(equation[index0]) || !right.contains(equation[index0]) ) {       
//             onlyParantheses.add(equation[index0]);
//          }
//          index0++;
//       }

      
   //   
//       for ( String s : equation ) {
//         System.out.println(onlyParantheses.get(index));    
//          index++;
//       }
//    String[] equationArray = new String[bee.length]; 
//       
//       for(int k = 0; k < bee.length; k++) {
//          equationArray[k] = String.valueOf(bee[k]);
//       }


/*
 Parentheses Match
 5+7	 good!
 (5+7)	 good!
 )5+7(	 BAD
 ((5+7)*3)	 good!
 <{5+7}*3>	 good!
 [(5+7)*]3	 good!
 (5+7)*3	 good!
 5+(7*3)	 good!
 ((5+7)*3	 BAD
 [(5+7]*3)	 BAD
 [(5+7)*3])	 BAD
 ([(5+7)*3]	 BAD
 */
