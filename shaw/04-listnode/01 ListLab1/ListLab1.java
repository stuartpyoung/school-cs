// Name: Shaw Young
// Date: 11-8-18

import java.util.*;
public class ListLab1
{
   public static void main(String[] args)
   {
      ListNode head = new ListNode("hello", null);
      head = new ListNode("foo", head);
      head = new ListNode("boo", head);
      head = new ListNode("nonsense", head);
      head = new ListNode("computer",
         	new ListNode("science",
         		new ListNode("java",
         			new ListNode("coffee", head)
         		)
         	)
         );
      print(head);
      print(head);
      
     
      /**** uncomment the code below for ListLab1 Extension  ****/
      
   	System.out.println("First = " + first(head));
      System.out.println("Second = " + second(head));
      ListNode p = pointerToLast(head);
      System.out.println("Pointer to Last = " + p.getValue()+ " at " + p);
      ListNode c = copyOfLast(head);
      System.out.println("Copy of Last =    " + c.getValue()+ " at " + c);
   		
      Scanner in = new Scanner(System.in);
      System.out.print("Insert what? ");
      String x = in.next();
      head = insertFirst(head, x);
      head = insertLast(head, x);
      print(head);
   }
   public static void print(ListNode head)
   {
      System.out.print("[");
      while(head != null)
      {
         System.out.print(head.getValue());
         head = head.getNext();
         if(head != null)
            System.out.print(", ");
      }
      System.out.println("]");
   }
   public static Object first(ListNode head) { //if i dont work try it with String instead of Object
      ListNode french = head;
      return french.getValue();
   }
   
   public static Object second(ListNode head) {
      head = head.getNext();
      return head.getValue();
   }
   public static ListNode pointerToLast(ListNode head) {
      while(head.getNext().getNext() != null) {
         head = head.getNext();
      }
      return head.getNext();
   }
   public static ListNode copyOfLast(ListNode head){
      while(head.getNext().getNext() != null) {
         head = head.getNext();
      }
      return new ListNode(head.getNext().getValue(), null);
   }
   public static ListNode insertFirst(ListNode head, Object value) {
      ListNode newFirst = new ListNode(value, head);
      return newFirst;
   }
   public static ListNode insertLast(ListNode head, Object end) { 
      ListNode finalNode = pointerToLast(head);  
      finalNode.setNext( new ListNode( end, null ) );
      return head;
   } 
      
      
}

/*****************************************
 
 [computer, science, java, coffee, nonsense, boo, foo, hello]
 [computer, science, java, coffee, nonsense, boo, foo, hello]
 First = computer
 Second = science
 Pointer to Last = hello at ListNode@18767ad
 Copy of Last =    hello at ListNode@a7bdcd
 Insert what? abc
 [abc, computer, science, java, coffee, nonsense, boo, foo, hello, abc]
 

  **********************************************/
