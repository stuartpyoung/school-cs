// Name: Rio Young
// Date: 5/22/19
 
import java.util.*;
import java.io.*;

/* Resource classes and interfaces
 * for use with Graphs3: EdgeList,
 * Graphs4: DFS-BFS
 * and Graphs5: EdgeListCities
 */

/* Graphs 3: EdgeList 
 */
interface VertexInterface
{
   String toString(); // Don't use commas in the list.  Example: "C [C D]"
   String getName();
   ArrayList<Vertex> getAdjacencies();
   void addEdge(Vertex v);
} 

class Vertex implements VertexInterface 
{
   private final String name;
   private ArrayList<Vertex> adjacencies;
  
  /* enter your code here  */
   
   public Vertex( String newName )
   {
      name = newName;
      adjacencies = new ArrayList<Vertex>();
   }
   
   public String toString() // Don't use commas in the list.  Example: "C [C D]"
   {
      String output = name + " [";
      for ( int i = 0; i < adjacencies.size(); i++ )
      {
         output += adjacencies.get( i ).getName();
         if ( i != adjacencies.size() - 1 )
         {
            output += " ";
         }
      }
      output += "]";

      return output;
   }

   public String getName()
   {
      return name;
   }
   
   public ArrayList<Vertex> getAdjacencies()
   {
      return adjacencies;
   }
   
   public void addEdge( Vertex vertex )
   {
      adjacencies.add( vertex );
   }
   
}   

interface AdjListInterface 
{ 
   List<Vertex> getVertices();
   Vertex getVertex(int i) ;
   Vertex getVertex(String vertexName);
   Map<String, Integer> getVertexMap();
   void addVertex(String v);
   void addEdge(String source, String target);
   String toString();  //returns all vertices with their edges (omit commas)
}

  
/* Graphs 4: DFS and BFS 
 */
interface DFS_BFS
{
   List<Vertex> depthFirstSearch(String name);
   List<Vertex> depthFirstSearch(Vertex v);
   List<Vertex> breadthFirstSearch(String name);
   List<Vertex> breadthFirstSearch(Vertex v);
   //List<Vertex> depthFirstRecur(String name);
   //List<Vertex> depthFirstRecur(Vertex v);
   //void depthFirstRecurHelper(Vertex v, ArrayList<Vertex> reachable);
}

/* Graphs 5: Edgelist with Cities 
 */
interface EdgeListWithCities
{
   void graphFromEdgeListData(String fileName) throws FileNotFoundException;
   int edgeCount();
   boolean isReachable(String source, String target);
   boolean isFullyReachable();
}


public class AdjList implements DFS_BFS //EdgeListWithCities
{
   private ArrayList<Vertex> vertices = new ArrayList<Vertex>();
   private Map<String, Integer> nameToIndex = new TreeMap<String, Integer>();
  
 /* enter your code here  */
 
   public AdjList()
   {
      
   }
 
   public List<Vertex> getVertices()
   {
      return vertices;
   }
   
   public Vertex getVertex(int i)
   {
      return vertices.get( i );
   }
   
   public Vertex getVertex(String vertexName)
   {
      for ( int i = 0; i < vertices.size(); i++ )
      {
         if ( vertices.get( i ).getName().equals( vertexName ) )
         {
            return vertices.get( i );
         }
      }
      
      System.out.println( "something's wrong" );
      System.exit( 0 );
      return vertices.get( 0 );
   }
   
   public Map<String, Integer> getVertexMap()
   {
      for ( int i = 0; i < vertices.size(); i++ )
      {
         nameToIndex.put( vertices.get( i ).getName(), i );
      }
      
      return nameToIndex;
   }
   
   public void addVertex( String vertexName )
   {
      for ( int i = 0; i < vertices.size(); i++ )
      {
         if ( vertices.get( i ).getName().equals( vertexName ) )
         {
            break;
         }
      }
      
      vertices.add( new Vertex( vertexName ) );
   }
   
   public void addEdge(String source, String target)
   {
      for ( int i = 0; i < vertices.size(); i++ )
      {
         if ( vertices.get( i ).getName().equals( source ) )
         {
            vertices.get( i ).addEdge( new Vertex( target ) );
            return;
         }
      }
      
      addVertex( source );
      addEdge( source, target );
   }
   
   public List<Vertex> depthFirstSearch(String name)
   {
      //System.out.println( " depthFirstSearch    name: " + name );
      List<Vertex> tempVertices = new ArrayList<Vertex>();
      Stack<Vertex> stack = new Stack<Vertex>();
      Vertex vertex = getOriginalVertex( name );
      //System.out.println( "depthFirstSearch    vertex: " + vertex );
      stack.push( vertex );
      while ( ! stack.isEmpty() )
      {
         //System.out.println( "depthFirstSearch    stack: " + stack );
         Vertex tempVertex = stack.pop();
         Vertex originalVertex = getOriginalVertex( tempVertex.getName() );
         if ( ! containsVertex( tempVertices, originalVertex ) )
         {
            tempVertices.add( originalVertex );

            for ( Vertex adjacent : tempVertex.getAdjacencies() )
            {
               //System.out.println( "depthFirstSearch    adjacent: " + adjacent );
               Vertex tempOriginalVertex = getOriginalVertex( adjacent.getName() );
               //System.out.println( "depthFirstSearch    tempOriginalVertex: " + tempOriginalVertex );
               stack.push( tempOriginalVertex );            
            }

         }
 
      }
      
      return tempVertices;
   }
   
   public boolean containsVertex( List<Vertex> vertexList, Vertex vertex )
   {
      for ( int i = 0; i < vertexList.size(); i++ )
      {
         if ( vertexList.get( i ).getName().equals( vertex.getName() ) )
         {
           return true;
         }
      }
      
      return false;
   }
   
   public Vertex getOriginalVertex ( String name )
   {
      //System.out.println( "getOrginalVertex    name: " + name );
      Map vertexMap = getVertexMap();
      int index = nameToIndex.get( name );
      //System.out.println( "getOrginalVertex    index: " + index );
      
      return vertices.get( index );
   }
   
   public List<Vertex> depthFirstSearch( Vertex vertex )
   {
      List<Vertex> tempVertices = new ArrayList<Vertex>();
      Stack<Vertex> stack = new Stack<Vertex>();
      //System.out.println( "depthFirstSearch    vertex: " + vertex );
      stack.push( vertex );
      while ( ! stack.isEmpty() )
      {
         //System.out.println( "depthFirstSearch    stack: " + stack );
         Vertex tempVertex = stack.pop();
         Vertex originalVertex = getOriginalVertex( tempVertex.getName() );
         if ( ! containsVertex( tempVertices, originalVertex ) )
         {
            tempVertices.add( originalVertex );

            for ( Vertex adjacent : tempVertex.getAdjacencies() )
            {
               //System.out.println( "depthFirstSearch    adjacent: " + adjacent );
               Vertex tempOriginalVertex = getOriginalVertex( adjacent.getName() );
               //System.out.println( "depthFirstSearch    tempOriginalVertex: " + tempOriginalVertex );
               stack.push( tempOriginalVertex );            
            }

         }
 
      }
      
      return tempVertices;
   }
   
   public List<Vertex> breadthFirstSearch(String name)
   {
      List<Vertex> tempVertices = new ArrayList<Vertex>();
      Queue<Vertex> queue = new LinkedList<Vertex>();
      Vertex vertex = getOriginalVertex( name );
      //System.out.println( "breadthFirstSearch    vertex: " + vertex );
      queue.add( vertex );
      while ( ! queue.isEmpty() )
      {
         //System.out.println( "breadthFirstSearch    queue: " + queue );
         Vertex tempVertex = queue.remove();
         Vertex originalVertex = getOriginalVertex( tempVertex.getName() );
         if ( ! containsVertex( tempVertices, originalVertex ) )
         {
            tempVertices.add( originalVertex );

            for ( Vertex adjacent : tempVertex.getAdjacencies() )
            {
               //System.out.println( "breadthFirstSearch    adjacent: " + adjacent );
               Vertex tempOriginalVertex = getOriginalVertex( adjacent.getName() );
               //System.out.println( "breadthFirstSearch    tempOriginalVertex: " + tempOriginalVertex );
               queue.add( tempOriginalVertex );            
            }

         }
 
      }
      
      return tempVertices;
   }
   
   public List<Vertex> breadthFirstSearch( Vertex vertex )
   {
      List<Vertex> tempVertices = new ArrayList<Vertex>();
      Queue<Vertex> queue = new LinkedList<Vertex>();
      //System.out.println( "breadthFirstSearch    vertex: " + vertex );
      queue.add( vertex );
      while ( ! queue.isEmpty() )
      {
         //System.out.println( "breadthFirstSearch    queue: " + queue );
         Vertex tempVertex = queue.remove();
         Vertex originalVertex = getOriginalVertex( tempVertex.getName() );
         if ( ! containsVertex( tempVertices, originalVertex ) )
         {
            tempVertices.add( originalVertex );

            for ( Vertex adjacent : tempVertex.getAdjacencies() )
            {
               //System.out.println( "breadthFirstSearch    adjacent: " + adjacent );
               Vertex tempOriginalVertex = getOriginalVertex( adjacent.getName() );
               //System.out.println( "breadthFirstSearch    tempOriginalVertex: " + tempOriginalVertex );
               queue.add( tempOriginalVertex );            
            }

         }
 
      }
      
      return tempVertices;
   }
   
   public String toString()  //returns all vertices with their edges (omit commas)
   {
      String output = "";
      for ( int i = 0; i < vertices.size(); i++ )
      {
         output += vertices.get( i ).toString() + "\n";
      }
      return output;
   }
}


