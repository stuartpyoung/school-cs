// Name: Rio Young  
// Date: 4/30/19
 
import java.util.*;
import java.io.*;

/* Resource classes and interfaces
 * for use with Graph0 AdjMat_0_Driver,
 * Graph1 WarshallDriver,
 * and Graph2 FloydDriver
 */

interface AdjacencyMatrix
{
   void addEdge(int source, int target);
   void removeEdge(int source, int target);
   boolean isEdge(int from, int to);
   String toString();   
   int edgeCount();
   List<Integer> getNeighbors(int source);
   //public List<String> getReachables(String from);  //Warshall extension
}

interface Warshall      
{
   boolean isEdge(String from, String to);
   Map<String, Integer> getVertices();     
   void readNames(String fileName) throws FileNotFoundException;
   void readGrid(String fileName) throws FileNotFoundException;
   void displayVertices();
   void allPairsReachability();  // Warshall's Algorithm
}

interface Floyd
{
   int getCost(int from, int to);
   int getCost(String from, String to);
   void allPairsWeighted(); 
}

public class AdjMat implements AdjacencyMatrix 
{
   //adjacency matrix representation
   private int[][] grid = null;   
   
   // name-->index (for Warshall & Floyd)
   private Map<String, Integer> vertices = null;
   private int gridSize = 0;
     
   // constructor
   public AdjMat ( int size )
   {
      grid = new int[ size ][ size ];
      gridSize = size;
   }

   public void addEdge ( int source, int target )
   {
      if ( source > gridSize - 1 || target > gridSize - 1 )
      {
         return;
      }
   
      grid[ source ][ target ] = 1;   
   }
   
   public void removeEdge ( int source, int target )
   {
      if ( source > gridSize - 1 || target > gridSize - 1 )
      {
         return;
      }
   
      grid[ source ][ target ] = 0;
   }
   
   public boolean isEdge ( int source, int target )
   {
      if ( grid[ source ][ target ] == 1 )
      {
         return true;
      }
   
      return false;
   }

   public int edgeCount ( )
   {
      int count = 0;
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int k = 0; k < gridSize; k++ )
         {
            if ( grid[ i ][ k ] == 1 )
            {
               count++;
            }
         }
      }
   
      return count;
   }

   public List<Integer> getNeighbors ( int source )
   {
      List<Integer> neighbors = new ArrayList<Integer>(); 
      for ( int i = 0; i < gridSize; i++ )
      {
         if ( grid[ source ][ i ] == 1 )
         {
            neighbors.add( i );
         }
      }
      
      return neighbors;
   }
   
   public String toString ()
   {
      String string = "";
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int k = 0; k < gridSize; k++ )
         {
            string += grid[ i ][ k ]  + " ";
         }
         string += "\n";
      }
      
      return string;
   }
   
}
