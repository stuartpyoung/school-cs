// Name: Rio Young  
// Date: 4/30/19
 
import java.util.*;
import java.io.*;

/* Resource classes and interfaces
 * for use with Graph0 AdjMat_0_Driver,
 * Graph1 WarshallDriver,
 * and Graph2 FloydDriver
 */

interface AdjacencyMatrix
{
   void addEdge(int source, int target);
   void removeEdge(int source, int target);
   boolean isEdge(int from, int to);
   String toString();   
   int edgeCount();
   List<Integer> getNeighbors(int source);
   //public List<String> getReachables(String from);  //Warshall extension
}

interface Warshall      
{
   boolean isEdge(String from, String to);
   Map<String, Integer> getVertices();     
   void readNames(String fileName) throws FileNotFoundException;
   void readGrid(String fileName) throws FileNotFoundException;
   void displayVertices();
   void allPairsReachability();  // Warshall's Algorithm
}

interface Floyd
{
   int getCost(int from, int to);
   int getCost(String from, String to);
   void allPairsWeighted(); 
}

public class AdjMat implements AdjacencyMatrix, Floyd
{
   //adjacency matrix representation
   private int[][] grid = null;   
   
   // name-->index (for Warshall & Floyd)
   private Map<String, Integer> vertices = null;
   private Map<Integer, String> invertedVertices = null;
   private int gridSize = 0;
     
   // constructor
   public AdjMat ( int size )
   {
      grid = new int[ size ][ size ];
      gridSize = size;
      vertices = new TreeMap<String, Integer>();
   }

   private void invertVertices()
   {   
      Map<Integer, String> tempVertices = new TreeMap<Integer, String>();
      
      for ( String key : vertices.keySet() )      
      {
         //System.out.println( "invertVertices    key: " + key );
         tempVertices.put( vertices.get( key ), key );
      }
      //System.out.println( "invertVertices    tempVertices: " + tempVertices );
      
      invertedVertices = tempVertices;
      
      return;
   }
   
   public ArrayList<String> getReachables ( String city )
   {
      //System.out.println( "getReachables    invertedVertices: " + invertedVertices );
      ArrayList<String> list = new ArrayList<String>();
      //System.out.println( "getReachables    city: " + city );
      int row = vertices.get( city );
      //System.out.println( "getReachables    row: " + row );
      for ( int i = 0; i < gridSize; i++ )
      {
         if ( grid[ row ][ i ] == 1 )
         {
            //System.out.println( "getReachables     i: " + i );
            //System.out.println( "getReachables    invertedVertices.get( " + i  + "): " + invertedVertices.get( i ) );
            list.add( invertedVertices.get( i ) );
         }
      }
            
      return list;
   }
   
   public void addEdge ( int source, int target )
   {
      if ( source > gridSize - 1 || target > gridSize - 1 )
      {
         return;
      }
   
      grid[ source ][ target ] = 1;   
   }
   
   public void removeEdge ( int source, int target )
   {
      if ( source > gridSize - 1 || target > gridSize - 1 )
      {
         return;
      }
   
      grid[ source ][ target ] = 0;
   }
   
   public boolean isEdge ( int source, int target )
   {
      if ( grid[ source ][ target ] != 0 &&  grid[ source ][ target ] != 9999 )
      {
         //System.out.println("isEdge    IS EDGE: [" + source  + ", " + target + "]");
         return true;
      }
   
      return false;
   }
   
   public boolean isEdge(String from, String to)
   {
      //System.out.println( "isEdge    from: " + from );
      //System.out.println( "isEdge    to: " + to );
      //System.out.println( "isEdge    vertices.get( from ): " + vertices.get( from ) );
      //System.out.println( "isEdge    vertices.get( to ): " + vertices.get( to ) );
      //
      if ( grid[ vertices.get( from ) ][ vertices.get( to ) ] == 1 )
      {
         return true;
      }
      
      return false;
   }
   
   public Map<String, Integer> getVertices()
   {
      return vertices;
   }
   
   public void readNames(String fileName) throws FileNotFoundException
   {
      //System.out.println( "readNames    filename: " + fileName );
      Scanner scanner = new Scanner( new File( fileName ) );
      int value = 0;
      scanner.nextLine();
      while ( scanner.hasNext() )
      {
         String key = scanner.nextLine();
         key = key.replaceAll("\\s", "");

         vertices.put( key, value );
         value++;
      }
     // System.out.println( "readNames    vertices: " + vertices );
      invertVertices();
      
      return;
   }
   
   public void readGrid( String fileName ) throws FileNotFoundException
   {
      //System.out.println( "readGrid    filename: " + fileName );
      Scanner scanner = new Scanner( new File( fileName ) );
      int row = 0;
      scanner.nextLine();
      while ( scanner.hasNext() )
      {
         String line = scanner.nextLine();
         String[] entries = line.split( " " );
         //System.out.println( "readGrid    entries: " + Arrays.toString( entries ) );
         for ( int i = 0; i < entries.length; i++ )
         {
            grid[ row ][ i ] = Integer.parseInt( entries[ i ] );
         }
         row++;
      }

      return;
   }
   
   public void displayVertices()
   {
      for ( String key: vertices.keySet() )
      {
         System.out.println( vertices.get( key ) + "-" + key );
      }
      
      System.out.println( );
      
      return;
   }
   
   public void allPairsReachability() // Warshall's Algorithm
   {
      int[][] reachability = setReachability();
      grid = reachability;
      reachability = setReachability();
      grid = reachability;
      
      return;
   }

   public int[][] setReachability( ) // Warshall's Algorithm
   {
      int[][] reachability = new int[ gridSize ][ gridSize ];

      for ( int i = 0; i < gridSize; i++ )
      {
         
         for ( int v = 0; v < gridSize; v++ )
         {
            if ( v == i )
            {
               continue;
            }
            
            if ( isEdge( i, v ) )
            {
               //System.out.println( "setReachability   outer isEdge( " + i + ", " + v + " )" );
               for ( int j = 0; j < gridSize; j++ )
               {
                  
                  if ( isEdge( v, j ) )
                  {
                     //System.out.println( "setReachability   inner isEdge( " + v + ", " + j + " )" );
                     reachability[ i ][ j ] = 1;
                  }
                  else if ( isEdge( i, j ) )
                  {
                     //System.out.println( "setReachability   inner isEdge( " + v + ", " + j + " )" );
                     reachability[ i ][ j ] = 1;
                  }
               }
            }
            
         }
      }
      //System.out.println( "setReachability    reachability: " + Arrays.deepToString( reachability ) );

      return reachability;
   }

   public int edgeCount ( )
   {
      int count = 0;
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int k = 0; k < gridSize; k++ )
         {
            if ( grid[ i ][ k ] != 0 && grid[ i ][ k ] != 9999 )
            {
               count++;
            }
         }
      }
   
      return count;
   }

   public List<Integer> getNeighbors ( int source )
   {
      List<Integer> neighbors = new ArrayList<Integer>(); 
      for ( int i = 0; i < gridSize; i++ )
      {
         if ( grid[ source ][ i ] == 1 )
         {
            neighbors.add( i );
         }
      }
      
      return neighbors;
   }
   
   public int getCost(int from, int to)
   {
      return grid[ from ][ to ];
   }
   
   public int getCost(String from, String to)
   {
      return grid[ vertices.get( from ) ][ vertices.get( to ) ];
   }
   
   public void allPairsWeighted()
   {
      int[][] reachability = setPairsWeighted();
      grid = reachability;
      reachability = setPairsWeighted();
      grid = reachability;
      
      return;
   }

   public int[][] copyGrid( int[][] sourceGrid ) // Floyd's Algorithm
   {
      int[][] targetGrid = new int[ sourceGrid.length ][ sourceGrid.length ];
        
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int j = 0; j < gridSize; j++ )
         {
            targetGrid[i][j] = sourceGrid[i][j];
         }
      }

      return targetGrid;    
   }

   public int[][] setPairsWeighted( ) // Floyd's Algorithm
   {
      int[][] reachability = copyGrid( grid );
      int[][] tempGrid = copyGrid( grid );
  
      for ( int i = 0; i < gridSize; i++ )
      {
         
         for ( int v = 0; v < gridSize; v++ )
         {
            if ( v == i )
            {
               continue;
            }
            
            if ( isEdge( i, v ) )
            {
               //System.out.println( "setReachability   outer isEdge( " + i + ", " + v + " )" );
               for ( int j = 0; j < gridSize; j++ )
               {
                  
                  if ( isEdge( v, j ) )
                  {
                     //System.out.println( "setReachability   inner isEdge( " + v + ", " + j + " )" );
                     int newValue = tempGrid[i][v] + reachability[v][j];
                     //System.out.println( "setReachability   newValue: " + newValue );

                     if ( tempGrid[i][j] > newValue && reachability[i][j] > newValue)
                     {
                        reachability[ i ][ j ] = newValue;
                        
                        //System.out.println( "setReachability   v: " + v + ", reachability[" + i + "][" + j + "]: " + reachability[i][j] + " == [" + i + "][" + v + "] (" + reachability[ i ][ v ] + ") + [" + v + "][" + j + "] (" + reachability[ v ][ j ] + ")" );
                     }
                  }
               }
            }
         }
      }
      
      //System.out.println( "\n\nsetPairsWeighted    reachability.toString:\n");
      //String string = "";
      //for ( int i = 0; i < gridSize; i++ )
      //{
      //   for ( int k = 0; k < gridSize; k++ )
      //   {
      //      int value = reachability[ i ][ k ] != 0 ? reachability[ i ][ k ]: 9999;
      //      string += value  + " ";
      //   }
      //   string += "\n";
      //}
      //
      //System.out.println( string );
  
      return reachability;
   }
   
   //public int[][] seconRunThrough( int[][] original )
   //{
   //   int[][] newGrid = r
   //}

   public String toString ()
   {
      String string = "";
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int k = 0; k < gridSize; k++ )
         {
            string += grid[ i ][ k ]  + " ";
         }
         string += "\n";
      }
      
      return string;
   }
   
}
