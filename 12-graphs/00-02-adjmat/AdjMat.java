// Name:   
// Date:
 
import java.util.*;
import java.io.*;

/* Resource classes and interfaces
 * for use with Graph0 AdjMat_0_Driver,
 * Graph1 WarshallDriver,
 * and Graph2 FloydDriver
 */

interface AdjacencyMatrix
{
   void addEdge(int source, int target);
   void removeEdge(int source, int target);
   boolean isEdge(int from, int to);
   String toString();   
   int edgeCount();
   List<Integer> getNeighbors(int source);
   //public List<String> getReachables(String from);  //Warshall extension
}

interface Warshall      
{
   boolean isEdge(String from, String to);
   Map<String, Integer> getVertices();     
   void readNames(String fileName) throws FileNotFoundException;
   void readGrid(String fileName) throws FileNotFoundException;
   void displayVertices();
   void allPairsReachability();  // Warshall's Algorithm
}

interface Floyd
{
   int getCost(int from, int to);
   int getCost(String from, String to);
   void allPairsWeighted(); 
}

public class AdjMat implements AdjacencyMatrix 
{
   private int[][] grid = null;   //adjacency matrix representation
   private Map<String, Integer> vertices = null;   // name-->index (for Warshall & Floyd)
   private gridSize = null;
     
   // constructor
   public void AdjMat ( int size )
   {
      grid = int[ size ][ size ];
      gridSize = size;
   }

   public boolean addEdge ( int source, int target )
   {
      if ( source > size - 1 || target > size - 1 )
      {
         return false;
      }
   
      grid[ source ][ target ] = 1;
   
      return true;
   }
   
   public boolean removeEdge ( )
   {
      if ( source > size - 1 || target > size - 1 )
      {
         return false;
      }
   
      grid[ source ][ target ] = 0;
   
      return true;    
   }
   
   public boolean isEdge ( int source, int target )
   {
      if ( grid[ source ][ target ] = 1 )
      {
         return true;
      }
   
      return false;
   }

   public int edgeCount ( )
   {
      int count = 0;
      for ( int i = 0; i < gridSize; i++ )
      {
         for ( int k = 0; k < gridSize; k++ )
         {
            if ( grid[ source ][ target ] = 1 )
            {
               count++;
            }
         }
      }
   
      return count;
   }

   
   
   
}
