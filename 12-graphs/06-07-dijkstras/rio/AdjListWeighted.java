// Name: Rio Young
// Date: 5/28/19
 
import java.util.*;
import java.io.*;

/* Resource classes and interfaces
 * for use with Graphs6: Dijkstra
 * and Graphs7: Dijkstra with Cities
 */

class Edge 
{
   //public fields not common on AP exam
   public final wVertex target;  
   public final double weight;   
  
   public Edge(wVertex argTarget, double argWeight) 
   {
      target = argTarget;
      weight = argWeight;
   }
   
   public String toString()
   {
      return target.getName() + "|" + weight;
   }
}

interface wVertexInterface 
{
   String getName();
   double getMinDistance();
   void setMinDistance(double m);
   HashSet getPrevious();   //for Dijkstra 7
   void setPrevious(String v);  //for Dijkstra 7
   ArrayList<Edge> getAdjacencies();
   void addEdge(wVertex v, double weight);   
   int compareTo(wVertex other);
}

class wVertex implements Comparable<wVertex>, wVertexInterface
{
   private final String name;
   private ArrayList<Edge> adjacencies;
   private double minDistance = Double.POSITIVE_INFINITY;
   private HashSet<String> previous;  //for building the actual path in Dijkstra 7
   
   /*  enter your code for this class here   */ 
   
   public wVertex( String newName )
   {
      name = newName;
      adjacencies = new ArrayList<Edge>();
      previous = new HashSet<String>();
      previous.add( newName );
   }
   
   
   public String getName()
   {
      return name;
   }
   
   public double getMinDistance()
   {
      return minDistance;
   }
   
   
   public void setMinDistance( double newMinDistance )
   {
      minDistance = newMinDistance;
   }
   
   public HashSet getPrevious()   //for Dijkstra 7
    {
      return previous;
    }
   
   public void setPrevious( String vertexName )  //for Dijkstra 7
    {
      previous.add( vertexName );
    }
   
   public ArrayList<Edge> getAdjacencies()
   {
      return adjacencies;
   }
   
   public void addEdge( wVertex vertex, double weight )  
   {
      adjacencies.add( new Edge( vertex, weight ) );
   }
   
   public int compareTo( wVertex other )
   {
      return new Double( this.getMinDistance() ).compareTo( other.getMinDistance() );
   }
   
   public String toString()
   {
      return name + " ( " + getMinDistance() + " ) " + adjacencies + ", previous: " + previous ;
   }
}

interface AdjListWeightedInterface 
{
   List<wVertex> getVertices();
   Map<String, Integer> getNameToIndex();
   wVertex getVertex(int i);   
   wVertex getVertex(String vertexName);
   void addVertex(String v);
   void addEdge(String source, String target, double weight);
   void minimumWeightPath(String vertexName);   //Dijkstra's
}

/* Interface for Graphs 7:  Dijkstra with Cities 
 */

interface AdjListWeightedInterfaceWithCities 
{       
   List<String> getShortestPathTo(wVertex v);
   AdjListWeighted graphFromEdgeListData(File vertexNames, File edgeListData) throws FileNotFoundException ;
}


public class AdjListWeighted implements AdjListWeightedInterface, AdjListWeightedInterfaceWithCities
{
   private List<wVertex> vertices = new ArrayList<wVertex>();
   private Map<String, Integer> nameToIndex = new HashMap<String, Integer>();
   private int nameToIndexCount = 0;
   private List<String> stringList = new LinkedList<String>(); 
   private String originVertexName = ""; 
   //the constructor is a no-arg constructor 
  
   /*  enter your code for Graphs 6 */ 
   public List<wVertex> getVertices()
   {
      return vertices;
   }
      
   public Map<String, Integer> getNameToIndex()
   {
      return nameToIndex;
   }
   
   public wVertex getVertex(int i)
   {
      return vertices.get( i );
   }   
   
   public wVertex getVertex(String vertexName)
   {
      for ( int i = 0 ; i < vertices.size() ; i++ )
      {
         if ( vertices.get( i ).getName().equals( vertexName ) )
         {
            return vertices.get( i );
         }
      }
      
      return new wVertex( "SOMETHING'S WRONG" );
   }
   
   public void addVertex(String vertex )
   {
      vertices.add( new wVertex( vertex ) );
      nameToIndex.put( vertex, nameToIndexCount );
      nameToIndexCount++;
   }
   
   public void addEdge(String source, String target, double weight)
   {
      wVertex vertex = null;
      if ( nameToIndex.get( target ) == null )
      {
         vertex = new wVertex( target );
      }
      else
      {
         vertex = vertices.get( nameToIndex.get( target ) );
      }

      getVertex( source ).addEdge( vertex, weight );

      return;
   }

   public void minimumWeightPath( String vertexName )   //Dijkstra's
   {
      originVertexName = vertexName;

      PriorityQueue<wVertex> queue = new PriorityQueue<wVertex>();
      getVertex( vertexName ).setMinDistance( 0 ); 
      queue.add(  getVertex( vertexName ) );
      
      // System.out.println( "minimumWeightPath    BEFORE queue: " + queue );

      while ( ! queue.isEmpty() )
      {
         wVertex currentVertex = queue.remove();
         
         System.out.println( "minimumWeightPath    ************* currentVertex: " + currentVertex.getName() );
         System.out.println( "minimumWeightPath    currentVertex.getAdjacencies(): " + currentVertex.getAdjacencies() );
         
         for ( int i = 0; i < currentVertex.getAdjacencies().size() ; i++ )
         {
            Edge currentEdge = currentVertex.getAdjacencies().get( i );
            System.out.println( "minimumWeightPath    ******* currentEdge.target.getName() " + i + ": " + currentEdge.target.getName() );
            double distance = currentVertex.getMinDistance() + currentEdge.weight;
            System.out.println( "minimumWeightPath    distance: " + distance );
            
            
            if ( currentEdge.target.getName().equals( vertexName ) ) {
               continue;
            }
            
            wVertex targetVertex = vertices.get( nameToIndex.get( currentEdge.target.getName() ) );



            // DISCARD THE VERTEX IF IT'S ALREADY IN THE PATH (I.E., A MEMBER OF THE SET previous)
            HashSet previous = currentVertex.getPrevious();
            System.out.println( "minimumWeightPath     currentVertex " + currentVertex.getName() + " previous: " + previous );
            if ( previous.contains( currentEdge.target.getName() ) )
            {
               System.out.println( "minimumWeightPath     SKIPPING currentEdge.target.getName(): " + currentEdge.target.getName() );
               continue;   
            }



            System.out.println( "minimumWeightPath    currentVertex.getMinDistance(): " + currentVertex.getMinDistance() );
            System.out.println( "minimumWeightPath    currentEdge.target.getMinDistance(): " + currentEdge.target.getMinDistance() );
            
            if ( distance < currentEdge.target.getMinDistance() )
            {
               targetVertex.setPrevious( currentVertex.getName() );
               System.out.println( "minimumWeightPath    CHANGING currentEdge.target " + currentEdge.target.getName() + " minDistance TO " + distance );
               currentEdge.target.setMinDistance( distance ); 
               System.out.println( "minimumWeightPath    currentEdge.target.getMinDistance(): " + currentEdge.target.getMinDistance() );
            }
            queue.add( targetVertex );

            System.out.println( "minimumWeightPath    AFTER queue: " + queue );
            System.out.println( );
         }
      }
      
      // System.out.println( "minimumWeightPath    FINAL queue: " + queue );
   }
   
   @SuppressWarnings("unchecked")
   public List<String> getShortestPathTo( wVertex vertex )
   {
      System.out.println( "getShortestPathTo    vertex: " + vertex );
      System.out.println( "getShortestPathTo    originVertexName: " + originVertexName );
      List<String> stringList = new ArrayList<String>();
      HashSet previous = vertex.getPrevious();
      System.out.println( "getShortestPathTo    previous: " + previous );

      Iterator<String> iterator = previous.iterator();
      while ( iterator.hasNext() ) 
      {
         stringList.add( 0, iterator.next() );
      } 

      if ( ! vertex.getName().equals( originVertexName ) 
         && vertex.getMinDistance() != Double.POSITIVE_INFINITY )
      {
         stringList.add( 0, originVertexName );
      }   

      return stringList;
   }
   
   
   public AdjListWeighted graphFromEdgeListData( File vertexNames, File edgeListData) throws FileNotFoundException
   {
      AdjListWeighted graph = new AdjListWeighted();
      Scanner scanner = new Scanner( vertexNames );
      scanner.nextLine();
      while ( scanner.hasNextLine() )
      {
         String vertex = scanner.nextLine();
         // System.out.println( vertex );
         graph.addVertex( vertex );
      }
      
      Scanner scanner2 = new Scanner( edgeListData );
      while ( scanner2.hasNextLine() )
      {
         String edgeString = scanner2.nextLine();
         String[] edge = edgeString.split( " " );
         graph.addEdge( edge[ 0 ], edge[ 1 ], Double.parseDouble( edge[ 2 ] ) );
         // System.out.println( edgeString );
      }
      
      return graph;
   }
   
   public String toString()
   {
      return vertices.toString();
      
   }
   
   
   
   /*  enter your code for two new methods in Graphs 7 */
   
   
}   


