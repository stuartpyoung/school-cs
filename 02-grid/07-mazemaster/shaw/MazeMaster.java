//name: Shaw Young    date: 10-15-18
import java.util.*;
import java.io.*;
public class MazeMaster
{
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("Enter the maze's filename (no .txt): ");
      Maze m = new Maze(sc.next());
      // Maze m = new Maze();     //extension:  generate a random maze
      m.display();      //display maze
      System.out.println("Options: ");
      System.out.println("1: Mark all paths.");
      System.out.println("2: Mark all paths, and display the count of all STEPs.");
      System.out.println("3: Mark only the correct path.");
      System.out.println("4: Mark only the correct path. If no path exists, say so.");
      System.out.println("5: Mark only the correct path and display the count of STEPs.\n\tIf no path exists, say so.");
      System.out.print("Please make a selection: ");
      m.solve(sc.nextInt());
//	   String filename = "maze1";
//	   System.out.println("Output for " + filename + ": ");
//	   Maze m = new Maze( filename );
	   m.display();
//	   System.out.println("1:");
//	   m.solve(1);
//	   m.display();
	   
//	   m.solve( 5 );
//	   m.display();
//	   m.solve(3);
//	   m.display();
//	   m.solve(4);
//	   m.display();
//	   m.solve(5);
//	   m.display();
   } 
}


class Maze
{
   //Constants
   private final char WALL = 'W';
   private final char DOT = '.';
   private final char START = 'S';
   private final char EXIT = 'E';
   private final char STEP = '*';
   //fields
   private char[][] maze;
   private int startRow, startColumn;
  
  //constructors
   public Maze()  //extension:  generate a random maze
   {
   }
   
   public Maze(char[][] m)  //copy constructor
   {
      maze = m;
      for(int r = 0; r < maze.length; r++)
      {
         for(int c = 0; c < maze[0].length; c++)
         { 
            if(maze[r][c] == START)      //identify start
            {
               startRow = r;
               startColumn = c;
               
            }
         }
      }
   } 
   
   public Maze(String filename)    
   {
      filename += ".txt";
      Scanner infile = null;

      try
      {
    	  infile = new Scanner(new File(filename));

    	  int rowLength = infile.nextInt();
    	  int columnLength = infile.nextInt();
    	  String sss = infile.nextLine();
         
    	  maze = new char[rowLength][columnLength];
         
    	  for ( int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
    		  String string = infile.nextLine();
    		  String[] stringArray = string.split("");
    		  for ( int columnIndex = 0; columnIndex < columnLength; columnIndex++ ) {
    			  maze[ rowIndex ][ columnIndex ] = stringArray[columnIndex].charAt(0);
       	  	  }
    	  }
    	  for(int r = 0; r < maze.length; r++)
          {
             for(int c = 0; c < maze[0].length; c++)
             { 
                if(maze[r][c] == START)      //identify start
                {
                   startRow = r;
                   startColumn = c;
                   break;
                }
             }
          }
      }
      
      catch (Exception e)
      {
         System.out.println("File error:    " + e );
      }

   }

   public char[][] getMaze()
   {
      return maze;
   }

   public void display()
   {
      if(maze==null) 
         return;
      for(int a = 0; a < maze.length; a++)
      {
         for(int b = 0; b<maze[0].length; b++)
         {
            System.out.print(maze[a][b]);
         }
         System.out.println("");
      }
      System.out.println("");
   }
   
   public void solve(int n)
   {
      switch(n)
      {
         case 1:
            {
            	markAllPaths(startRow, startColumn);
               
               break;
            }
         case 2:
            {
               int count = markAllPathsAndCountStars(startRow, startColumn);
               System.out.println("Number of steps = " + count);
               break;
            }
         case 3:
            {
               markTheCorrectPath(startRow, startColumn);
               break;
            }
         case 4:         //use mazeNoPath.txt 
            {
               if( !markTheCorrectPath(startRow, startColumn) )
                  System.out.println("No path exists."); 
               break;
            }
         case 5:
            {
               if( !markCorrectPathAndCountSteps(startRow, startColumn, 0) )
                  System.out.println("No path exists."); 
               break;
            }
         default:
            System.out.println("File not found");   
      }
   }
   
    /*  1  almost like AreaFill*/
   public void markAllPaths(int rowCounter, int columnCounter)
   {
	   if ( rowCounter >= 0
         && rowCounter < maze.length
         && columnCounter >= 0
         && columnCounter < maze[0].length
         &&  ( maze[ rowCounter ][ columnCounter ] == DOT
         || maze[ rowCounter ][ columnCounter ] == START )
         ) {
         if ( maze[ rowCounter ][ columnCounter ] == DOT ) {
        	maze[ rowCounter ][ columnCounter ] = STEP;
            markAllPaths( rowCounter - 1, columnCounter );
   	     	markAllPaths( rowCounter + 1, columnCounter);
   	     	markAllPaths( rowCounter, columnCounter - 1);
   	     	markAllPaths( rowCounter, columnCounter + 1);
         }
         else if ( maze[ rowCounter ][ columnCounter ] == START ) {
        	 markAllPaths( rowCounter - 1, columnCounter );
    	     markAllPaths( rowCounter + 1, columnCounter);
    	     markAllPaths( rowCounter, columnCounter - 1);
    	     markAllPaths( rowCounter, columnCounter + 1);
         }
      }
   }
   
    /*  2  like AreaFill's counting without a static variable */  
   public int markAllPathsAndCountStars(int rowCounter, int columnCounter)
   {
	   if ( rowCounter < 0
         || rowCounter > maze.length
         || columnCounter < 0
         || columnCounter >= maze[0].length
         ||  ( maze[ rowCounter ][ columnCounter ] != DOT
             && maze[ rowCounter ][ columnCounter ] != START )
        ) {	   
		  return 0;
	   }
	   else {

		   if ( maze[ rowCounter ][ columnCounter ] == DOT ) {
			   maze[ rowCounter ][ columnCounter ] = STEP;
		   }
		   
		   int counter = 1;
           if ( maze[ rowCounter ][ columnCounter ] == START ) {
        	   counter = 0;
           }

           counter += markAllPathsAndCountStars( rowCounter - 1, columnCounter );
		   counter += markAllPathsAndCountStars( rowCounter + 1, columnCounter );
		   counter += markAllPathsAndCountStars( rowCounter, columnCounter - 1 );
		   counter += markAllPathsAndCountStars( rowCounter, columnCounter + 1 );

		   return counter;
	   }
   }

   /*  3   recur until you find E, then mark the True path */
   public boolean markTheCorrectPath(int rowCounter, int columnCounter )
   {
      System.out.println( "markTheCorrectPath    (" + rowCounter + ", " + columnCounter + ") --------------" );

	   char character = maze[ rowCounter ][ columnCounter ];
	   if ( character == EXIT ) {
		   return true;
	   }
      else if ( character != START ) {
         if ( character == DOT ) {
         maze[ rowCounter ][ columnCounter ] = 'o';      
         }
         else if ( character != START ) {
            return false;
         }
      }
      
     // RECURSE ALL DIRECTIONS   
	  if ( 
         // LEFT
         ( ! inBounds( rowCounter, columnCounter - 1 )
            || ! markTheCorrectPath( rowCounter- 1, columnCounter )
         )
         // DOWN
         && 
         (
           ! inBounds( rowCounter + 1, columnCounter )
            || ! markTheCorrectPath( rowCounter + 1, columnCounter ) 
         )
         // RIGHT 
         && (
            ! inBounds( rowCounter, columnCounter + 1 )
           || ! markTheCorrectPath( rowCounter, columnCounter + 1 )
         )
         // UP
         && (
           ! inBounds( rowCounter - 1, columnCounter )
           || ! markTheCorrectPath( rowCounter - 1, columnCounter )	
         )
      ) {   
           if ( ! isStart( rowCounter, columnCounter) ) {
             maze[ rowCounter][ columnCounter ] = '.';          
           }
           
           if ( character != START ) {          
             return false;
           }
         }
         else {
           maze[ rowCounter][ columnCounter ] = '*';
         
           return true;
         }

      return true;
   }
   
   boolean isStart ( int rowCounter, int columnCounter ) {
      if ( rowCounter == startRow
          && columnCounter == startColumn ) {
         return true;
      }
      
      return false;
   }
   
   boolean inBounds ( int rowCounter, int columnCounter ) {	   
      if ( columnCounter >= 0 
    	&& columnCounter < maze[0].length
    	&& rowCounter >= 0
    	&& rowCounter < maze.length 
	  ) {
	   //System.out.println( "inBounds    (" + rowCounter + ", " + columnCounter + ")    RETURNING TRUE" );
    	  return true;
      }

      //System.out.println( "inBounds    (" + rowCounter + ", " + columnCounter + ")    RETURNING FALSE" );
      return false;	   
   }
   
   
   /*  4   Mark only the correct path. If no path exists, say so.
           Hint:  the method above returns the boolean that you need.  */
      

   /*  5  Mark only the correct path and display the count of STEPs.
          If no path exists, say so. */
   public boolean markCorrectPathAndCountSteps(int rowCounter, int columnCounter, int count)
   {
      
	   char character = maze[ rowCounter ][ columnCounter ];
	   if ( character == EXIT ) {
         System.out.println( "Number of steps: " + count );
		   return true;
	   }
      else if ( character == START || character == DOT ) {
         count++;
         if ( character == DOT ) {
            maze[ rowCounter ][ columnCounter ] = 'o';      
         }
      }
      else {
         return false;
      }
     // RECURSE ALL DIRECTIONS   
	  if ( 
         // LEFT
         ( ! inBounds( rowCounter, columnCounter - 1 )
            || ! markCorrectPathAndCountSteps( rowCounter- 1, columnCounter, count )
         )
         // DOWN
         && 
         (
           ! inBounds( rowCounter + 1, columnCounter )
            || ! markCorrectPathAndCountSteps( rowCounter + 1, columnCounter, count ) 
         )
         // RIGHT 
         && (
            ! inBounds( rowCounter, columnCounter + 1 )
           || ! markCorrectPathAndCountSteps( rowCounter, columnCounter + 1, count )
         )
         // UP
         && (
           ! inBounds( rowCounter - 1, columnCounter )
           || ! markCorrectPathAndCountSteps( rowCounter - 1, columnCounter, count )	
         )
      ) {   
           if ( ! isStart( rowCounter, columnCounter) ) {
             maze[ rowCounter][ columnCounter ] = '.';          
           }
           
           if ( character != START ) {          
             
             return false;
           }
         }
         else {
           maze[ rowCounter][ columnCounter ] = '*';
           //System.out.println( "markCorrectPathAndCountSteps    (" + rowCounter + ", " + columnCounter + ")    RETURNING TRUE" );
           return true;
         }

      return true;
   }
   
}
