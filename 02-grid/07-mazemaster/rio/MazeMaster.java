
//name: Rio Young
//date: 10/15/18
import java.util.*;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class MazeMaster
{
  public static void main(String[] args)
  {
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter the maze's filename (no \".txt\") OR 'random' to generate random maze: ");
    String filename = sc.next();
    //String filename = "random";
    //String filename = "mazeNoPath";
    //String filename = "maze1";
    //String filename = "maze6";
    //String filename = "maze7";
    //System.out.println( "filename: " + filename );
      
    Maze maze;
    if ( filename.equals( "random" ) )
    {
      maze = new Maze();  
    }
    else
    {
      maze = new Maze( filename );      
    }
    
    Maze m = new Maze();     //extension:  generate a random maze
    maze.display();      //display maze
    System.out.println("Options: ");
    System.out.println("1: Mark all paths.");
    System.out.println("2: Mark all paths, and display the count of all STEPs.");
    System.out.println("3: Mark only the correct path.");
    System.out.println("4: Mark only the correct path. If no path exists, say so.");
    System.out.println("5: Mark only the correct path and display the count of STEPs.\n\tIf no path exists, say so.");
    System.out.print("Please make a selection: ");
    maze.solve( sc.nextInt() );
    //maze.solve( 5 );
    maze.display();      //display solved maze  
  }
}

class Maze
{
  //Constants
  private final char WALL = 'W';
  private final char DOT = '.';
  private final char START = 'S';
  private final char EXIT = 'E';
  private final char STEP = '*';
  private final int SIZEMAXIMUM = 10;
  private final int SIZEMINIMUM = 5;
  private final double DOTPROBABILITY = 0.70;
  //private final double WALLPROBABILITY = 0.30;
  
  //fields
  private char[][] maze;
  private int startRow, startColumn;
  
  /*  1  almost like AreaFill*/
  public void markAllPaths( int rowIndex, int columnIndex )
  {
    //System.out.println( "markAllPaths    maze[" + rowIndex + "][" + columnIndex + "]: " + maze[ rowIndex][ columnIndex ] );
    
    char character = '.';
    int numberRows = maze.length;
    int numberColumns = maze[0].length;
    
    if ( maze[ rowIndex][ columnIndex ] == '*' )
    {
      return;
    }
  
    if ( maze[ rowIndex][ columnIndex ] == character
        || maze[ rowIndex][ columnIndex ] == 'S' )
    {
      
      if ( maze[ rowIndex][ columnIndex ] != 'S'
        && maze[ rowIndex][ columnIndex ] != 'E' ) {
        //System.out.println( "markAllPaths    (" + rowIndex + ", " + columnIndex + ")    CONVERTING TO '*'" );
        maze[ rowIndex][ columnIndex ] = '*';      
      }
    
      // up
      if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
      {
        markAllPaths( rowIndex - 1, columnIndex );
      }
      
      // down
      if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
      {
        markAllPaths( rowIndex + 1, columnIndex );
      }
      
      // left
      if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
      {    
        markAllPaths( rowIndex, columnIndex - 1 );  
      }
      
      // right
      if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
      {
        markAllPaths( rowIndex, columnIndex + 1 );
      }
    }    
  }
  
  /*  2  like AreaFill's counting without a static variable */  
  public int markAllPathsAndCountStars( int rowIndex, int columnIndex )
  {
    //System.out.println( "markAllPaths    maze[" + rowIndex + "][" + columnIndex + "]: " + maze[ rowIndex][ columnIndex ] );
    
    char character = '.';
    int numberRows = maze.length;
    int numberColumns = maze[0].length;
    
    // SET COUNTER
    int counter = 0;
    
    if ( maze[ rowIndex][ columnIndex ] == '*' )
    {
      return counter;
    }
    
    if ( maze[ rowIndex][ columnIndex ] == character
        || maze[ rowIndex][ columnIndex ] == 'S' )
    {
      if ( maze[ rowIndex][ columnIndex ] != 'S'
        && maze[ rowIndex][ columnIndex ] != 'E' ) {
        //System.out.println( "markAllPathsAndCountStars    (" + rowIndex + ", " + columnIndex + ")    CONVERTING TO '*'" );
        counter++;        
        maze[ rowIndex][ columnIndex ] = '*';      
      }
    
      // up
      if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
      {
        counter += markAllPathsAndCountStars( rowIndex - 1, columnIndex );
      }
      
      // down
      if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
      {
        counter += markAllPathsAndCountStars( rowIndex + 1, columnIndex );
      }
      
      // left
      if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
      {    
        counter += markAllPathsAndCountStars( rowIndex, columnIndex - 1 );  
      }
      
      // right
      if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
      {
        counter += markAllPathsAndCountStars( rowIndex, columnIndex + 1 );
      }
    }    

    return counter;
  }

  /*  3   recur until you find E, then mark the True path */
  public boolean markTheCorrectPath( int rowIndex, int columnIndex )
  {
     //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")" );
     char character = '.';
     int numberRows = maze.length;
     int numberColumns = maze[0].length;
     boolean up = false;
     boolean down = false;
     boolean left = false;
     boolean right = false;
     
     // Return true if exit found
     if ( maze[ rowIndex][ columnIndex ] == 'E' )
     {
       //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    FOUND EXIT!!!!!!!!!!!!" );
       return true;
     }
     
     if ( maze[ rowIndex][ columnIndex ] != character
         && maze[ rowIndex][ columnIndex ] != 'S' )
     {
       //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    '" + maze[ rowIndex][ columnIndex ] + "' DOES NOT MATCH '.' OR 'S' " );        
       return false;
     }
     //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    MATCH!!!" );
     
     if ( maze[ rowIndex][ columnIndex ] != 'S' )
     {
       //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    CONVERTING TO 'o'" );
       maze[ rowIndex][ columnIndex ] = 'o';      
     }
     
     // up
     if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
     {
       up = markTheCorrectPath( rowIndex - 1, columnIndex );
     }
     
     // down
     if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
     {
       down = markTheCorrectPath( rowIndex + 1, columnIndex );
     }
     
     // left
     if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
     {    
       left = markTheCorrectPath( rowIndex, columnIndex - 1 );  
     }
     
     // right
     if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
     {
       right = markTheCorrectPath( rowIndex, columnIndex + 1 );
     }
     
     if ( up || down || left || right
       && maze[ rowIndex][ columnIndex ] != 'S' )
     {
       maze[ rowIndex][ columnIndex ] = '*';
               
       //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    RETURNING TRUE" );
  
       return true;
     }
     else
     {
       // convert back to '.'
       //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    CONVERTING TO '.'" );
       if ( rowIndex != startRow || columnIndex != startColumn )
       {
         maze[ rowIndex][ columnIndex ] = '.';          
       }
  
       if ( maze[ rowIndex][ columnIndex ] != 'S' ) {          
         //System.out.println( "markTheCorrectPath    (" + rowIndex + ", " + columnIndex + ")    RETURNING FALSE" );
         
         return false;
       }
     }
     
     return true;
  }

   /*  4   Mark only the correct path. If no path exists, say so.
           Hint:  the method above returns the boolean that you need.  */
      

   /*  5  Mark only the correct path and display the count of STEPs.
          If no path exists, say so. */
   public boolean markCorrectPathAndCountSteps( int rowIndex, int columnIndex, int counter)
   {
      char character = '.';
      int numberRows = maze.length;
      int numberColumns = maze[0].length;
      boolean up = false;
      boolean down = false;
      boolean left = false;
      boolean right = false;
      
      // Return true if exit found
      if ( maze[ rowIndex][ columnIndex ] == 'E' )
      {
        System.out.println( "Number of steps: " + counter );

        return true;
      }

      
      if ( maze[ rowIndex][ columnIndex ] != character
          && maze[ rowIndex][ columnIndex ] != 'S' )
      { 
        return false;
      }
      else
      {
        counter++;
      }
      
      if ( maze[ rowIndex][ columnIndex ] != 'S' )
      {
        maze[ rowIndex][ columnIndex ] = 'o';      
      }
      
      // up
      if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
      {
        up = markCorrectPathAndCountSteps( rowIndex - 1, columnIndex, counter );
      }
      
      // down
      if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
      {
        down = markCorrectPathAndCountSteps( rowIndex + 1, columnIndex, counter );
      }
      
      // left
      if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
      {    
        left = markCorrectPathAndCountSteps( rowIndex, columnIndex - 1, counter );  
      }
      
      // right
      if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
      {
        right = markCorrectPathAndCountSteps( rowIndex, columnIndex + 1, counter );
      }
      
      if ( up || down || left || right
        && maze[ rowIndex][ columnIndex ] != 'S' )
      {
        maze[ rowIndex][ columnIndex ] = '*';
        return true;
      }
      else
      {
        if ( rowIndex != startRow || columnIndex != startColumn )
        {
          if ( maze[ rowIndex][ columnIndex ] != 'S' ) {          
            maze[ rowIndex][ columnIndex ] = '.';
            return false;
          }
        }        
      }
     
      if ( maze[ rowIndex][ columnIndex ] != 'S' ) {
        return false;        
      }
      
      return true;
   }
      
   public char[][] getMaze()
   {
      return maze;
   }

   public void display()
   {
      if ( maze==null )
      {
        return;
      }
      
      for ( int a = 0; a < maze.length; a++ )
      {
        for ( int b = 0; b<maze[0].length; b++ )
        {
          char character = maze[ a ][ b ];
          if ( character == 0 )
          {
            System.out.print( "-" );
          }
          else
          {  
           System.out.print( character );
          }
        }
        System.out.println("");
      }
      System.out.println("");
   }
   
   public void solve(int n)
   {
      System.out.println( "solve    n: " + n );
      switch(n)
      {
         case 1:
            {
               markAllPaths(startRow, startColumn);
               break;
            }
         case 2:
            {
               int count = markAllPathsAndCountStars(startRow, startColumn);
               System.out.println("Number of steps = " + count);
               break;
            }
         case 3:
            {
               markTheCorrectPath(startRow, startColumn);
               break;
            }
         case 4:         //use mazeNoPath.txt 
            {
               if( !markTheCorrectPath(startRow, startColumn) )
                  System.out.println("No path exists."); 
               break;
            }
         case 5:
            {
              if( ! markCorrectPathAndCountSteps( startRow, startColumn, 0 ) )
              { 
                System.out.println("No path exists."); 
              }
              break;
            }
         default:
            System.out.println("Unsupported option: " + n );   
      }
   }

  //constructors
  public Maze()  //extension:  generate a random maze
  {    
    int rowSize = getRandomSize();
    int columnSize = getRandomSize();
    maze = new char[ rowSize ][ columnSize ];

    // 1. Set START and END on edge
    int startRowIndex = (int)( 1 + Math.random() * ( rowSize - 2 ) );
    int endRowIndex = (int)( 1 + Math.random() * ( rowSize - 2 ) );
    maze[ startRowIndex ][ 0 ] = 'S';
    maze[ endRowIndex ][ columnSize - 1 ] = 'E';
    startRow = startRowIndex;
    startColumn = 0;
    
    // 2. Fill rest of edge with WALL characters
    for ( int i = 0; i < rowSize; i++ )
    {
      for ( int k = 0; k < columnSize; k++ )
      {
        if ( i == 0
          || i == rowSize - 1
          || k == 0
          || k == columnSize - 1
          )
        {
          if ( maze[ i ][ k ] != 'S'
            && maze[ i ][ k ] != 'E' )
          {
            maze[ i ][ k ] = 'W';
          }
        }
      }
    }

    // 3. Fill inner with random characters
    for ( int i = 1; i < rowSize - 1; i++ )
    {
      for ( int k = 1; k < columnSize - 1; k++ )
      {
        maze[ i ][ k ] = getRandomCharacter();
      }
    }
  }
   
  public int getRandomSize()
  {
    int randomSize = 0;
    while ( randomSize < SIZEMINIMUM )
    {
      randomSize = (int)( Math.random() * SIZEMAXIMUM );        
    }
    
    return randomSize;
  }
  
  public char getRandomCharacter ()
  {
    char character = '.';
    double randomNumber = Math.random();
    if ( randomNumber > DOTPROBABILITY )
    {
      character = 'W';
    }
   
    return character; 
  }

  public Maze( char[][] inputMaze )  //copy constructor
  {
    maze = inputMaze;
    
    for ( int row = 0; row < maze.length; row++ )
    {
      for ( int column = 0; column < maze[0].length; column++ )
      { 
        if ( maze[ row ][ column ] == START)      //identify start
        {
          startRow = row;
          startColumn = column;
        }
      }
    }
  } 
  
  public Maze( String filename )    
  {
    this( read( filename + ".txt") );
  }

   /**
    * Reads the contents of the file into a matrix.
    * Uses try-catch.
    * @param filename The string representing the filename.
    * @returns A 2D array of chars.
  */
   public static char[][] read( String filename )
   {
      char[][] twoDArray;
      Scanner infile = null;
      try
      {
        infile = new Scanner( new File( filename ) );
        String string = infile.nextLine();
        String[] matches = string.split(" ");
        int rowNumber = Integer.parseInt( matches[0] );
        int columnNumber = Integer.parseInt( matches[1] );
        twoDArray = new char[rowNumber][columnNumber];
        int rowCounter = 0;
        while ( infile.hasNext() )
        {
          String line = infile.nextLine();
          String[] tokens = line.split( "" );
          
          for ( int i = 0; i < tokens.length; i++ )
          {
            twoDArray[ rowCounter ][ i ] = tokens[ i ].charAt( 0 );
          }
          rowCounter++;
        }
      }
      catch (Exception e)
      {
         System.out.println("File not found    e: " + e);
         return null;
      }
      
      infile.close();
      
      return twoDArray;
  }
}
