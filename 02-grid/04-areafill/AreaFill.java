// Name: Rio Young
// Date: 10/1/18

import java.util.*;
import java.io.*;
 
public class AreaFill
{
  private static char[][] grid = null;
  private static String filename = null;
  private static int counter = 0;

   /**
    * Fills part of the matrix with a different character.
    * @param g A 2D char array.
    * @param r An int representing the row of the cell to be filled.
    * @param c An int representing the column of the cell to be filled.
    * @param ch A char representing the replacement character.
    */
  public static void fill( char[][] grid, int rowIndex, int columnIndex, char character )
  {
    int numberRows = grid.length;
    int numberColumns = grid[0].length;
    
    if ( grid[ rowIndex][ columnIndex ] != character )
    {
      return;
    }
    
    grid[ rowIndex][ columnIndex ] = '*';
    
    // check up
    if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
    {
      fill( grid, rowIndex - 1, columnIndex, character );
    }
    
    // check down
    if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
    {
      fill( grid, rowIndex + 1, columnIndex, character );
    }
    
    // check left
    if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
    {    
      fill( grid, rowIndex, columnIndex - 1, character );  
    }
    
    // check right
    if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
    {
      fill( grid, rowIndex, columnIndex + 1, character );
    }

    return;
  }
   
   /**
    * Fills part of the matrix with a different character.
    * Counts as you go.  Does not use a static variable.
    * @param g A 2D char array.
    * @param r An int representing the row of the cell to be filled.
    * @param c An int representing the column of the cell to be filled.
    * @param ch A char representing the replacement character.
    * @return An int representing the number of characters that were replaced.
    */
  public static int fillAndCount( char[][] grid, int rowIndex, int columnIndex, char character )
  {
    int numberRows = grid.length;
    int numberColumns = grid[0].length;
    if ( grid[ rowIndex][ columnIndex ] != character )
    {
      return counter;
    }
      
    counter++;
    grid[ rowIndex][ columnIndex ] = '*';
    
    // check up
    if ( rowIndex - 1 >= 0 && rowIndex < numberRows )
    {
      fillAndCount( grid, rowIndex - 1, columnIndex, character );
    }
    
    // check down
    if ( rowIndex + 1 >= 0 && rowIndex + 1 < numberRows )
    {
      fillAndCount( grid, rowIndex + 1, columnIndex, character );
    }
    
    // check left
    if ( columnIndex - 1 >= 0 && columnIndex < numberColumns )
    {    
      fillAndCount( grid, rowIndex, columnIndex - 1, character );  
    }
    
    // check right
    if ( columnIndex + 1 >= 0 && columnIndex + 1 < numberColumns )
    {
      fillAndCount( grid, rowIndex, columnIndex + 1, character );
    }

    return counter;
  }
   
   
   /**
    * Reads the contents of the file into a matrix.
    * Uses try-catch.
    * @param filename The string representing the filename.
    * @returns A 2D array of chars.
  */
   public static char[][] read( String filename )
   {
      char[][] twoDArray;
      Scanner infile = null;
      try
      {
        infile = new Scanner(new File(filename));
        String string = infile.nextLine();
        String[] matches = string.split(" ");
        int rowNumber = Integer.parseInt( matches[0] );
        int columnNumber = Integer.parseInt( matches[1] );
        twoDArray = new char[rowNumber][columnNumber];
        int rowCounter = 0;
        while ( infile.hasNext() )
        {
          String line = infile.nextLine();
          String[] tokens = line.split( "" );
          
          for ( int i = 0; i < tokens.length; i++ )
          {
            twoDArray[ rowCounter ][ i ] = tokens[ i ].charAt( 0 );
          }
          rowCounter++;
        }
      }
      catch (Exception e)
      {
         System.out.println("File not found");
         return null;
      }
      
      infile.close();
      
      return twoDArray;
  }
   
   /**
    * @param g A 2-D array of chars.
    * @returns A string representing the 2D array.
    */
  public static String display( char[][] twoDArray )
  {
    String output = "\n";
    for ( int i=0; i < twoDArray.length; i++ )
    {
      for ( int j=0; j < twoDArray[i].length; j++ )
      {
        output += twoDArray[ i ][ j ];
      }
      output += "\n";
    }
    
    return output;
  }
  
  public static void main(String[] args) 
  {
    Scanner scanner = new Scanner(System.in);
    while(true)  // what does this do?
    {
      counter = 0;
      System.out.print("Fill the Area of (-1 to exit): ");
      filename = scanner.next();
      if(filename.equals("-1"))
      {
         scanner.close();
         System.out.println("Good-bye");
         System.exit(0);   
      }
      grid = read(filename);
      String theGrid = display(grid);
      System.out.println( theGrid );
      System.out.print( "1-Fill or 2-Fill-and-Count: ");
      int choice = scanner.nextInt();
      switch(choice)
      {
        case 1:
        {
          System.out.print("\nEnter ROW COL to fill from: ");
          int row = scanner.nextInt();
          int column = scanner.nextInt();
          if ( Integer.valueOf( row ) == null || Integer.valueOf( column ) == null )
          {
             break;
          }
          fill( grid, row, column, grid[ row ][ column ] );
          System.out.println( display(grid) );
          break;
        }
        case 2:
        {
          System.out.print("\nEnter ROW COL to fill from: ");
          int row = scanner.nextInt();
          int column = scanner.nextInt();
          int count = fillAndCount( grid, row, column, grid[row][column] );
          System.out.println( display(grid) );
          System.out.println("count = " + count);
          System.out.println();
          break;
        }
        default:
           System.out.print("\nTry again! ");
      } // while
    }
  }
 

}