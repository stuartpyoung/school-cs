import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class TestJunit {
 
 @Test
   public static void testGetExpressionValue() {

      String[][] testList =
      {
        //{ "5 * 4 + 3", "23" },
        //{ "3 + 4 * 5", "23" },
        //{ "4*5+3", "23" },
        //{ "3 + 4 * 5 + 2", "25" },
        //{ "5*(3+4)", "35" },
        //{ "5*((3+4)-2)", "25" },

        { "3+4*5", "23" },
        { "(3+4)*5", "35" },
        { "(4+5)-5*3", "-6" },
        { "(3+4)*(5+6)", "77" },
        { "(3*(4+5)-2)/5", "5" },
        { "2*3*4-9/3", "21" },

        //{ "6*8/6", "6" },
        //{ "2-3+5", "-6" },
        //{ "3*4/6", "0" },
        //{ "3+4-5+2*2", "6" },
      };

      for ( int i = 0; i < testList.length; i++ )
      {
         String input = testList[ i ][ 0 ];
         int expected = Integer.parseInt( testList[ i ][ 1 ] );
         System.out.println("input: " + input);
         //System.out.println("expected: " + expected);
         //
         Evaluator evaluator = new Evaluator( input );
         // DO TEST
         int actual = evaluator.getExpressionValue();
         //System.out.println("actual: " + actual);
         //System.out.println("expected: " + expected);
         //
         assertEquals( expected, actual );
         System.out.println( "TESTS SUCCESSFUL: "  + input + " = " + actual );
      }    

      System.out.println("ALL TESTS COMPLETED SUCCESSFULLY.");

   }
      
   public static void main(String[] args)
   {
     testGetExpressionValue();
   }
   

}