// Name: Rio Young
// Date: 9/25/18
  
import java.util.*;
public class Permutations
{
   public static int count = 0;
    
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("\nHow many digits? ");
      int n = sc.nextInt();
      
     //leftRight("", n);          
      //oddDigits("", n);
      superprime(n);
      if ( count == 0 )  
      {
         System.out.println( "there are no " + n + "-digit superprimes" );
      }
      else
      {
          System.out.println( "Count is " + count );
      }
   }
   
    /**
     * Builds all the permutations of a string of length n containing Ls and Rs
     * @param s A string 
     * @param n An postive int representing the length of the string
     */
   public static void leftRight(String leftRightString, int n)
   {
      if ( leftRightString.length() == n )
      {
         System.out.println( leftRightString );
      }
      else
      {
         leftRight( leftRightString + "L", n );
         leftRight( leftRightString + "R", n );
      }
   }
   
    /**
     * Builds all the permutations of a string of length n containing odd digits
     * @param s A string 
     * @param n A postive int representing the length of the string
     */
   public static void oddDigits(String oddDigitString, int n)
   {
      if ( oddDigitString.length() == n )
      {
         System.out.println( oddDigitString );
      }
      else
      {
         int[] oddNumbers = { 1, 3, 5, 7, 9 };
         for ( int i = 0; i < oddNumbers.length; i++ )
         {
            oddDigits( oddDigitString + oddNumbers[i], n );
         }
      }
   }
      
    /**
     * Builds all combinations of a n-digit number whose value is a superprime
     * @param n A positive int representing the desired length of superprimes  
     */
   public static void superprime(int n)
   {
      recur(2, n); //try leading 2, 3, 5, 7, i.e. all the single-digit primes
      recur(3, n); 
      recur(5, n);
      recur(7, n);
   }

    /**
     * Recursive helper method for superprime
     * @param k The possible superprime
     * @param n A positive int representing the desired length of superprimes
     */
   private static void recur(int k, int n)
   {
      if ( isPrime( k ) )
      {
         if ( ( Integer.toString( k ) ).length() == n )
         {
            System.out.println( k );
            count++;
         }
         else
         {
            int[] oddNumbers = { 1, 3, 5, 7, 9 };
            for ( int i = 0; i < oddNumbers.length; i++ )
            {
                recur( ( k * 10 ) + oddNumbers[i], n );
            }
         }
      }
   }

    /**
     * Determines if the parameter is a prime number.
     * @param n An int.
     * @return true if prime, false otherwise.
     */
   public static boolean isPrime( int possiblePrime )
   {
      for ( int i = 2; i <= possiblePrime / i; i++ )
        {
            if ( possiblePrime % i == 0 )
            {
                return false;
            }
        }
        
        return possiblePrime > 1;
   }
}
