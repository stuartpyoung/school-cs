// Name: Shaw Young
// Date: 10/15/18
//
// 
//
// License Information:
//   This class is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation.
//
//   This class is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

import edu.kzoo.grid.BoundedGrid;
import edu.kzoo.grid.Grid;
import edu.kzoo.grid.Location;
import edu.kzoo.grid.display.GridDisplay;
import edu.kzoo.grid.GridObject;
import java.lang.Math;
import java.io.*;
import java.util.*;

/**
 *  Environment-Based Applications:<br>
 *
 *    The NQueens class implements the N Queens problem.
 *
 *  @author Your Name (based on a template provided by Alyce Brady)
 *  @version 1 September 2002
 **/

public class NQueens
{
    // Instance Variables: Encapsulated data for EACH NQueens problem
    private Grid board;
    private GridDisplay display;
    private int columnStart = 0;
    
    /** Attempts to place the <code>q</code>th queen on the board.
     *  (Precondition: <code>0 <= q < numQueens()</code>)
     *    @param q index of next queen to place
     **/
    private boolean placeQueen( int queenIndex ) 
    {    
      int rowIndex = queenIndex;
      int columnIndex = columnStart;
      Location location = new Location( rowIndex, columnIndex );
      if ( queenIndex >= numQueens() ) {
        return true;
      }
      
      if ( columnIndex >= ( numQueens() - 1 ) ) {
        return false;
      }
      
      while ( locationIsOK( location ) == false ) {
        columnIndex++;
        
        if ( columnIndex >= numQueens() ) {
          return false;
        }
        location = new Location( queenIndex, columnIndex );
      }
      
      columnStart = 0;
      new Queen( board, location );
      display.showGrid();
      
      if ( placeQueen( queenIndex + 1 ) ) {
        return true;
      }
      else {
        removeQueen( location );
        display.showGrid();               
        columnStart = columnIndex + 1;
 
        return placeQueen( queenIndex );
      }
    }

    /** Determines whether a queen can be placed at the specified
     *  location.
     *    @param loc  the location to test
     **/
    private boolean locationIsOK(Location location)
    {
      // Verify no queens in same column
    	for ( int i = 0; i < location.row(); i++ ) {
        GridObject found = board.objectAt( new Location( i, location.col() ) );
        if (  found != null ) {
          return false;
        }
    }
      
      // Verify no queens on diagonal
      for ( int i = 0; i < numQueens(); i++ ) {
      	for ( int k = 0; k < numQueens(); k++ ) {
          GridObject found = board.objectAt( new Location( i, k ) );
          if ( found != null ) {
            int xDifference = Math.abs( i - location.row() );
            int yDifference = Math.abs( k - location.col() );            
            if ( ( Double.valueOf( xDifference ) / Double.valueOf( yDifference ) ) == 1 )
            {
              return false;
            }
          }
        }
      }
      
      return true;
    }

    /** Adds a queen to the specified location.
     *    @param loc  the location where the queen should be placed
     **/
    private void addQueen(Location loc)
    {
        new Queen(board, loc);      // queens add themselves to the board
    }

    /** Removes a queen from the specified location.
     *    @param loc  the location where the queen should be removed
     **/
    private void removeQueen(Location loc)
    {
        board.remove(loc);
    }
    
    // constructor
    /** Constructs an object that solves the N Queens Problem.
     *    @param n the number of queens to be placed on an
     *              <code>n</code> x <code>n</code> board
     *    @param d an object that knows how to display an 
     *              <code>n</code> x <code>n</code> board and
     *              the queens on it
     **/
    public NQueens(int n, GridDisplay d)
    {
        //board = new BoundedGrid(n, n);
        display = d;
        //display.setGrid(board);
        //display.showGrid();
    }

  // methods

    /** Returns the number of queens to be placed on the board. **/
    public int numQueens()
    {
    	return board.numRows();   // replace this with something more useful
    }

    /** Solves (or attempts to solve) the N Queens Problem. **/
    public boolean solve()
    {
        Scanner scanner = new Scanner(System.in);
        while ( true )
        {
            int size = 0;
            System.out.print("Input size of board (0 to exit): ");
            size = scanner.nextInt();
            if ( size == 0  )
            {
                scanner.close();
                System.out.println("Exiting. Goodbye");
                System.exit(0);   
            }
            else
            {
                System.out.println( "size: " + size );
                
                board = new BoundedGrid( size , size );
                display.setGrid( board );
                display.showGrid();

                if ( ! placeQueen( 0 ) )
                {
                    System.out.println("Solution not found");
                }
                else
                {
                    display.showGrid();
                }
            }
        }
   }

}
