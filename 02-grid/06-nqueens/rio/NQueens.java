//Name: Rio Young
//Date: 10/4/18
//
// 
//
// License Information:
//   This class is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation.
//
//   This class is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

import edu.kzoo.grid.BoundedGrid;
import edu.kzoo.grid.Grid;
import edu.kzoo.grid.GridObject;
import edu.kzoo.grid.Location;
import edu.kzoo.grid.display.GridDisplay;
import java.lang.Math;
import java.util.*;
import java.io.*;


/**
 *  Environment-Based Applications:<br>
 *
 *    The NQueens class implements the N Queens problem.
 *
 *  @author Your Name (based on a template provided by Alyce Brady)
 *  @version 1 September 2002
 **/

public class NQueens
{
    // Instance Variables: Encapsulated data for EACH NQueens problem
    private Grid board;
    private GridDisplay display;
    private Location[] addedQueens;
    private int currentColumn = 0;
 
    /** Attempts to place the <code>q</code>th queen on the board. q-->queenNumber
     *  (Precondition: <code>0 <= queenNumber < numQueens()</code>)
     *    @param queenNumber index of next queen to place
     **/
    private boolean placeQueen( int queenNumber )
    {
        //System.out.println( "\n\nplaceQueen " + queenNumber + "    PLACING QUEEN" );
        if ( queenNumber >= numQueens() ) //finished
        {
            return true;
        }
        else
        {
            //System.out.println( "placeQueen " + queenNumber + "    CURRENT COLUMN: " + currentColumn );
            if ( currentColumn >= numQueens() )
            {
                return false;
            }
            int rowNumber = queenNumber;
            int columnNumber = currentColumn;
            
            Location location = new Location( queenNumber, columnNumber );
            if ( numQueens() >= 0 )
            {
                if ( queenNumber > 0 )
                {
                    while ( locationIsOK( location ) == false
                           && columnNumber < ( numQueens() ) )
                    {
                        columnNumber++;
                        if ( columnNumber >= numQueens() )
                        {
                            //System.out.println( "placeQueen " + queenNumber + "    Solution not found on this line" );
                            return false;
                        }
                        ////System.out.println( "placeQueen " + queenNumber + "    INCREMENTED columnNumber: " + columnNumber );
                        location = new Location( queenNumber, columnNumber );
                                
                        //   columnNumber = 0;
                    }
                }
            }
            
            //System.out.println( "placeQueen " + queenNumber + "    PLACED QUEEN AT location: " + location );
            
            
            new Queen( board, location );
            addedQueens[ queenNumber ] = location;
            display.showGrid();
            
            //System.out.println( "placeQueen " + queenNumber + "    RECURSIVE CALL TO placeQueen( " + (queenNumber + 1) + " )" );

            currentColumn = 0;
            boolean placed = placeQueen( queenNumber + 1 );

            //System.out.println( "placeQueen " + queenNumber + "    PLACED: " + placed );
            if ( placed == true )
            {
                //System.out.println( "placeQueen " + queenNumber + "    RETURNING true" );
               return true;
            }
            else
            {
               removeQueen( location );
               addedQueens[ queenNumber ] = null;
               display.showGrid();               
               currentColumn = columnNumber + 1;
                //System.out.println( "placeQueen " + queenNumber + "    RETURNING placeQueen( " + queenNumber + " )" );

               return placeQueen( queenNumber );
            }     
        }
    }

    /** Determines whether a queen can be placed at the specified
     *  location.
     *    @param loc  the location to test
     **/
    private boolean locationIsOK( Location location )
    {
        // Verify that another queen can't attack this location.
        // (Only queens in previous rows have been placed.)
        
        int currentRow = location.row();
        int currentColumn = location.col();

        ////System.out.println( "locationIsOK    (" + currentRow + ", "  + currentColumn + ")" );
        
        if ( queenInColumn( location ) == true ) {
         //   //System.out.println( "locationIsOK    (" + currentRow + ", " + currentColumn + ")     RETURNING false BECAUSE QUEEN IN COLUMN" );
            return false;
        }
        
        if ( onDiagonal( location ) == true ) {
            //System.out.println( "locationIsOK    (" + currentRow + ", " + currentColumn + ")     RETURNING false BECAUSE ON DIAGONAL" );
            return false;
        }

        //System.out.println( "locationIsOK    (" + currentRow + ", " + currentColumn + ")     RETURNING true AT LOCATION: (" + currentRow + ", " + currentColumn + ")" );
        return true;
    }

    private boolean onDiagonal( Location location )
    {
        // Return false if any queens have been placed in the same column
        // as the location parameter        
        int currentRow = location.row();
        int currentColumn = location.col();
        //System.out.println( "onDiagonal    location: (" + currentRow + ", " + currentColumn + ")" );
        ////System.out.println( "ADDED QUEENS" );
        //for ( int i = 0; i < addedQueens.length; i++ )
        //{
        //    if ( addedQueens[i] == null )
        //    {
        //        break;
        //    }
        //    //System.out.print( addedQueens[ i ].toString() + " " );   
        //}
        ////System.out.println(  );
        
        for ( int i = 0; i < addedQueens.length; i++ )
        {
            Location addedQueen = addedQueens[ i ];
            
            if ( addedQueen == null )
            {
                //System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")     addedQueens[" + i + "]  IS NULL. RETURNING false" );
                return false;
            }
            
           // //System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")     addedQueens[" + i + "]: " + addedQueen );

            int queenRow = addedQueen.row();
            int queenColumn = addedQueen.col();
            ////System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")     queenRow : " + queenRow + ", queenColumn: " + queenColumn );
            //    
            int rowDifference = Math.abs( currentRow - queenRow );
            ////System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")     rowDifference: " + rowDifference );
            int columnDifference = Math.abs( currentColumn - queenColumn );
            ////System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")     columnDifference: " + columnDifference );
            //System.out.println( "onDiagonal    " + rowDifference + " / " + columnDifference + ": " + ( Double.valueOf( rowDifference ) / Double.valueOf( columnDifference ) ) );
            
            if ( ( Double.valueOf( rowDifference ) / Double.valueOf( columnDifference ) ) == 1 )
            {
                //System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")    RETURNING true" );
        
                return true;
            }
        }

        //System.out.println( "onDiagonal    (" + currentRow + ", " + currentColumn + ")    RETURNING false" );
        return false;
    }

    private boolean queenInColumn( Location location )
    {
        // Return false if any queens have been placed in the same column
        // as the location parameter        
        int currentRow = location.row();
        int currentColumn = location.col();
        ////System.out.println( "queenInColumn(" + currentRow + ", " + currentColumn + ")    numQueens: " + numQueens() );
        
        for ( int i = 0; i < numQueens(); i++ )
        {
         //   //System.out.println( "queenInColumn(" + currentRow + ", " + currentColumn + ")    i: " + i );

            if ( i != currentRow ) {
                Location newLocation = new Location( i, currentColumn );
                GridObject objectFound = board.objectAt( newLocation );
                
        //        //System.out.println( "queenInColumn(" + currentRow + ", " + currentColumn + ")    objectFound(" + i + ", " + currentColumn + "): " + objectFound );
                
                if ( objectFound != null )
                {
                    //System.out.println( "queenInColumn(" + currentRow + ", " + currentColumn + ")     RETURNING true AT LOCATION: (" + i + ", " + currentColumn + ")" );
                    return true;
                }
                else  {
                    
                }
            }
        }

        //System.out.println( "queenInColumn(" + currentRow + ", " + currentColumn + ")    RETURNING false" );
        return false;
    }
    

  // constructor

    /** Constructs an object that solves the N Queens Problem.
     *    @param n the number of queens to be placed on an
     *              <code>n</code> x <code>n</code> board
     *    @param d an object that knows how to display an 
     *              <code>n</code> x <code>n</code> board and
     *              the queens on it
     **/
    public NQueens( int n, GridDisplay d )
    {
        board = new BoundedGrid( n, n );
        addedQueens = new Location[ n ];
        display = d;
        display.setGrid( board );
        display.showGrid();
    }

  // methods

    /** Solves (or attempts to solve) the N Queens Problem. **/
    public boolean solve()
    {
       // Location location = new Location( 2, 3 );
       //addQueen( location );
       // display.showGrid();
       // removeQueen( location );
       //   display.showGrid();

        Scanner scanner = new Scanner(System.in);
        while ( true )
        {
            String boardSize = "";
            System.out.print("Input size of board (-1 to exit): ");
            boardSize = scanner.next();
            if ( boardSize.equals("-1") )
            {
                scanner.close();
                System.out.println("Good-bye");
                System.exit(0);   
            }
            else
            {
                System.out.println( "boardSize: " + boardSize );
                
                board = new BoundedGrid( Integer.parseInt( boardSize) , Integer.parseInt( boardSize ) );
                addedQueens = new Location[ Integer.parseInt( boardSize ) ];
                display.setGrid( board );
                display.showGrid();

                if ( placeQueen( 0 ) )
                {
                    display.showGrid();
                    //return true;
                }
                else
                {
                    System.out.println("Solution not found");
                    //return false;
                }
                
            }
        }
       
    }

    /** Adds a queen to the specified location.
     *    @param loc  the location where the queen should be placed
     **/
    private void addQueen( Location location )
    {
        new Queen( board, location );      // queens add themselves to the board
    }

    /** Removes a queen from the specified location.
     *    @param loc  the location where the queen should be removed
     **/
    private void removeQueen( Location location )
    {
        board.remove( location );
    }

    /** Returns the number of queens to be placed on the board. **/
    public int numQueens()
    {
        return board.numRows();
    }

}
