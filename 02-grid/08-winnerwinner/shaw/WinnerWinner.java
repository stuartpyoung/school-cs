// Name: Shaw Young
// Date: 10-22-18

import java.util.*;

public class WinnerWinner
{
   public static void main(String[] args)
   {
      Board b = null;
      b = new Board( 3, 4, "W--S-------X-"); 
      b.display();
      //System.out.println( b.check(1, 0) );
     System.out.println("Shortest path is " + b.win());  //2
     
     b = new Board(4,3,"S-W-----X-W-"); 
     b.display();
    System.out.println("Shortest path is " + b.win());  //4
     
     b = new Board(3,4,"X-WS--W-W---"); 
     b.display();
     System.out.println("Shortest path is " + b.win());  //7
     
     b = new Board(3,5,"W--WW-X----SWWW"); 
     b.display();
     System.out.println("Shortest path is " + b.win());  //1
     
     b = new Board(3,3,"-SW-W-W-X");     //no path exists
     b.display();
     System.out.println("Shortest path is " + b.win());  //-1
     
     b = new Board(5,7,"-W------W-W-WX--S----W----W-W--W---");     //Example Board 1
     b.display();
     System.out.println("Shortest path is " + b.win());  //5
     
     b = new Board(4,4,"-WX--W-W-WW-S---");     //Example Board -1
     b.display();
     System.out.println("Shortest path is " + b.win());  //5
//  
//      //what other test cases should you test?
      
   }
}

class Board
{
   private char[][] board;  
   private int maxPath;
   private int BIGNUMBER;

   public Board(int rows, int cols, String line)  
   {
      maxPath = rows*cols;
      
      board = new char[rows][cols];
      
      for ( int r = 0; r < rows ; r++ ) {
    	  for ( int c = 0; c < cols; c++ ) {
    		  board[r][c] = line.charAt( c );
    	  }
        line = line.substring(cols);
      } 
      
//      MAXSTEPS = board.length * board[0].length;
   }

	/**	returns the length of the longest possible path in the Board   */
   public int getMaxPath()		
   {  
      return maxPath; 
   }	
   
   public void display()
   {
	   if(board==null) 
         return;
      System.out.println();
      for(int a = 0; a < board.length; a++)
      {
         for(int b = 0; b < board[0].length; b++)
         {
            System.out.print(board[a][b]);
         }
         System.out.println();
      }
   }
      /**	
    *  precondition:  S and X exist in board
    *  postcondition:  returns either the length of the path
    *                  from S to X, or -1, if no path exists.    
    */
   public int win()
   {
	  int rowOfS = 0;
      int columnOfS = 0;
      for ( int r= 0; r < board.length; r++ ) {
         for ( int c = 0; c < board[0].length; c++ ) {
            if ( board[r][c] == 'S' ) {
               rowOfS = r;
               columnOfS = c;
            }
         }
      }
      if ( check( rowOfS, columnOfS) == ( board.length*board[0].length + 1) ) {
    	  return -1;
      }
      else {
    	  return check( rowOfS, columnOfS);
      }

	
   }
   
   /**	
    *  calculates and returns the shortest path from S to X, if it exists   
    *  @param r is the row of "S"
    *  @param c is the column of "S"
    */
   public int check(int r, int c)
   {	
	      if(r < 0 || r >= board.length || c < 0 || c >= board[0].length)
            return getMaxPath();
         if(board[r][c] == 'W')
            return getMaxPath();
         if(board[r][c] == 'V') 
	         return getMaxPath();
         if(board[r][c] == 'X') {
	         return 0;
         }
         
         board[r][c] = 'V';
         
         int left = 1 + check( r, c - 1);
         int right = 1 + check( r, c + 1);
         int up = 1 + check( r - 1, c);
         int down = 1 + check( r + 1 , c);
         board[r][c] = '-';
         
         return Math.min(up, Math.min(down, Math.min(left, right)));
            // 
// 	      int a = maxPath, b = maxPath, e = maxPath, d = maxPath;
// //	      int a, b, e, d;
// 
// 	      
//          
// 	      if(board[r][c] == 'o') {
// 	         return -1;
//          }
//          
//          if( ( r - 1 ) >= 0){
// 	         if(board[r-1][c] == 'X') {
// 	            return 1;
//             }
// 	         if(board[r-1][c] == '-'){
// 	            board[r-1][c] = 'o';
// 	            a = 1 + check(r-1, c);
// 	            board[r-1][c] = '-';   
// 	         }    
// 	      }
// 
// 	      if( ( r + 1 ) < ( board.length ) ){
// 	         if(board[r+1][c] == 'X') {
// 	            return 1;
//             }
// 	         if(board[r+1][c] == '-'){
// 	            board[r+1][c] = 'o';
// 	            b = 1 + check(r+1, c);
// 	            board[r+1][c] = '-'; 
// 	         } 
// 	      }
// 
// 	      if(c - 1 >= 0){
// 	         if(board[r][c-1] == 'X') {
// 	            return 1;
//             }
// 	         if(board[r][c-1] == '-'){
// 	            board[r][c-1] = 'o';
// 	            e = 1 + check(r, c-1);
// 	            board[r][c-1] = '-';   
// 	         }    
// 	      }
// 
// 	      if( (c + 1 ) < ( board[0].length ) ){
// 	         if(board[r][c+1] == 'X') { 
// 	            return 1;
//             }
// 	         if(board[r][c+1] == '-'){
// 	            board[r][c+1] = 'o';
// 	            d = 1 + check(r, c+1);
// 	            board[r][c+1] = '-'; 
// 	         } 
// 	      }
//          
// 	      return Math.min(Math.min(a, b), Math.min(e, d));
	   
   }
}
          

  




/************************ output ************
 W-S-
 ----
 --X-
 Shortest path is 2
 
 S-W
 ---
 --X
 -W-
 Shortest path is 4
 
 X-WS
 --W-
 W---
 Shortest path is 7
 
 W--WW
 -X---
 -SWWW
 Shortest path is 1
 
 -SW
 -W-
 W-X
 Shortest path is -1
 
 -W-----
 -W-W-WX
 --S----
 W----W-
 W--W---
 Shortest path is 5
 
 -WX-
 -W-W
 -WW-
 S---
 Shortest path is -1 
 ***************************************/