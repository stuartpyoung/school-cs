// Name: Shaw Young   Date: 12/17/18

import java.io.*;
import java.util.*;

/**
 * Program takes a text file then creates an index (by line numbers)
 * for all the words in the file.
 * Writes the index into output file.
 * Program prompts user for file names.
 */  
public class IndexMaker
{
   public static void main(String[] args) throws IOException
   {
      Scanner keyboard = new Scanner(System.in);
      System.out.print("\nEnter input file name: ");
      String inFileName = keyboard.nextLine().trim();
      Scanner inputFile = new Scanner(new File(inFileName));
      String outFileName = "fishIndex.txt";
      PrintWriter outputFile = new PrintWriter(new FileWriter(outFileName));
      indexDocument(inputFile, outputFile);
      inputFile.close(); 						
      outputFile.close();
      System.out.println("Done.");
   }
   
   public static void indexDocument(Scanner inputFile, PrintWriter outputFile)
   {
      DocumentIndex index = new DocumentIndex();
      String line = null;
      int lineNum = 0;
      while(inputFile.hasNextLine())
      {
         lineNum++;
         index.addAllWords(inputFile.nextLine(), lineNum);
      }
      String output = index.toString();
      outputFile.println( output );      
   }   
}

class DocumentIndex extends ArrayList<IndexEntry>
{
    // fields
    ArrayList<IndexEntry> lineIndex;
    
   //constructors
   public DocumentIndex() {
     lineIndex = new ArrayList<IndexEntry>();
   }

   /**
    * Calls foundOrInserted, which returns an index.
    * At that position, updates that IndexEntry's 
    * list of line numbers with num.   
    */
   public void addWord( String word, int number ) {
      int index = foundOrInserted( word );
      IndexEntry entry = lineIndex.get( index );
      entry.add( number );
   }
      
   /**
    * Extracts all words from str, skipping 
    * punctuation and whitespace.
    * For each word calls addWord(word, num).
    * Use split with punct = ",./;:'\"?<>[]{}|`~!@#$%^&*()" 
    */
   public void addAllWords( String string, int number ) 
   {
      string = string.replaceAll( "^\\W+", "" );
      if ( string.equals( "" ) ) {
         return;
      }
      else {
        String[] tokens = string.split( "[,./;:'\"?<>\\[\\]{}|`~!@#$%^&*()\\s]+" );      
        for ( int i = 0; i < tokens.length; i++ ) {
           addWord( tokens[ i ], number );
        }
      }
   }
      
   /** 
    * Traverses this DocumentIndex comparing word to the words in the 
    * IndexEntry objects in list, looking for correct position of word. 
    * If an IndexEntry with word is not already in that position, the 
    * method creates and inserts a new IndexEntry at that position. 
    * @return position of either the found or the inserted IndexEntry.
    */
   private int foundOrInserted(String word) {
      int counter = 0;
      for ( IndexEntry entry : lineIndex ) {
         if ( word.toUpperCase().equals( entry.getWord() ) ) {
            return counter;
         }
         counter++;  
      }
         
      lineIndex.add( new IndexEntry( word ) );
      
      return lineIndex.size() - 1;
   }

   public String toString() {
      ArrayList<IndexEntry> outputList = new ArrayList<IndexEntry>();
      Collections.sort( lineIndex );
      String outputString = "";
      for ( IndexEntry currentEntry : lineIndex ) {
         outputString += currentEntry.toString() + "\n";
      }

      return outputString;
   }
}
   
class IndexEntry implements Comparable<IndexEntry>
{
   //fields
   private String word;
   private ArrayList<Integer> numberList;
   
   //constructors
   public IndexEntry( String input ) {
      word = input.toUpperCase();
      numberList = new ArrayList<Integer>();
   }
   
   //other methods
   public int compareTo( IndexEntry other ) {
      return word.compareTo( other.word );
   }
   
   /** 
    * Appends num to numList but only if not already in list. 
    */
   public void add( int number ) {
      numberList.add( number );  
   }
      
   public String getWord() {
      return word;
   }
      
   public String toString() {
      String outputString = word + " ";
      for ( int number : numberList ) {
         outputString += number + " ";
      }
      
      return outputString;
   }
}

