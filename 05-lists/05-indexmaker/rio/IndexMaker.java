// Name: Rio Young
// Date: 12/11/18

import java.io.*;
import java.util.*;

/**
 * Program takes a text file then creates an index (by line numbers)
 * for all the words in the file.
 * Writes the index into output file.
 * Program prompts user for file names.
 */  
public class IndexMaker
{
   public static void main(String[] args) throws IOException
   {
      Scanner keyboard = new Scanner(System.in);
      System.out.print("\nEnter input file name: ");
      String inFileName = keyboard.nextLine().trim();
      //String inFileName = "fish3-short.txt";
      //String inFileName = "fish3.txt";
      Scanner inputFile = new Scanner(new File(inFileName));
      String outFileName = "fishIndex.txt";
      PrintWriter outputFile = new PrintWriter(new FileWriter(outFileName));
      indexDocument(inputFile, outputFile);
      inputFile.close(); 						
      outputFile.close();
      System.out.println("Done.");
   }
   
   public static void indexDocument(Scanner inputFile, PrintWriter outputFile)
   {
      DocumentIndex index = new DocumentIndex();
      //System.out.println( "indexDocument    index.entryList: " + index.entryList );
      String line = null;
      int lineNum = 0;
      while(inputFile.hasNextLine())
      {
         lineNum++;
         index.addAllWords(inputFile.nextLine(), lineNum);
      }
      String output = index.toString();
      //System.out.println( "indexDocument    output: " + output );
      outputFile.println( output );      
   }   
}

class DocumentIndex extends ArrayList<IndexEntry>
{
    // fields
    ArrayList<IndexEntry> entryList;
    
   //constructors
   public DocumentIndex()
   {
     entryList = new ArrayList<IndexEntry>();
     //System.out.println( "DocumentIndex::constructor()    entryList: " + entryList );
   }

   /**
    * Calls foundOrInserted, which returns an index.
    * At that position, updates that IndexEntry's 
    * list of line numbers with num.   
    */
   public void addWord( String word, int number )
   {
      //System.out.println( "addWord     number: " + number + ", word: " + word );
      int index = foundOrInserted( word );
      //System.out.println( "addWord index: " + index );
      
      // ADD LINE NUMBER TO indexEntry AT POSITION index IN entryList
      IndexEntry entry = entryList.get( index );
      entry.add( number );
   }
      
   /**
    * Extracts all words from str, skipping 
    * punctuation and whitespace.
    * For each word calls addWord(word, num).
    * Use split with punct = ",./;:'\"?<>[]{}|`~!@#$%^&*()" 
    */
   public void addAllWords( String string, int number ) 
   {
      //System.out.println( "addAllWords      number: " + number + ", string: " + string);
      string = string.replaceAll( "^[^A-Za-z]+", "" );
      if ( string.equals( "" ) )
      {
         return;
      }
      String[] words = string.split( "[,./;:'\"?<>\\[\\]{}|`~!@#$%^&*()\\s]+" );      
      //print( words );
      for ( int i = 0; i < words.length; i++ )
      {
         //System.out.println( "addAllWords     words[" + i + "]: " + words[ i ] );
         addWord( words[ i ], number );
      }
   }
      
   /** 
    * Traverses this DocumentIndex comparing word to the words in the 
    * IndexEntry objects in list, looking for correct position of word. 
    * If an IndexEntry with word is not already in that position, the 
    * method creates and inserts a new IndexEntry at that position. 
    * @return position of either the found or the inserted IndexEntry.
    */
   private int foundOrInserted(String word)
   {
      //System.out.println( "foundOrInserted    word.toUpperCase(): " + word.toUpperCase() );
      //System.out.println( "foundOrInserted    listSize: " + listSize );
      //System.out.print( "foundOrInserted    entryList: " );
      //System.out.println( "foundOrInserted    entryList.length: " + entryList.length );
      
      // return index if found
      int counter = 0;
      for ( IndexEntry entry : entryList )
      {
         if ( entry.getWord().equals( word.toUpperCase() ) )
         {
            //System.out.println( "foundOrInserted    **** MATCHED indexEntry.getWord(): " + indexEntry.getWord() );
            return counter;
         }
         counter++;  
      }
         
      // not found, so inserted
      
      entryList.add( new IndexEntry( word ) );
      
      return entryList.size() - 1;
   }

   public String toString() {
      ArrayList<IndexEntry> outputList = new ArrayList<IndexEntry>();
      String output = "";
      Collections.sort( entryList );
      
      for ( IndexEntry entry : entryList )
      {
         output += entry.toString() + "\n";
      }

      return output;
   }

}
   
class IndexEntry implements Comparable<IndexEntry>
{
   //fields
   private String word;
   private ArrayList<Integer> numberList;
   
   //constructors
   public IndexEntry( String inputWord )
   {
      word = inputWord.toUpperCase();
      numberList = new ArrayList<Integer>();
   }
   
   //other methods
   public int compareTo( IndexEntry other)
   {
      return word.compareTo( other.word );
   }
   
   /** 
    * Appends num to numList but only if not already in list. 
    */
   public void add( int number )
   {
      numberList.add( number );  
   }
      
   public String getWord()
   {
      return word;
   }
      
   public String toString()
   {
      String output = word + " ";
      for ( int number : numberList )
      {
         output += number + " ";
      }
      
      return output;
   }
}

