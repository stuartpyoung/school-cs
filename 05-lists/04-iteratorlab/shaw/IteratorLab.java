// Name: Shaw Young   Date: 12/12/18

import java.io.*;
import java.util.*;

/**
 * Use for-each loops or iterators.
 * Do not use traditional for-loops.
 */  
public class IteratorLab
{
   public static void main(String[] args)
   {
      System.out.println("Iterator Lab");
      int[] rawNumbers = {-9, 4, 2, 5, -10, 6, -4, 24, 20, -28};
      for(int n : rawNumbers )
         System.out.print(n + " ");
      System.out.println();      
      ArrayList<Integer> numbers = createNumbers(rawNumbers);
      System.out.println("ArrayList: "+ numbers);      //Implicit Iterator!
      System.out.println("Count negative numbers: " + countNeg(numbers));
      System.out.println("Average: " + average(numbers));
      System.out.println("Replace negative numbers: " + replaceNeg(numbers));
      System.out.println("Delete zeros: " + deleteZero(numbers));
      String[] rawMovies = {"High_Noon", "High_Noon", "Star_Wars", "Tron", "Mary_Poppins", 
               "Dr_No", "Dr_No", "Mary_Poppins", "High_Noon", "Tron"};
      ArrayList<String> movies = createMovies(rawMovies);
      System.out.println("Movies: " + movies);
      System.out.println("Movies: " +  removeDupes(movies));
   }

   /**
    * @return ArrayList containing all values in the int array
    */     
   public static ArrayList<Integer> createNumbers(int[] rawNumbers) 
   {
      ArrayList<Integer> newList = new ArrayList<Integer>();
      for ( int currentNumber : rawNumbers ) {
         newList.add( currentNumber );
      }
      
      return newList;
   }
 
   /**
    * @return ArrayList containing all values in the String array
    */    
   public static ArrayList<String> createMovies(String[] rawWords) 
   {
        ArrayList<String> newList = new ArrayList<String>();
        for ( String word : rawWords ) {
            newList.add( word );
        }
        
        return newList;
   }

   /**
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return number of negative values in ArrayList a
    */    
   public static int countNeg(ArrayList<Integer> array )
   {
      int count = 0;
      for ( int currentNumber : array ) {
         if ( currentNumber < 0 ) {
            count++;
         }
      }
      
      return count;
   }
  
   /**
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return average of all values in the ArrayList a
    */    
   public static double average( ArrayList<Integer> array )
   {
       double average = 0;
       int totalNumbers = 0;
       for ( int currentNumber : array ) {
         average += currentNumber;
         totalNumbers++;
       }
       average = average / totalNumbers;
       
       return average;    
   }

   /**
    * Changes all negative values in ArrayList to 0.
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return ArrayList with negative values turned to 0 
    */    
   public static ArrayList<Integer> replaceNeg( ArrayList<Integer> array )
   {
      ListIterator<Integer> iterator = array.listIterator( 0 );
      while ( iterator.hasNext() ) {
         Integer currentEntry = iterator.next();
         if ( currentEntry < 0 ) {
            iterator.set( 0 );
         }
      }
      
      return array;
   }
  
   /**
    * Deletes all zero values in ArrayList.
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return ArrayList with no zero values. 
    */   
   public static ArrayList<Integer> deleteZero( ArrayList<Integer> array )
   {
      ListIterator<Integer> iterator = array.listIterator();
      while ( iterator.hasNext() ) {
         Integer currentEntry = iterator.next();
         if ( currentEntry == 0 ) {
            iterator.remove();
         }
      }
      
      return array;
   }

   /**
    * Removes duplicates from list.
    * Precondition:  Arraylist a is not empty; contains String objects
    * @return ArrayList without duplicate movie titles. 
    */   
   public static ArrayList<String> removeDupes( ArrayList<String> array )
   {
      ListIterator<String> iterator = array.listIterator();
      ArrayList<String> uniqueEntries = new ArrayList<String>();
      while ( iterator.hasNext() ) {
         String currentEntry = iterator.next();         
         if ( checkEntry( uniqueEntries, currentEntry ) ) {
            iterator.remove();
         }
         else {
            uniqueEntries.add( currentEntry );
         }
      }
      
      return array;
      
   }
   
   public static boolean checkEntry( ArrayList<String> array, String entry)
   {
      for ( String currentEntry : array ) {
         if ( entry == currentEntry ) {
            return true;
         }
      }
      
      return false;
   }
   
}

