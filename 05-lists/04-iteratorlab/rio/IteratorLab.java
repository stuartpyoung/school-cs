// Name: Rio Young
// Date: 12/17/18

//javac IteratorLab.java ; java IteratorLab
//Iterator Lab
//-9 4 2 5 -10 6 -4 24 20 -28
//ArrayList: [-9, 4, 2, 5, -10, 6, -4, 24, 20, -28]
//Count negative numbers: 4
//Average: 1.0
//Replace negative numbers: [0, 4, 2, 5, 0, 6, 0, 24, 20, 0]
//Delete zeros: [4, 2, 5, 6, 24, 20]
//Movies: [High_Noon, High_Noon, Star_Wars, Tron, Mary_Poppins, Dr_No, Dr_No, Mary_Poppins, High_Noon, Tron]
//Movies: [High_Noon, Star_Wars, Tron, Mary_Poppins, Dr_No]

import java.io.*;
import java.util.*;

/**
 * Use for-each loops or iterators.
 * Do not use traditional for-loops.
 */  
public class IteratorLab
{
   public static void main(String[] args)
   {
      System.out.println("Iterator Lab");
      int[] rawNumbers = {-9, 4, 2, 5, -10, 6, -4, 24, 20, -28};
      for(int n : rawNumbers )
         System.out.print(n + " ");
      System.out.println();      
      ArrayList<Integer> numbers = createNumbers(rawNumbers);
      System.out.println("ArrayList: "+ numbers);      //Implicit Iterator!
      System.out.println("Count negative numbers: " + countNeg(numbers));
      System.out.println("Average: " + average(numbers));
      System.out.println("Replace negative numbers: " + replaceNeg(numbers));
      System.out.println("Delete zeros: " + deleteZero(numbers));
      String[] rawMovies = {"High_Noon", "High_Noon", "Star_Wars", "Tron", "Mary_Poppins", 
               "Dr_No", "Dr_No", "Mary_Poppins", "High_Noon", "Tron"};
      ArrayList<String> movies = createMovies(rawMovies);
      System.out.println("Movies: " + movies);
      System.out.println("Movies: " +  removeDupes(movies));
   }

   /**
    * @return ArrayList containing all values in the int array
    */     
   public static ArrayList<Integer> createNumbers(int[] rawNumbers) 
   {
      ArrayList<Integer> myList = new ArrayList<Integer>();
      for ( int number : rawNumbers )
      {
         myList.add( number );
      }
      
      return myList;
   }
 
   /**
    * @return ArrayList containing all values in the String array
    */    
   public static ArrayList<String> createMovies(String[] rawWords) 
   {
        ArrayList<String> myList = new ArrayList<String>();
        for ( String entry : rawWords )
        {
            myList.add( entry );
        }
        
        return myList;
   }

   /**
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return number of negative values in ArrayList a
    */    
   public static int countNeg(ArrayList<Integer> array )
   {
      int count = 0;
      for ( int number : array )
      {
         if ( number < 0 )
         {
            count++;
         }
      }
      
      return count;
   }
  
   /**
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return average of all values in the ArrayList a
    */    
   public static double average( ArrayList<Integer> array )
   {
       int numberOfNumbers = 0;
       double average = 0;
       for ( int number : array )
       {
         numberOfNumbers++;
         average += number;
       }
       average = average / numberOfNumbers;
       
       return average;    
   }

   /**
    * Changes all negative values in ArrayList to 0.
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return ArrayList with negative values turned to 0 
    */    
   public static ArrayList<Integer> replaceNeg( ArrayList<Integer> array )
   {
      //System.out.println("replaceNeg    array： " + array );
      ListIterator<Integer> iterator = array.listIterator( 0 );
      for ( int entry : array )
      {
         //System.out.println("replaceNeg    entry： " + entry );
         iterator.next();
         if ( entry < 0 )
         {
            iterator.set( 0 );
         }
         //System.out.println("replaceNeg    number：" + entry );
      }
      
      return array;
   }
  
   /**
    * Deletes all zero values in ArrayList.
    * Precondition:  Arraylist a is not empty; contains Integer objects
    * @return ArrayList with no zero values. 
    */   
   public static ArrayList<Integer> deleteZero( ArrayList<Integer> array )
   {
      ListIterator<Integer> iterator = array.listIterator();
      //System.out.println("deleteZero    entry： " + entry );
      //for ( int entry : array )
      while ( iterator.hasNext() )
      {
         Integer entry = iterator.next();
         if ( entry == 0 )
         {
            iterator.remove();
         }
        // System.out.println("deleteZero    number：" + entry );
      }
      
      return array;
   }

   /**
    * Removes duplicates from list.
    * Precondition:  Arraylist a is not empty; contains String objects
    * @return ArrayList without duplicate movie titles. 
    */   
   public static ArrayList<String> removeDupes( ArrayList<String> array )
   {
      ListIterator<String> iterator = array.listIterator();
      ArrayList<String> seenAlready = new ArrayList<String>();
      while ( iterator.hasNext() )
      {
         String entry = iterator.next();
         boolean found = checkEntry( seenAlready, entry );
         if ( found )
         {
            iterator.remove();
         }
         else {
            seenAlready.add( entry );
         }
      }
      
      return array;
      
   }
   
   public static boolean checkEntry( ArrayList<String> array, String entry)
   {
      for ( String checkEntry : array )
      {
         if ( entry == checkEntry )
         {
            return true;
         }
      }
      
      return false;
   }
   
}

