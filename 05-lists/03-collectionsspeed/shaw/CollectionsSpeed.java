// Name: Shaw Young    Date: 01/06/19

import java.util.*;

public class CollectionsSpeed
{
   public static final int MAX = 10000;
   
   public static void main(String[] args)
   {
      ArrayList<Integer> alist = new ArrayList<Integer>();
      LinkedList<Integer> llist = new LinkedList<Integer>();
      makeValues( alist, llist );
      
      System.out.println("get each for ArrayList = " + timeGetEach(alist));
      System.out.println("get each for LinkedList = " + timeGetEach(llist));
      System.out.println("add at 0 for ArrayList = " + timeAddFirst(alist));
      System.out.println("add at 0 for LinkedList = " + timeAddFirst(llist));
      System.out.println("add at list.size() for ArrayList = " + timeAddLast(alist));
      System.out.println("add at list.size() for LinkedList = " + timeAddLast(llist));
      System.out.println("addLast for LinkedList = " + timeAddLastLL(llist));
   }
   
   public static void makeValues(ArrayList<Integer> alist, LinkedList<Integer> llist) {
      for( int i = 0; i < MAX; i++ ) {
         alist.add(i);              		 
         llist.add(i);							
      }
   }
   
   /**
    * Get n items by searching for each one.
    */      
   public static double timeGetEach( List<Integer> list ) {
      double startTime = System.nanoTime();
      for( int i = 0; i < MAX; i++ ) {
         int index = list.get( i );
      }

      return ( System.nanoTime() - startTime ) / 1E6;
   }
   
   /**
    * Add 10000 new objects at position 0.
    */
   public static double timeAddFirst( List<Integer> list ) {
      double startTime = System.nanoTime();
      for ( int i = 0; i < MAX; i++ ) {
         int randomNumber =  (int) Math.round( Math.random() );
         list.add( 0, randomNumber );
      }
      
      return ( System.nanoTime() - startTime ) / 1E6;
   }
   
   /*    
    * Add 10000 new objects at position list.size() 
    */
   public static double timeAddLast( List<Integer> list ) {
      double start = System.nanoTime();
      for ( int i = 0; i < MAX; i++ ) {
         int randomNumber =  (int) Math.round( Math.random() );
         list.add( list.size(), randomNumber );
      }
      
      return (System.nanoTime() - start) / 1E6;
   }
   
   /*    
    * Add 10000 new objects at the end.  
    * Uses the LinkedList method addLast().
    * You must cast List list into a LinkedList. 
    */
   public static double timeAddLastLL( List<Integer> list ) {
      LinkedList<Integer> tempList = new LinkedList<Integer>( list );

      double startTime = System.nanoTime();
      for ( int i = 0; i < MAX; i++ ) {
         int randomNumber = (int) Math.round( Math.random() );
         tempList.addLast( randomNumber );
      }
      
      return ( System.nanoTime() - startTime ) / 1E6;
   }
}


// output:
// get each for ArrayList = 1.371099
// get each for LinkedList = 48.458214
// add at 0 for ArrayList = 23.515676
// add at 0 for LinkedList = 2.594003
// add at list.size() for ArrayList = 1.836519
// add at list.size() for LinkedList = 3.13795
// addLast for LinkedList = 1.249739


