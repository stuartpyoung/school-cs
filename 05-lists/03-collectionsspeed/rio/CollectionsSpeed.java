// Name: Rio Young
// Date: 12/11/18

// get each for ArrayList = 1.375089
// get each for LinkedList = 56.295722
// add at 0 for ArrayList = 23.377883
// add at 0 for LinkedList = 2.28576
// add at list.size() for ArrayList = 1.965355
// add at list.size() for LinkedList = 2.28629
// addLast for LinkedList = 2.861914


import java.util.*;

public class CollectionsSpeed
{
   public static final int MAX = 10000;
   
   public static void main(String[] args)
   {
      ArrayList<Integer> alist = new ArrayList<Integer>();
      LinkedList<Integer> llist = new LinkedList<Integer>();
      makeValues(alist, llist);
      
      System.out.println("get each for ArrayList = " + timeGetEach(alist));
      System.out.println("get each for LinkedList = " + timeGetEach(llist));
      System.out.println("add at 0 for ArrayList = " + timeAddFirst(alist));
      System.out.println("add at 0 for LinkedList = " + timeAddFirst(llist));
      System.out.println("add at list.size() for ArrayList = " + timeAddLast(alist));
      System.out.println("add at list.size() for LinkedList = " + timeAddLast(llist));
      System.out.println("addLast for LinkedList = " + timeAddLastLL(llist));
   }
   
   public static void makeValues(ArrayList<Integer> alist, LinkedList<Integer> llist)
   {
      for( int i = 0; i < MAX; i++ )
      {
         alist.add(i);              		 
         llist.add(i);							
      }
   }
   
   /**
    * Get n items by searching for each one.
    */      
   public static double timeGetEach(List<Integer> list)
   {
      double start = System.nanoTime();
      for( int i = 0; i < MAX; i++ )
      {
         int index = list.get(i);
      }
      return (System.nanoTime() - start)/1E6;
   }
   
   /**
    * Add 10000 new objects at position 0.
    */
   public static double timeAddFirst(List<Integer> list)
   {
      double start = System.nanoTime();
      for ( int i = 0; i < MAX; i++ )
      {
         int x =  (int) Math.round( Math.random() );
         list.add( 0, x );
      }
      
      // return list.get( 0 );
      return (System.nanoTime() - start)/1E6;
   }
   
   /*    
    * Add 10000 new objects at position list.size() 
    */
   public static double timeAddLast(List<Integer> list)
   {
      double start = System.nanoTime();
      for ( int i = 0; i < MAX; i++ )
      {
         int x =  (int) Math.round( Math.random() );
         list.add( list.size(), x );
      }
      
      // return list.get( list.size() - 1);
      return (System.nanoTime() - start)/1E6;
   }
   
   /*    
    * Add 10000 new objects at the end.  
    * Uses the LinkedList method addLast().
    * You must cast List list into a LinkedList. 
    */
   public static double timeAddLastLL(List<Integer> list)
   {
      LinkedList<Integer> newList = new LinkedList<Integer>();

      double start = System.nanoTime();
      for ( int i = 0; i < MAX; i++ )
      {
         int x =  (int) Math.round( Math.random() );
         newList.addLast( x );
      }
      
      // return newList.get( newList.size() - 1 );
      return (System.nanoTime() - start)/1E6;
   }
}
